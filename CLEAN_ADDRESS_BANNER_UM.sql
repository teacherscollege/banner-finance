CREATE OR REPLACE Package CLEAN_Address_Banner_UM
IS
/*****************************************************************************************
 *
 *  Package Name    :   CLEAN_Address_Banner_UM
 *
 *  Description     :   CLEAN_Address Banner specific implementation procedures
 *                       Modified for Unimarket addresses
 *
 *  Version         :   3.6.0.3 Jul 29, 2009
 *
 * ---------------------------------------------------------------------------------------
 *   Customer has the right to modify this package so long as original copyright remains
 * ---------------------------------------------------------------------------------------
 *
 *****************************************************************************************
 * Copyright (c) 2004-2009 Runner Technologies, Inc.  All Rights Reserved.
 * www.RunnerTechnologies.com   (877)784-0003  561-395-9322
 *****************************************************************************************/
-----------------------------------------------------------------------------------------
-- PUBLIC PROCEDURES
-----------------------------------------------------------------------------------------
/******************************************************************************************
 *  Procedure Name  :   Verify_Address_Record
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Verify a single address for Banner
 *                      - accepts CLEAN_Address Address record type as input
 ******************************************************************************************/
PROCEDURE Verify_Address_Record (
                     f_address_rec                  IN OUT CLEAN_Address.Address_rec
                    ,fv_plsql_error                 IN OUT VARCHAR2
                    ,fv_address_type                IN     VARCHAR2 DEFAULT NULL /* SPRADDR_ATYP_CODE */
                    ,fv_object_name                 IN     VARCHAR2 DEFAULT NULL /* Object Name where procedure is called from */
                    );
/******************************************************************************************
 *  Procedure Name  :   Check_Telephone_Record
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Verify a single telephone number for Banner
 *                      - accepts CLEAN_Address Telephone record type as input
 ******************************************************************************************/
PROCEDURE Check_Telephone_Record (
                     f_telephone_rec                IN OUT CLEAN_Address.Phone_rec
                    ,fv_phone_area                  IN     VARCHAR2
                    ,fv_plsql_error                 IN OUT VARCHAR2
                    ,fv_telephone_code              IN     VARCHAR2 DEFAULT NULL
                    ,fv_object_name                 IN     VARCHAR2 DEFAULT NULL /* Object Name where procedure is called from */
                    );
/******************************************************************************************
 *  Procedure Name  :   Standardize_Name_Record
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Standardize a single name record for Banner
 *                      - accepts CLEAN_Address Name record type as input
 ******************************************************************************************/
PROCEDURE Standardize_Name_Record (
                     f_name_rec                     IN OUT CLEAN_Address.Name_rec
                    ,fv_name_error_text             IN OUT VARCHAR2
                    ,fv_plsql_error                 IN OUT VARCHAR2
                    ,fv_name_type_code              IN     VARCHAR2 DEFAULT NULL /* SPRIDEN_NTYP_CODE */
                    ,fv_object_name                 IN     VARCHAR2 DEFAULT NULL /* Object Name where procedure is called from */
                    );
/******************************************************************************************
 *  Procedure Name  :   Validate_Email_Record
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Standardize a single name record for Banner
 *                      - accepts CLEAN_Address Name record type as input
 ******************************************************************************************/
PROCEDURE Validate_Email_Record (
                     f_email_rec                    IN OUT CLEAN_Address.Email_rec
                    ,fv_email_error_text            IN OUT VARCHAR2
                    ,fv_plsql_error                 IN OUT VARCHAR2
                    ,fv_email_type_code             IN     VARCHAR2 DEFAULT NULL /* GOREMAL_EMAL_CODE */
                    ,fv_object_name                 IN     VARCHAR2 DEFAULT NULL /* Object Name where procedure is called from */
                    );
/******************************************************************************************
 *  Procedure Name  :   Verify (OVERLOADED FOR 3 ADDRESS LINES FOR LEGACY SUPPORT)
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Verify a single address for Banner
 *
 *                      This call wraps the Address record implementation and excepts pure
 *                      VARCHAR2 IN/OUT parameters.
 *
 *                      address_date_and_error_code - YYYYMMDD:<error_code>
 *                         - address verified date concatenated with error code, separated by :
 ******************************************************************************************/
PROCEDURE Verify (
                     address_1                      IN OUT VARCHAR2 /* SPRADDR_STREET_LINE1 */
                    ,address_2                      IN OUT VARCHAR2 /* SPRADDR_STREET_LINE2 */
                    ,address_3                      IN OUT VARCHAR2 /* SPRADDR_STREET_LINE3 */
                    ,city                           IN OUT VARCHAR2 /* SPRADDR_CITY */
                    ,state                          IN OUT VARCHAR2 /* SPRADDR_STAT_CODE */
                    ,postal_code                    IN OUT VARCHAR2 /* SPRADDR_ZIP */
                    ,county_code                    IN OUT VARCHAR2 /* SPRADDR_CNTY_CODE */
                    ,country_code                   IN OUT VARCHAR2 /* SPRADDR_NATN_CODE */
                    ,delivery_point                 IN OUT NUMBER   /* SPRADDR_DELIVERY_POINT */
                    ,check_digit                    IN OUT NUMBER   /* SPRADDR_CORRECTION_DIGIT */
                    ,carrier_route                  IN OUT VARCHAR2 /* SPRADDR_CARRIER_ROUTE */
                    ,address_error_flag             IN OUT VARCHAR2 /* SPRADDR_REVIEWED_IND */
                    ,address_date_and_error_code    IN OUT VARCHAR2 /* SPRADDR_REVIEWED_USER */
                    ,address_error_text             IN OUT VARCHAR2
                    ,fv_plsql_error                 IN OUT VARCHAR2
                    ,fv_address_type                IN     VARCHAR2 DEFAULT NULL /* SPRADDR_ATYP_CODE */
                    ,company_name                   IN     VARCHAR2 DEFAULT NULL /* SPRIDEN_LAST_NAME */
                    ,fv_object_name                 IN     VARCHAR2 DEFAULT NULL /* Object Name where procedure is called from */
                    );
/******************************************************************************************
 *  Procedure Name  :   Verify
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Verify a single address for Banner
 *
 *                      This call wraps the Address record implementation and excepts pure
 *                      VARCHAR2 IN/OUT parameters.
 *
 *                      address_date_and_error_code - YYYYMMDD:<error_code>
 *                         - address verified date concatenated with error code, separated by :
 ******************************************************************************************/
PROCEDURE Verify (
                     address_1                      IN OUT VARCHAR2 /* SPRADDR_STREET_LINE1 */
                    ,address_2                      IN OUT VARCHAR2 /* SPRADDR_STREET_LINE2 */
                    ,address_3                      IN OUT VARCHAR2 /* SPRADDR_STREET_LINE3 */
     /*PREBANNER82*/,address_4                      IN OUT VARCHAR2 /* SPRADDR_STREET_LINE4 */
     /*PREBANNER82*/,house_number                   IN OUT VARCHAR2 /* SPRADDR_HOUSE_NUMBER */
                    ,city                           IN OUT VARCHAR2 /* SPRADDR_CITY */
                    ,state                          IN OUT VARCHAR2 /* SPRADDR_STAT_CODE */
                    ,postal_code                    IN OUT VARCHAR2 /* SPRADDR_ZIP */
                    ,county_code                    IN OUT VARCHAR2 /* SPRADDR_CNTY_CODE */
                    ,country_code                   IN OUT VARCHAR2 /* SPRADDR_NATN_CODE */
                    ,delivery_point                 IN OUT NUMBER   /* SPRADDR_DELIVERY_POINT */
                    ,check_digit                    IN OUT NUMBER   /* SPRADDR_CORRECTION_DIGIT */
                    ,carrier_route                  IN OUT VARCHAR2 /* SPRADDR_CARRIER_ROUTE */
                    ,address_error_flag             IN OUT VARCHAR2 /* SPRADDR_REVIEWED_IND */
                    ,address_date_and_error_code    IN OUT VARCHAR2 /* SPRADDR_REVIEWED_USER */
                    ,address_error_text             IN OUT VARCHAR2
                    ,fv_plsql_error                 IN OUT VARCHAR2
                    ,fv_address_type                IN     VARCHAR2 DEFAULT NULL /* SPRADDR_ATYP_CODE */
                    ,company_name                   IN     VARCHAR2 DEFAULT NULL /* SPRIDEN_LAST_NAME */
                    ,fv_object_name                 IN     VARCHAR2 DEFAULT NULL /* Object Name where procedure is called from */
                    );
/******************************************************************************************
 *  Procedure Name  :   Verify_SPRADDR_Record
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Verify a single address by passing in the SPRADDR Record type
 ******************************************************************************************/
PROCEDURE Verify_SPRADDR_Record (
                     f_SPRADDR_rec                  IN OUT SPRADDR%ROWTYPE
                    ,address_error_text             IN OUT VARCHAR2
                    ,fv_plsql_error                 IN OUT VARCHAR2
                    ,fv_address_type                IN     VARCHAR2 DEFAULT NULL /* SPRADDR_ATYP_CODE */
                    ,company_name                   IN     VARCHAR2 DEFAULT NULL /* SPRIDEN_LAST_NAME */
                    ,fv_object_name                 IN     VARCHAR2 DEFAULT NULL /* Object Name where procedure is called from */
                    );
/******************************************************************************************
 *  Procedure Name  :   Batch_Verify_SPRADDR
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the addresses in the Banner Student Address table (SPRADDR)
 *                      * fn_max_verify
 *                           - maximum number of address to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fv_address_type
 *                           - Banner Address Type - only verify this type
 *                           - NULL = to verify all address types
 *                      * fn_days_back
 *                           - Verify Addresses that were updated/created this many days back
 *                             - Note: You can enter fractional days such as 0.25, 0.5, etc
 *                           - NULL = all addresses
 *                      * fb_skip_international
 *                           - Skip international address verification in batch mode
 *                      * fb_verify_inactive
 *                           - Verify inactive addresses as well as active ones
 *                           - This essentially ignores the "to date" and status indicator on the record
 *                      * fd_from_date / fd_to_date
 *                           - Specify a date range for the verification based on SPRADDR_ACTIVITY_DATE
 *                           - This is useful for segmenting the data to create parallel batch procedures,
 *                             increasing throughput
 *                      * fb_set_activity_date_user
 *                           - Set the activity date (SPRADDR_ACTIVITY_DATE) and user (SPRADDR_USER)
 *                             audit fields if any of the address fields change after an update
 *                      * fv_set_source_code
 *                           - Set the address source code (SPRADDR_ASRC_CODE) to the value specified
 *                             if any of the address fields change after an update
 ******************************************************************************************/
PROCEDURE Batch_Verify (
                     fn_max_verify                  IN     NUMBER   DEFAULT 3000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fv_address_type                IN     VARCHAR2 DEFAULT NULL /* SPRADDR_ATYP_CODE */
                    ,fn_days_back                   IN     NUMBER   DEFAULT NULL
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    ,fb_verify_inactive             IN     BOOLEAN  DEFAULT FALSE
                    ,fd_from_date                   IN     DATE     DEFAULT NULL
                    ,fd_to_date                     IN     DATE     DEFAULT NULL
                    ,fb_set_activity_date_user      IN     BOOLEAN  DEFAULT FALSE
                    ,fv_set_source_code             IN     VARCHAR2 DEFAULT NULL
                    );
--
-- Batch_Verify_SPRADDR - overloaded version of Batch_Verify
--
PROCEDURE Batch_Verify_SPRADDR (
                     fn_max_verify                  IN     NUMBER   DEFAULT 3000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fv_address_type                IN     VARCHAR2 DEFAULT NULL /* SPRADDR_ATYP_CODE */
                    ,fn_days_back                   IN     NUMBER   DEFAULT NULL
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    ,fb_verify_inactive             IN     BOOLEAN  DEFAULT FALSE
                    ,fd_from_date                   IN     DATE     DEFAULT NULL
                    ,fd_to_date                     IN     DATE     DEFAULT NULL
                    ,fb_set_activity_date_user      IN     BOOLEAN  DEFAULT FALSE
                    ,fv_set_source_code             IN     VARCHAR2 DEFAULT NULL
                    );
/******************************************************************************************
 *  Procedure Name  :   Batch_Verify_SZRUMEX
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the custom Unimarket Remittance Address Table (SZRUMEX)
 *                      * fn_max_verify
 *                           - maximum number of address to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                        NOTE: This option only works if you have made the CLEAN_Address
 *                              mods to this table for missing address columns
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fb_skip_international
 *                           - Skip international address verification in batch mode
 ******************************************************************************************/                   
PROCEDURE Batch_Verify_SZRUMEX (
                     fn_max_verify                  IN     NUMBER   DEFAULT 1000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    );
/******************************************************************************************
 *  Procedure Name  :   Batch_Verify_SARADDR
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the Banner Electronics Admission Address Table (SARADDR)
 *                      * fn_max_verify
 *                           - maximum number of address to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                        NOTE: This option only works if you have made the CLEAN_Address
 *                              mods to this table for missing address columns
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fn_days_back
 *                           - Verify Addresses that were updated/created this many days back
 *                             - Note: You can enter fractional days such as 0.25, 0.5, etc
 *                           - NULL = all addresses
 *                      * fb_skip_international
 *                           - Skip international address verification in batch mode
 *                      * fd_from_date / fd_to_date
 *                           - Specify a date range for the verification based on SARADDR_ACTIVITY_DATE
 *                           - This is useful for segmenting the data to create parallel batch procedures,
 *                             increasing throughput
 ******************************************************************************************/
PROCEDURE Batch_Verify_SARADDR (
                     fn_max_verify                  IN     NUMBER   DEFAULT 1000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fn_days_back                   IN     NUMBER   DEFAULT NULL
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    ,fd_from_date                   IN     DATE     DEFAULT NULL
                    ,fd_to_date                     IN     DATE     DEFAULT NULL
                    );
/******************************************************************************************
 *  Procedure Name  :   Batch_Verify_SHBDIPL
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the Banner Diploma Address Table (SHBDIPL)
 *                      * fn_max_verify
 *                           - maximum number of address to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                        NOTE: This option only works if you have made the CLEAN_Address
 *                              mods to this table for missing address columns
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fn_days_back
 *                           - Verify Addresses that were updated/created this many days back
 *                             - Note: You can enter fractional days such as 0.25, 0.5, etc
 *                           - NULL = all addresses
 *                      * fb_skip_international
 *                           - Skip international address verification in batch mode
 *                      * fd_from_date / fd_to_date
 *                           - Specify a date range for the verification based on SHBDIPL_ACTIVITY_DATE
 *                           - This is useful for segmenting the data to create parallel batch procedures,
 *                             increasing throughput
 ******************************************************************************************/
PROCEDURE Batch_Verify_SHBDIPL (
                     fn_max_verify                  IN     NUMBER   DEFAULT 1000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fn_days_back                   IN     NUMBER   DEFAULT NULL
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    ,fd_from_date                   IN     DATE     DEFAULT NULL
                    ,fd_to_date                     IN     DATE     DEFAULT NULL
                    );
/******************************************************************************************
 *  Procedure Name  :   Batch_Verify_SPTADDR
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the Banner Temporary SPRADDR Address Table (SPTADDR)
 *                      * fn_max_verify
 *                           - maximum number of address to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                        NOTE: This option only works if you have made the CLEAN_Address
 *                              mods to this table for missing address columns
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fv_address_type
 *                           - Banner Address Type (SPTADDR_ATYP_CODE) - only verify this type
 *                           - NULL = to verify all address types
 *                      * fn_days_back
 *                           - Verify Addresses that were updated/created this many days back
 *                             - Note: You can enter fractional days such as 0.25, 0.5, etc
 *                           - NULL = all addresses
 *                      * fb_skip_international
 *                           - Skip international address verification in batch mode
 *                      * fb_verify_inactive
 *                           - Verify inactive addresses as well as active ones
 *                           - This essentially ignores the "to date" and status indicator on the record
 *                      * fd_from_date / fd_to_date
 *                           - Specify a date range for the verification based on SPTADDR_ACTIVITY_DATE
 *                           - This is useful for segmenting the data to create parallel batch procedures,
 *                             increasing throughput
 ******************************************************************************************/
PROCEDURE Batch_Verify_SPTADDR (
                     fn_max_verify                  IN     NUMBER   DEFAULT 1000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fv_address_type                IN     VARCHAR2 DEFAULT NULL /* SPTADDR_ATYP_CODE */
                    ,fn_days_back                   IN     NUMBER   DEFAULT NULL
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    ,fb_verify_inactive             IN     BOOLEAN  DEFAULT FALSE
                    ,fd_from_date                   IN     DATE     DEFAULT NULL
                    ,fd_to_date                     IN     DATE     DEFAULT NULL
                    );
/******************************************************************************************
 *  Procedure Name  :   Batch_Verify_ROTADDR
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the Banner Financial Aid Temporary SPRADDR Table (ROTADDR)
 *                      * fn_max_verify
 *                           - maximum number of address to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                        NOTE: This option only works if you have made the CLEAN_Address
 *                              mods to this table for missing address columns
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fv_address_type
 *                           - Banner Address Type (ROTADDR_ATYP_CODE) - only verify this type
 *                           - NULL = to verify all address types
 *                      * fn_days_back
 *                           - Verify Addresses that were updated/created this many days back
 *                             - Note: You can enter fractional days such as 0.25, 0.5, etc
 *                           - NULL = all addresses
 *                      * fb_skip_international
 *                           - Skip international address verification in batch mode
 *                      * fb_verify_inactive
 *                           - Verify inactive addresses as well as active ones
 *                           - This essentially ignores the "to date" and status indicator on the record
 *                      * fd_from_date / fd_to_date
 *                           - Specify a date range for the verification based on ROTADDR_ACTIVITY_DATE
 *                           - This is useful for segmenting the data to create parallel batch procedures,
 *                             increasing throughput
 ******************************************************************************************/
PROCEDURE Batch_Verify_ROTADDR (
                     fn_max_verify                  IN     NUMBER   DEFAULT 1000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fv_address_type                IN     VARCHAR2 DEFAULT NULL /* ROTADDR_ATYP_CODE */
                    ,fn_days_back                   IN     NUMBER   DEFAULT NULL
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    ,fb_verify_inactive             IN     BOOLEAN  DEFAULT FALSE
                    ,fd_from_date                   IN     DATE     DEFAULT NULL
                    ,fd_to_date                     IN     DATE     DEFAULT NULL
                    );
/******************************************************************************************
 *  Procedure Name  :   Batch_Verify_SRTADDR
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the Banner Electronic Prospect Address Table (SRTADDR)
 *                      * fn_max_verify
 *                           - maximum number of address to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                        NOTE: This option only works if you have made the CLEAN_Address
 *                              mods to this table for missing address columns
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fv_address_type
 *                           - Banner Address Type (SRTADDR_ATYP_CODE) - only verify this type
 *                           - NULL = to verify all address types
 *                      * fn_days_back
 *                           - Verify Addresses that were updated/created this many days back
 *                             - Note: You can enter fractional days such as 0.25, 0.5, etc
 *                           - NULL = all addresses
 *                      * fb_skip_international
 *                           - Skip international address verification in batch mode
 *                      * fb_verify_inactive
 *                           - Verify inactive addresses as well as active ones
 *                           - This essentially ignores the "to date" and status indicator on the record
 *                      * fd_from_date / fd_to_date
 *                           - Specify a date range for the verification based on SRTADDR_ACTIVITY_DATE
 *                           - This is useful for segmenting the data to create parallel batch procedures,
 *                             increasing throughput
 ******************************************************************************************/
PROCEDURE Batch_Verify_SRTADDR (
                     fn_max_verify                  IN     NUMBER   DEFAULT 1000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fv_address_type                IN     VARCHAR2 DEFAULT NULL /* SRTADDR_ATYP_CODE */
                    ,fn_days_back                   IN     NUMBER   DEFAULT NULL
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    ,fb_verify_inactive             IN     BOOLEAN  DEFAULT FALSE
                    ,fd_from_date                   IN     DATE     DEFAULT NULL
                    ,fd_to_date                     IN     DATE     DEFAULT NULL
                    );
/******************************************************************************************
 *  Procedure Name  :   Batch_Verify_SOBSBGI
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the Banner Electronic Prospect Address Table (SOBSBGI)
 *                      * fn_max_verify
 *                           - maximum number of address to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                        NOTE: This option only works if you have made the CLEAN_Address
 *                              mods to this table for missing address columns
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fb_skip_international
 *                           - Skip international address verification in batch mode
 ******************************************************************************************/
PROCEDURE Batch_Verify_SOBSBGI (
                     fn_max_verify                  IN     NUMBER   DEFAULT 1000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    );
/******************************************************************************************
 *  Procedure Name  :   Batch_Verify_SRTHSCH
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the Banner Electronic Prospect Address Table (SRTHSCH)
 *                      * fn_max_verify
 *                           - maximum number of address to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                        NOTE: This option only works if you have made the CLEAN_Address
 *                              mods to this table for missing address columns
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fn_days_back
 *                           - Verify Addresses that were updated/created this many days back
 *                             - Note: You can enter fractional days such as 0.25, 0.5, etc
 *                           - NULL = all addresses
 *                      * fb_skip_international
 *                           - Skip international address verification in batch mode
 ******************************************************************************************/
PROCEDURE Batch_Verify_SRTHSCH (
                     fn_max_verify                  IN     NUMBER   DEFAULT 1000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fn_days_back                   IN     NUMBER   DEFAULT NULL
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    );
/******************************************************************************************
 *  Procedure Name  :   Batch_Verify_SRTPCOL
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the Banner Electronic Prospect Address Table (SRTPCOL)
 *                      * fn_max_verify
 *                           - maximum number of address to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                        NOTE: This option only works if you have made the CLEAN_Address
 *                              mods to this table for missing address columns
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fn_days_back
 *                           - Verify Addresses that were updated/created this many days back
 *                             - Note: You can enter fractional days such as 0.25, 0.5, etc
 *                           - NULL = all addresses
 *                      * fb_skip_international
 *                           - Skip international address verification in batch mode
 ******************************************************************************************/
PROCEDURE Batch_Verify_SRTPCOL (
                     fn_max_verify                  IN     NUMBER   DEFAULT 1000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fn_days_back                   IN     NUMBER   DEFAULT NULL
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    );
/******************************************************************************************
 *  Procedure Name  :   Batch_Verify_SPREMRG
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the Banner Emergency Contact Table (SPREMRG)
 *                      * fn_max_verify
 *                           - maximum number of address to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                        NOTE: This option only works if you have made the CLEAN_Address
 *                              mods to this table for missing address columns
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fv_address_type
 *                           - Banner Address Type (SPREMRG_ATYP_CODE) - only verify this type
 *                           - NULL = to verify all address types
 *                      * fn_days_back
 *                           - Verify Addresses that were updated/created this many days back
 *                             - Note: You can enter fractional days such as 0.25, 0.5, etc
 *                           - NULL = all addresses
 *                      * fb_skip_international
 *                           - Skip international address verification in batch mode
 *                      * fd_from_date / fd_to_date
 *                           - Specify a date range for the verification based on SPREMRG_ACTIVITY_DATE
 *                           - This is useful for segmenting the data to create parallel batch procedures,
 *                             increasing throughput
 ******************************************************************************************/
PROCEDURE Batch_Verify_SPREMRG (
                     fn_max_verify                  IN     NUMBER   DEFAULT 1000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fv_address_type                IN     VARCHAR2 DEFAULT NULL /* SPREMRG_ATYP_CODE */
                    ,fn_days_back                   IN     NUMBER   DEFAULT NULL
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    ,fd_from_date                   IN     DATE     DEFAULT NULL
                    ,fd_to_date                     IN     DATE     DEFAULT NULL
                    );
/******************************************************************************************
 *  Procedure Name  :   Batch_Verify_SHTTRAN
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the Banner Transcript Request Table (SHTTRAN)
 *                      * fn_max_verify
 *                           - maximum number of address to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                        NOTE: This option only works if you have made the CLEAN_Address
 *                              mods to this table for missing address columns
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fn_days_back
 *                           - Verify Addresses that were updated/created this many days back
 *                             - Note: You can enter fractional days such as 0.25, 0.5, etc
 *                           - NULL = all addresses
 *                      * fb_skip_international
 *                           - Skip international address verification in batch mode
 *                      * fd_from_date / fd_to_date
 *                           - Specify a date range for the verification based on SHTTRAN_ACTIVITY_DATE
 *                           - This is useful for segmenting the data to create parallel batch procedures,
 *                             increasing throughput
 ******************************************************************************************/
PROCEDURE Batch_Verify_SHTTRAN (
                     fn_max_verify                  IN     NUMBER   DEFAULT 1000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fn_days_back                   IN     NUMBER   DEFAULT NULL
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    ,fd_from_date                   IN     DATE     DEFAULT NULL
                    ,fd_to_date                     IN     DATE     DEFAULT NULL
                    );
/******************************************************************************************
 *  Function Name   :   Get_Error_Code
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Return the error code from the SPRADDR_REVIEWED_USER field
 *                      NOTE: The SPRADDR_REVIEWED_USER holds the date and address error
 *                            in the format YYYYMMDD:<error_code>
 ******************************************************************************************/
FUNCTION Get_Error_Code (
                     address_date_and_error_code    IN     VARCHAR2 /* SPRADDR_REVIEWED_USER */
                    ) RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES (Get_Error_Code, WNDS, WNPS, RNDS);
/******************************************************************************************
 *  Function Name   :   Get_Error_Text
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Return the error text from the code in the SPRADDR_REVIEWED_USER field
 *                      NOTE: The SPRADDR_REVIEWED_USER holds the date and address error
 *                            in the format YYYYMMDD:<error_code>
 ******************************************************************************************/
FUNCTION Get_Error_Text (
                     address_date_and_error_code    IN     VARCHAR2 /* SPRADDR_REVIEWED_USER */
                    ) RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES (Get_Error_Text, WNDS, WNPS);
/******************************************************************************************
 *  Function Name   :   Get_Error_Help
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Return the error help text from the code in the SPRADDR_REVIEWED_USER field
 *                      NOTE: The SPRADDR_REVIEWED_USER holds the date and address error
 *                            in the format YYYYMMDD:<error_code>
 *                      NOTE: You can update the help text in the view CLN_Address_Errors_V
 *                            by updating the HELP_TEXT field, or you can override it by
 *                            using the COMMENT_TEXT field
 ******************************************************************************************/
FUNCTION Get_Error_Help (
                     address_date_and_error_code    IN     VARCHAR2 /* SPRADDR_REVIEWED_USER */
                    ) RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES (Get_Error_Help, WNDS, WNPS);
/******************************************************************************************
 *  Function Name   :   Get_Verified_Date
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Return the Address Verified Date from the SPRADDR_REVIEWED_USER field
 *                      NOTE: The SPRADDR_REVIEWED_USER holds the date and address error
 *                            in the format YYYYMMDD:<error_code>
 ******************************************************************************************/
FUNCTION Get_Verified_Date (
                     address_date_and_error_code    IN     VARCHAR2 /* SPRADDR_REVIEWED_USER */
                    ) RETURN DATE;
PRAGMA RESTRICT_REFERENCES (Get_Verified_Date, WNDS, WNPS, RNDS);
/******************************************************************************************
 *  Procedure Name  :   Check_Telephone
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Verify a single telephone number for Banner
 *
 *                      This call wraps the Telephone record implementation and excepts pure
 *                      VARCHAR2 IN/OUT parameters.
 *
 *                      telephone_results - <suggested_area>:<distance>:<error_code>
 *                         - Telephone suggested Area, Distance, and error code, separated by :
 ******************************************************************************************/
PROCEDURE Check_Telephone (
                     phone_area                     IN OUT VARCHAR2 /* SPRTELE_PHONE_AREA */
                    ,phone_number                   IN OUT VARCHAR2 /* SPRTELE_PHONE_NUMBER */
                    ,phone_extension                IN OUT VARCHAR2 /* SPRTELE_PHONE_EXT */
                    ,postal_code                    IN     VARCHAR2 /* SPRADDR_ZIP */
                    ,telephone_results              IN OUT VARCHAR2 /* SPRTELE_DATA_ORIGIN */
                    ,suggested_area                    OUT VARCHAR2 /* Possible new area code */
                    ,distance                          OUT NUMBER   /* Distance from Phone to ZIP Code */
                    ,telephone_error_text           IN OUT VARCHAR2
                    ,fv_plsql_error                 IN OUT VARCHAR2
                    ,fv_telephone_code              IN     VARCHAR2 DEFAULT NULL /* SPRTELE_TELE_CODE */
                    ,fv_object_name                 IN     VARCHAR2 DEFAULT NULL /* Object Name where procedure is called from */
                    );
/******************************************************************************************
 *  Procedure Name  :   Check_SPRTELE_Record
 *
 *  Scope           :   PRIVATE
 *
 *  Description     :   Verify a single telephone by passing in the SPRTELE Record type
 ******************************************************************************************/
PROCEDURE Check_SPRTELE_Record (
                     f_SPRTELE_rec                  IN OUT SPRTELE%ROWTYPE
                    ,suggested_area                    OUT VARCHAR2 /* Possible new area code */
                    ,distance                          OUT NUMBER   /* Distance from Phone to ZIP Code */
                    ,telephone_error_text           IN OUT VARCHAR2
                    ,fv_plsql_error                 IN OUT VARCHAR2
                    ,fv_telephone_code              IN     VARCHAR2 DEFAULT NULL /* SPRTELE_TELE_CODE */
                    ,fv_object_name                 IN     VARCHAR2 DEFAULT NULL /* Object Name where procedure is called from */
                    );
/******************************************************************************************
 *  Procedure Name  :   Batch_Check_Telephone
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the telephones in the Banner Telephone table (SPRTELE)
 *                      * fn_max_verify
 *                           - maximum number of telephones to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fv_telephone_code
 *                           - Banner Telephone Code - only verify this type (SPRTELE_TELE_CODE)
 *                           - NULL = to verify all telephone codes
 *                      * fn_days_back
 *                           - Verify Telephone numbers that were updated/created this many days back
 *                             - Note: You can enter fractional days such as 0.25, 0.5, etc
 *                           - NULL = all telephones
 ******************************************************************************************/
PROCEDURE Batch_Check_Telephone (
                     fn_max_verify                  IN     NUMBER   DEFAULT 1000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fv_telephone_code              IN     VARCHAR2 DEFAULT NULL /* SPRTELE_TELE_CODE */
                    ,fn_days_back                   IN     NUMBER   DEFAULT NULL
                    );
/******************************************************************************************
 *  Function Name   :   Get_Telephone_Suggested_Area
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Return the Suggested Area Code from the SPRTELE_DATA_ORIGIN field
 *                      NOTE: The SPRTELE_DATA_ORIGIN holds the telephone results in the format
 *                            <suggested_area>:<distance>:<error_code>
 ******************************************************************************************/
FUNCTION Get_Telephone_Suggested_Area (
                     telephone_results              IN     VARCHAR2 /* SPRTELE_DATA_ORIGIN */
                    ) RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES (Get_Telephone_Suggested_Area, WNDS, WNPS, RNDS);
/******************************************************************************************
 *  Function Name   :   Get_Telephone_Distance
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Return the Distance from the SPRTELE_DATA_ORIGIN field
 *                      - This is the distance from the area code to the Zip code for the
 *                        attached address
 *                      NOTE: The SPRTELE_DATA_ORIGIN holds the telephone results in the format
 *                            <suggested_area>:<distance>:<error_code>
 ******************************************************************************************/
FUNCTION Get_Telephone_Distance (
                     telephone_results              IN     VARCHAR2 /* SPRTELE_DATA_ORIGIN */
                    ) RETURN NUMBER;
PRAGMA RESTRICT_REFERENCES (Get_Telephone_Distance, WNDS, WNPS, RNDS);
/******************************************************************************************
 *  Function Name   :   Get_Telephone_Error_Code
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Return the error code from the SPRTELE_DATA_ORIGIN field
 *                      NOTE: The SPRTELE_DATA_ORIGIN holds the telephone results in the format
 *                            <suggested_area>:<distance>:<error_code>
 ******************************************************************************************/
FUNCTION Get_Telephone_Error_Code (
                     telephone_results              IN     VARCHAR2 /* SPRTELE_DATA_ORIGIN */
                    ) RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES (Get_Telephone_Error_Code, WNDS, WNPS, RNDS);
/******************************************************************************************
 *  Function Name   :   Get_Telephone_Error_Text
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Return the error text from the code in the SPRTELE_DATA_ORIGIN field
 *                      NOTE: The SPRTELE_DATA_ORIGIN holds the telephone results in the format
 *                            <suggested_area>:<distance>:<error_code>
 ******************************************************************************************/
FUNCTION Get_Telephone_Error_Text (
                     telephone_results              IN     VARCHAR2 /* SPRTELE_DATA_ORIGIN */
                    ) RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES (Get_Telephone_Error_Text, WNDS, WNPS);
/******************************************************************************************
 *  Procedure Name  :   Sync_Postal_Codes
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Sync the Banner GTVZIPC postal codes table from the CLEAN_Address
 *                      CLN_Postal_Codes table
 *                      - Set fb_sync_county to TRUE to sync the STVCNTY table
 *                      - Set fb_sync_postal_codes to TRUE to sync the GTVZIPC table
 *                      - Set fv_state a specific state to ONLY sync county codes for that state
 *                      - Set fb_set_county_for_postal to FALSE to not populate county on GTVZIPC table
 *                      - Set fb_prefix_state_on_county_name to TRUE to prefix the state code on the
 *                        county name (I.e. FL-Orange, CA-Orange)
 *                      - Set fb_prefix_state_on_county_code to TRUE to prefix the state code on the
 *                        county code (I.e. FL099 instead of 12099), FALSE to use numeric prefix
 *                        NOTE: This only applies if the STVCNTY table has no records (new sync)
 *                      - Set fb_use_unique_postal_codes to only keep one unique postal code without multiple cities - uses preferred city name
 ******************************************************************************************/
PROCEDURE Sync_Postal_Codes (
                     fb_sync_county                 IN     BOOLEAN  DEFAULT TRUE /* STVCNTY */
                    ,fb_sync_postal_codes           IN     BOOLEAN  DEFAULT TRUE /* GTVZIPC */
                    ,fv_state                       IN     VARCHAR2 DEFAULT NULL /* Specific State for STVCNTY */
                    ,fb_set_county_for_postal       IN     BOOLEAN  DEFAULT TRUE /* Populate county on GTVZIPC */
                    ,fb_prefix_state_on_county_name IN     BOOLEAN  DEFAULT FALSE /* Prefixes State code on County Name (i.e. FL-Palm Beach) */
                    ,fb_prefix_state_on_county_code IN     BOOLEAN  DEFAULT TRUE  /* Prefixes State code on County Code (i.e. FL099 instead of 12099 */
                    ,fb_use_unique_postal_codes     IN     BOOLEAN  DEFAULT TRUE  /* Only keep one unique postal code without multiple cities - uses preferred city name */
                    );
/******************************************************************************************
 *  Procedure Name  :   Standardize_Name
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Standardize the name components and return warning message if first name appears invalid
 *
 *                      This call wraps the Name record implementation and excepts pure
 *                      VARCHAR2 IN/OUT parameters.
 ******************************************************************************************/
PROCEDURE Standardize_Name (
                     first_name                     IN OUT VARCHAR2 /* SPRIDEN_FIRST_NAME */
                    ,last_name                      IN OUT VARCHAR2 /* SPRIDEN_LAST_NAME */
                    ,middle_name                    IN OUT VARCHAR2 /* SPRIDEN_MI */
                    ,prefix                         IN OUT VARCHAR2 /* SPBPERS_NAME_PREFIX */
                    ,suffix                         IN OUT VARCHAR2 /* SPBPERS_NAME_SUFFIX */
                    ,name_error_code                IN OUT VARCHAR2
                    ,name_error_text                IN OUT VARCHAR2
                    ,fv_plsql_error                 IN OUT VARCHAR2
                    ,fv_name_type_code              IN     VARCHAR2 DEFAULT NULL /* SPRIDEN_NTYP_CODE */
                    ,fv_object_name                 IN     VARCHAR2 DEFAULT NULL /* Object Name where procedure is called from */
                    );
/******************************************************************************************
 *  Function Name   :   Get_Name_Error_Text
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Return the error text from the name error code
 ******************************************************************************************/
FUNCTION Get_Name_Error_Text (
                     name_error_code                IN     VARCHAR2
                    ) RETURN VARCHAR2;
/******************************************************************************************
 *  Procedure Name  :   Validate_Email
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Validate the Email Address, checking syntax and domain name
 *
 *                      This call wraps the Email record implementation and excepts pure
 *                      VARCHAR2 IN/OUT parameters.
 ******************************************************************************************/
PROCEDURE Validate_Email (
                     email_address                  IN OUT VARCHAR2 /* GOREMAIL_EMAIL_ADDRESS */
                    ,email_error_code               IN OUT VARCHAR2
                    ,email_error_text               IN OUT VARCHAR2
                    ,fv_plsql_error                 IN OUT VARCHAR2
                    ,fv_email_type_code             IN     VARCHAR2 DEFAULT NULL /* GOREMAL_EMAL_CODE */
                    ,fv_object_name                 IN     VARCHAR2 DEFAULT NULL /* Object Name where procedure is called from */
                    );
/******************************************************************************************
 *  Function Name   :   Get_Email_Error_Text
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Return the error text from the email error code
 ******************************************************************************************/
FUNCTION Get_Email_Error_Text (
                     email_error_code               IN     VARCHAR2
                    ) RETURN VARCHAR2;
-----------------------------------------------------------------------------------------
--
-- PUBLIC PROCEDURES - PARAMETER GET / SET
--
-----------------------------------------------------------------------------------------
/******************************************************************************************
 *  Function Name   :   Get_Expand_Street_Suffix
 *
 *  Description     :
 *    Set gb_expand_street_suffix to TRUE to expand the street suffix (Ave -> Avenue) if it will
 *    fit on one line.  If set to FALSE, the USPS CASS standardization rules will be used.
 *    DEFAULT: FALSE
 ******************************************************************************************/
Function Get_Expand_Street_Suffix RETURN BOOLEAN;
/******************************************************************************************
 *  Procedure Name  :   Set_Expand_Street_Suffix
 ******************************************************************************************/
Procedure Set_Expand_Street_Suffix (
                     fb_value                       IN     BOOLEAN  DEFAULT FALSE
                    );
/******************************************************************************************
 *  Function Name   :   Get_Suite_Apt_Before_Street
 *
 *  Description     :
 *    Set gb_suite_apt_before_street to TRUE to put the suite/apartment line before the address line
 *    in cases where the the suite/apartment does not fit on one line.  If set to FALSE, the suite/apt
 *    will appear on the line after the street address line.
 *    DEFAULT: FALSE
 ******************************************************************************************/
Function Get_Suite_Apt_Before_Street RETURN BOOLEAN;
/******************************************************************************************
 *  Procedure Name  :   Set_Suite_Apt_Before_Street
 ******************************************************************************************/
Procedure Set_Suite_Apt_Before_Street (
                     fb_value                       IN     BOOLEAN  DEFAULT FALSE
                    );
/******************************************************************************************
 *  Function Name   :   Get_Append_Suite_To_Street
 *
 *  Description     :
 *    Set gb_append_suite_to_street to TRUE to always "try" to append the suite/apartment line
 *    on the same line as the the address line - only when it will fit.  If set to FALSE, the
 *    suite/apt will be on a separate line if there a free line available
 *    DEFAULT: TRUE
 ******************************************************************************************/
Function Get_Append_Suite_To_Street RETURN BOOLEAN;
/******************************************************************************************
 *  Procedure Name  :   Set_Append_Suite_To_Street
 ******************************************************************************************/
Procedure Set_Append_Suite_To_Street (
                     fb_value                       IN     BOOLEAN  DEFAULT TRUE
                    );
/******************************************************************************************
 *  Function Name   :   Get_Use_ZIP_Plus4
 *
 *  Description     :
 *    Set gb_use_ZIP_Plus4 to TRUE to append the ZIP+4 digits after the ZIP Code in the
 *    SPRADDR_ZIP field.  If set to FALSE, only the 5-digit ZIP Code will be used
 *    DEFAULT: TRUE
 ******************************************************************************************/
Function Get_Use_ZIP_Plus4 RETURN BOOLEAN;
/******************************************************************************************
 *  Procedure Name  :   Set_Use_ZIP_Plus4
 ******************************************************************************************/
Procedure Set_Use_ZIP_Plus4 (
                     fb_value                       IN     BOOLEAN  DEFAULT TRUE
                    );
/******************************************************************************************
 *  Function Name   :   Get_Batch_Update_Existing
 *
 *  Description     :
 *    Set gb_batch_update_existing to TRUE to update the existing address record when running
 *    in batch mode.  If set to FALSE, a new record will be created if anything changes in the
 *    address.  This could easily double the size of the SPRADDR table if set FALSE, since most
 *    addresses are not keyed in in USPS standards. Please take this into consideration.
 *    DEFAULT: TRUE
 ******************************************************************************************/
Function Get_Batch_Update_Existing RETURN BOOLEAN;
/******************************************************************************************
 *  Procedure Name  :   Set_Batch_Update_Existing
 ******************************************************************************************/
Procedure Set_Batch_Update_Existing (
                     fb_value                       IN     BOOLEAN  DEFAULT TRUE
                    );
/******************************************************************************************
 *  Function Name   :   Get_Telephone_Distance_Limit
 *
 *  Description     :
 *    Set gn_telephone_distance_limit to the number of miles threshhold to allow before an
 *    alert message is displayed to the user.  This is the distance between the telephone
 *    wire center (area code and prefix = first 6 digits) and the 5-digit ZIP Code
 *    DEFAULT: 100
 ******************************************************************************************/
Function Get_Telephone_Distance_Limit RETURN NUMBER;
/******************************************************************************************
 *  Procedure Name  :   Set_Telephone_Distance_Limit
 ******************************************************************************************/
Procedure Set_Telephone_Distance_Limit (
                     fn_value                       IN     NUMBER  DEFAULT 100
                    );
/******************************************************************************************
 *  Function Name   :   Get_Error_Suggest_Count
 *
 *  Description     :
 *    Set gn_Error_Suggest_Count to the number of possible matches to return for unverified addresses
 *    DEFAULT: 0
 ******************************************************************************************/
Function Get_Error_Suggest_Count RETURN PLS_INTEGER;
/******************************************************************************************
 *  Procedure Name  :   Set_Error_Suggest_Count
 ******************************************************************************************/
Procedure Set_Error_Suggest_Count (
                     fn_value                       IN     PLS_INTEGER  DEFAULT 0
                    );
/******************************************************************************************
 *  Function Name   :   Get_Upper_Case_Results
 *
 *  Description     :
 *    Set gb_Upper_Case_Results to TRUE to always UPPER CASE address fields
 *    DEFAULT: FALSE
 ******************************************************************************************/
Function Get_Upper_Case_Results RETURN BOOLEAN;
/******************************************************************************************
 *  Procedure Name  :   Set_Upper_Case_Results
 ******************************************************************************************/
Procedure Set_Upper_Case_Results (
                     fb_value                       IN     BOOLEAN  DEFAULT FALSE
                    );
/******************************************************************************************
 *  Function Name   :   Get_Address_Lines
 *
 *  Description     :
 *    Set gn_Address_Lines to the number of address lines to process
 *    DEFAULT: 4
 ******************************************************************************************/
Function Get_Address_Lines RETURN NUMBER;
/******************************************************************************************
 *  Procedure Name  :   Set_Address_Lines
 ******************************************************************************************/
Procedure Set_Address_Lines (
                     fn_value                       IN     NUMBER  DEFAULT 4
                    );
/******************************************************************************************
 *  Function Name   :   Get_Max_Address_Length
 *
 *  Description     :
 *    Set gn_Max_Address_Length to limit size of address field
 *    DEFAULT: 30
 ******************************************************************************************/
Function Get_Max_Address_Length RETURN NUMBER;
/******************************************************************************************
 *  Procedure Name  :   Set_Max_Address_Length
 ******************************************************************************************/
Procedure Set_Max_Address_Length (
                     fn_value                       IN     NUMBER  DEFAULT 55
                    );
/******************************************************************************************
 *  Function Name   :   Get_Max_City_Length
 *
 *  Description     :
 *    Set gn_Max_City_Length to limit size of City field
 *    DEFAULT: 20
 ******************************************************************************************/
Function Get_Max_City_Length RETURN NUMBER;
/******************************************************************************************
 *  Procedure Name  :   Set_Max_City_Length
 ******************************************************************************************/
Procedure Set_Max_City_Length (
                     fn_value                       IN     NUMBER  DEFAULT 55
                    );
/******************************************************************************************
 *  Function Name   :   Get_Ignore_Error
 ******************************************************************************************/
Function Get_Ignore_Error RETURN BOOLEAN;
/******************************************************************************************
 *  Procedure Name  :   Set_Ignore_Error
 *
 *  Description     :
 *    Set gb_Ignore_Error TRUE to allow Web Self-Service to save data even with an error code
 *    - NOTE: The error code will still be stored on the record but allow the transaction to continue
 ******************************************************************************************/
Procedure Set_Ignore_Error (
                     fb_value                       IN     BOOLEAN
                    );
/******************************************************************************************
 *  Function Name   :   Get_Object_Name
 ******************************************************************************************/
Function Get_Object_Name RETURN VARCHAR2;
/******************************************************************************************
 *  Procedure Name  :   Set_Object_Name
 *
 *  Description     :
 *    Set gv_Object_Name to the Object that is currently calling the verification procedure
 ******************************************************************************************/
Procedure Set_Object_Name (
                     fv_value                       IN     VARCHAR2
                    );
/******************************************************************************************
 *  Function Name   :   Get_Local_Country
 ******************************************************************************************/
Function Get_Local_Country RETURN VARCHAR2;
/******************************************************************************************
 *  Procedure Name  :   Set_Local_Country
 *
 *  Description     :
 *    Set gv_Local_Country to the value specified, which is the Banner specific
 *    Nation Code for the predominant country
 ******************************************************************************************/
Procedure Set_Local_Country (
                     fv_value                       IN     VARCHAR2
                    );
/******************************************************************************************
 *  Function Name   :   Get_Skip_International
 ******************************************************************************************/
Function Get_Skip_International RETURN BOOLEAN;
/******************************************************************************************
 *  Procedure Name  :   Set_Skip_International
 *
 *  Description     :
 *    Set gb_Skip_International TRUE to not process international address records
 *    This allows more control on the purchased Web Service transactions.
 *    NOTE: User should pass this parameter into the batch procedure rather than setting directly
 ******************************************************************************************/
Procedure Set_Skip_International (
                     fb_value                       IN     BOOLEAN
                    );
/******************************************************************************************
 *  Function Name   :   Get_Current_URL
 *
 *  Description     :
 *    Return the current host, port, and PL/SQL DAD URL prefix - used in AJAX implementation
 ******************************************************************************************/
Function Get_Current_URL RETURN VARCHAR2;
/******************************************************************************************
 *  Function Name   :   Set_Current_URL
 *
 *  Description     :
 *    Set the current host, port, and PL/SQL DAD URL prefix - used in AJAX implementation
 *    NOTE: The setting should look something like: 'https://www.myURL.edu:8888/pls/DAD/'
 ******************************************************************************************/
Procedure Set_Current_URL (
                     fv_value                       IN     VARCHAR2
                    );
END CLEAN_Address_Banner_UM;
/


CREATE OR REPLACE Package Body CLEAN_Address_Banner_UM
IS
gv_Package_Body_Version  CONSTANT VARCHAR2(30) := '3.6.0.7';
gv_Package_Body_Date     CONSTANT VARCHAR2(30) := '25-Jan-2010';
/*****************************************************************************************
 *
 *  Package Name    :   CLEAN_Address_Banner_UM
 *
 *  Description     :   CLEAN_Address Banner specific implementation procedures
 *                      Modified for Unimarket
 *
 *  Version         :   3.6.0.7 Jan 25, 2010
 *
 *  NOTES:
 *  1) Some of the Banner address tables do not contain fields for address error,
 *     carrier route, delivery point, or check digit. To include these columns, run the
 *     script: pkg/address_table_mods.sql.
 *     If you choose to not apply the new columns then do the following replacment in this package: */
/* Replace */
              --CLNMOD--
/* with */
              --CLNMOD--
/*
 *  2) If your Banner installation contains SICAS modifications, replace --SICASMOD-- with blank.
 *
 *  3) For releases of Banner prior to Banner 8.2 and above, do the following replacment in this package spec and body: */
/* Replace */
              /*PREBANNER82*/
/* with */
              --PREBANNER82--
/*
 *
 * The following Banner tables are supported for batch verification in this package:
 *
 *  SPRADDR -- Student / HR / Finance / Alumni Address Table
 *  SARADDR -- Electronics Admission Address Table
 *  SHBDIPL -- Diploma Address Table
 *  SPTADDR -- Temporary SPRADDR Table
 *  ROTADDR -- Financial Aid Temporary SPRADDR Table
 *  SRTADDR -- Electronic Prospect Address Table loaded from search and test
 *             score tapes, or entered on Web for Prospects.
 *  SOBSBGI -- College Code Address Table
 *  SRTHSCH -- High School Address Table
 *  SRTPCOL -- Prior College Address Table
 *  SPREMRG -- Emergency Contact Address Table
 *  SHTTRAN -- Transcript Request Table
 *****************************************************************************************/
/* ---------------------------------------------------------------------------------------
 *   Customer has the right to modify this package so long as original copyright remains
 * ---------------------------------------------------------------------------------------
 * History:
 * 03/03/2005 - Added Sync_Postal_Codes procedure;
 *              Added county code logic to use either character or numeric state codes
 * 03/31/2005 - Append Suite to Address if address is in line 2 and suite in line 3
 *              Return NULL if there is no error instead of 'No Error'
 * 04/08/2005 - Added Get_Error_Help function to return Comment / Help text for address errors
 *            - Modified Sync_Postal_Codes to sync counties first to avoid Foreign Key constraint
 * 04/11/2005 - Update Check_Telephone procedure to work for real-time APIs
 * 05/08/2005
 *   - Added Pre and Post verification user exits for Address and Telephone realtime and batch
 *   - Replace # with Apt in address lines to prevent errors in downstream print applications
 *   - Added new Address Verificaiton configurable parameters and Get/Set procedures.
 *     gb_Use_ZIP_Plus4, gb_Expand_Street_Suffix, gb_Suite_Apt_Before_Street, gb_Append_Suite_To_Street
 *   - Added new telephone parameters: gn_Telephone_Distance_Limit
 *   - Added Batch Address Verication parameter: gb_Batch_Update_Existing
 *   - Added initCity call to correctly capitalize city name in Sync_Postal_Codes
 *   - Added parameters to Sync_Postal_Codes for syncing county / ZIP Codes independently
 *   - Fixed issue in Sync_Postal_Codes for missing Counties and foreign key exceptions
 * 07/13/2005 - Fixed bug in Sync_Postal_Codes for 00000 County code for APO/FPO
 * 09/01/2005 - Added batch address verification for additional Banner address tables
 * 09/08/2005 - Fixed Null county issue in ZIP code sync for Alpha State prefix on counties
 * 09/13/2005 - Fixed # replacement with Apt if there is an address error
 * 09/14/2005 - Added SICAS modification for STVCNTY_YACN_CODE_SAPC NOT NULL in STVCNTY
 *               - Added exception code in Sync_Postal_Codes when State does not exist in STVSTAT
 * 09/17/2005 - Added Verify_Address_User_Exit API support for Banner 7.0 GB_ADDRESS package
 *               - Fixed Sync_Postal_Code for 3-digit county codes
 * 10/07/2005 3.0.2.0 - Trim address lines > 30 characters at last space instead of truncing
 * 11/30/2005 3.0.3.0 - Set REVIEWED_IND to Y if verified successfully, N if address has error - Banner 7.1 compliance
 * 12/13/2005 3.0.4.0 - Added optional Company_Name parameter to Verify procedure so Vendor and Finance addresses
 *                      will return the correct ZIP+4 for certain larger companies
 * 01/18/2006 3.0.5.0 - PMB Fix for Private Mailboxes
 * 01/25/2006 3.0.5.1 - conditionally show the telephone Suggested Area Code
 * 01/27/2006 3.0.6.0 - Added Batch Verify for the Banner Emergency Contact Table (SPREMRG)
 * 03/06/2006 3.1.0.0 - Return optional Address Suggestion List for unverified addresses
 * 03/29/2006 3.2.0.0 - Added fb_prefix_state_on_county_name option to Sync_Postal_Codes
 * 05/15/2006 3.3.0.0 - Fixed concatenation of PO Box address lines with apartment numbers
 * 05/18/2006 3.3.1.0 - Added SOBSBGI_CNTY_CODE to Batch_Verify_SOBSBGI; fixed numeric overflow for batch statistics
 * 05/28/2006 3.3.2.0 - Fixed logic for Canadian French addresses
 * 07/24/2006 3.3.3.0 - Added support for SHTTRAN Transcript Request table
 * 03/24/2007 3.3.4.0 - Pass company name into verify address for Batch_Verify_SPRADDR
 * 05/16/2007 3.3.5.0 - Contatenate Phone Extension to Phone Number if Ext is 4 characters and Number is 3 characters
 *                    - Fix warning message sometimes when new area code is same as existing area code
 * 03/01/2008 3.5.0.0
 *   - Added check to Batch procecdures to not override "user overrridden" addresses containing error code "!"
 *   - Added Set_Object_Name and Get_Object_Name procedures for returning the Banner Object
 *     that is calling the verification procedure
 *   - Added Verify_Address_AJAX procedure for supporting Banner SSB instantaneous feedback
 *   - Added fv_object_name to all Verify procedures and in calling INB Form / SSB pages
 *     - The object name is in the format FORM.DATA_BLOCK, PACKAGE.PROCEDURE,
 *       AJAX.PACKAGE.PROCEDURE[.address_block] in upper case
 *     - For Batch procedures, the Object name will be the procedure name
 *   - Added Set_Ignore_Error and Get_Ignore_Error functions
 *     - Call CLEAN_Address_Banner.Set_Ignore_Error(TRUE) to allow Web Self-Service to save data even with an error code
 *     NOTE: The error code will still be stored on the record but allow the transaction to continue
 *   - Added gv_Local Country for setting the Banner specific Nation Code for the predominant country.
 *     - This is used for skipping international addresses in batch processing
 *   - Added fd_From_Date and fd_To_Date parameters to batch procedures to enable running parallel batch processes
 *   - Added fb_Verify_Inactive parameter to batch procedures for tables that contain a status indicator
 *   - Added fb_Skip_International parameter to all batch procedures so users can limit number of Web Service transactions
 *     NOTE: The Pre_Address_Verify user exit procedure needs to check for the CLEAN_Address_Banner.Get_Skip_International
 *           function call based on the local country code value.  This is predominantly for interactive checking.
 *     NOTE: You can also call CLEAN_Address_Banner.Set_Local_Country('US') to define the Banner "local" Nation Code.
 *           This is checked automatically in the batch procedures.
 *   - Fixed issue with international addresses with "Percent Match" error message and some international addresses not updating
 * 04/25/2008 3.5.0.1 - Fixed issue with port numbers in Get_Current_URL function
 * 05/02/2008 3.5.0.2 - Created new procedures for Get_Max_Address_Length and Set_Max_Address_Length
 *                      to set soft limits for address length in Banner 8
 * 05/12/2008 3.5.0.3 - Use trunc() around fd_from_date and fd_to_date in the batch verify procedures
 *                    - Fixed issue of country code not being passed to pre_address_verify procedure
 *                    - Fix to don't append suite for international addresses
 *                    - Fix State append issue for long city names
 * 05/15/2008 3.5.0.4 - Fix state for international addresses from being put in state field
 * 07/21/2008 3.5.0.5 - Added new parameters to Batch_Verify_SPRADDR: fb_set_activity_date_user and fv_set_source_code
 * 09/11/2008 3.5.0.6 - Added a new procedure Set_Current_URL to override the default URL for AJAX calls as returned from Get_Current_URL
 * 10/03/2008 3.5.0.7 - Added a user exit call in Set_Current_URL to override the default URL
 *                    - NOTE: Set_Override_URL needs to be added to the user exit package to compile correctly
 * 12/18/2008 3.5.0.8 - Added check to "NOT update existing" logic to create a new address record even if there is an error
 * 02/20/2009 3.5.0.9 - Added fb_prefix_state_on_county_code parameter to sync_postal_codes to force state prefix when STVCNTY is empty
 * 04/24/2009 3.5.0.10 - Added fb_use_unique_postal_codes parameter to allow multiple cities to be synced, or only preferred
 * 6/18/2009  3.5.1.0 - Fixed issue with non-suite fields appending to address lines - address swapping issue
 * 7/10/2009  3.6.0.0 - Added support for Banner 8.2 new fields: Address_Line4, House_Number
 *                    - Removed legacy procedures: Verify_Address_User_Exit
 * 7/27/09    3.6.0.1 - Added update for FTVVEND and GXRDIRD tables which rely on seqno for direct deposit and purchase orders
 *                    - Only when the address record is being appended to SPRADDR in batch mode; Normally existing record is updated instead of appended for batch_verify_SPRADDR
 * 7/28/09    3.6.0.2 - Fixed compile issue with some CLEAN_Address installations
 * 7/29/09    3.6.0.3 - Fixed compile issue with some CLEAN_Address installations for SPTADDR, SHTTRAN
 *                    - Added overloaded Verify procedure for legacy support of forms
 * 7/31/09    3.6.0.4 - Always put address after any non-suite field - i.e. attention above address
 * 8/21/09    3.6.0.5 - Check for phone number split between area code and phone number field
 * 9/23/09    3.6.0.6 - Fix for expanding street suffix when there is no suite on the address
 * 1/25/10    3.6.0.7 - Added fb_vendor update procedure call to keep vendor addresses in sync with SciQuest
 *****************************************************************************************
 * Copyright (c) 2004-2009 Runner Technologies, Inc.  All Rights Reserved.
 * www.RunnerTechnologies.com   (877)784-0003  561-395-9322
 *****************************************************************************************/
-----------------------------------------------------------------------------------------
-- GLOBAL VARIABLES
-----------------------------------------------------------------------------------------
-- - NOTE: These parameters can also be set dynamically in the Set_ procedure calls
--         in the CLEAN_Address_Banner package
-----------------------------------------------------------------------------------------
--
-- Set gb_expand_street_suffix to TRUE to expand the street suffix (Ave -> Avenue) if it will
-- fit on one line.  If set to FALSE, the USPS CASS standardization rules will be used.
-- DEFAULT: FALSE
gb_Expand_Street_Suffix       BOOLEAN := FALSE;
--
-- Set gb_suite_apt_before_street to TRUE to put the suite/apartment line before the address line
-- in cases where the the suite/apartment does not fit on one line.  If set to FALSE, the suite/apt
-- will appear on the line after the street address line.
-- DEFAULT: FALSE
gb_Suite_Apt_Before_Street    BOOLEAN := FALSE;
--
-- Set gb_append_suite_to_street to TRUE to always "try" to append the suite/apartment line
-- on the same line as the the address line - only when it will fit.  If set to FALSE, the
-- suite/apt will be on a separate line if there a free line available
-- DEFAULT: TRUE
gb_Append_Suite_To_Street     BOOLEAN := TRUE;
--
-- Set gb_use_ZIP_Plus4 to TRUE to append the ZIP+4 digits after the ZIP Code in the
-- SPRADDR_ZIP field.  If set to FALSE, only the 5-digit ZIP Code will be used
-- DEFAULT: TRUE
gb_Use_ZIP_Plus4              BOOLEAN := TRUE;
--
-- Set gb_batch_update_existing to TRUE to update the existing address record when running
-- in batch mode.  If set to FALSE, a new record will be created if anything changes in the
-- address.  This could easily double the size of the SPRADDR table if set FALSE, since most
-- addresses are not entered in the USPS standards. Please take this into consideration.
-- DEFAULT: TRUE
gb_Batch_Update_Existing      BOOLEAN := TRUE;
--
-- Set gn_telephone_distance_limit to the number of miles threshhold to allow before an
-- alert message is displayed to the user.  This is the distance between the telephone
-- wire center (area code and prefix = first 6 digits) and the 5-digit ZIP Code
-- DEFAULT: 100
gn_Telephone_Distance_Limit   NUMBER  := 100;
--
-- Set gn_address_lines to the number of address lines to be processed
-- DEFAULT: 3 for Pre-Banner 8.2
-- DEFAULT: 4 for Banner 8.2 and above - set in Verify_Address_Record procedure
gn_Address_Lines              NUMBER  := 3;
--
-- Set gn_max_address_length to the size of the address fields in SPRADDR (currently 30)
-- DEFAULT: 30
gn_Max_Address_Length         NUMBER  := 30;
--
-- Set gn_max_city_length to the size of the city field in Address table (currently 20)
-- DEFAULT: 20
gn_Max_City_Length            NUMBER  := 20;
--
-- Set gn_error_suggest_count to the number of possible matches to return for unverified addresses
gn_Error_Suggest_Count        PLS_INTEGER := 0;
--
-- Set gb_Upper_Case_Results to upper case all address results
gb_Upper_Case_Results         BOOLEAN := FALSE;
--
-- Set gb_Ignore_Error to TRUE to allow Web Self-Service to save data even with an error code
-- NOTE: The error code will still be stored on the record but allow the transaction to continue
-- The functions Set_Ignore_Error and Get_Ignore_Error are used for this variable
gb_Ignore_Error               BOOLEAN := FALSE;
--
-- gv_Object_Name contains the Object name that is currently calling the verification procedure
gv_Object_Name                VARCHAR2(100) := NULL;
--
-- Set gv_Local_Country to the Banner specific Nation Code for the predominant country
gv_Local_Country              VARCHAR2(100) := 'US';
--
-- Set gb_Skip_International TRUE to not process international address records for interactive processing
-- This allows more control on the purchased Web Service transactions.
-- NOTE: User should pass this parameter into the batch procedure rather than setting directly
gb_Skip_International         BOOLEAN := TRUE;
--
-- Set the current URL for AJAX calls from SSB - used only to override the default function Get_Current_URL
gv_current_url                VARCHAR2(200) := NULL;
--
-------------------------------------------------------------------------------
--
-- TYPES and PROCEDURES for managing result output
--
-------------------------------------------------------------------------------
-- Declare results type table for holding summary
TYPE RESULTS_REC IS RECORD (
   code             VARCHAR2(10)
  ,description      VARCHAR2(100)
  ,rec_count        NUMBER
  );
TYPE RESULTS_TAB IS TABLE OF RESULTS_REC INDEX BY BINARY_INTEGER;
gt_results        RESULTS_TAB;
gi_total          NUMBER := 0;
-----------------------------------------------------------------------------------------
-- PRIVATE PROCEDURES
-----------------------------------------------------------------------------------------
/******************************************************************************************
 *  Procedure Name  :   Add_Result
 *
 *  Scope           :   PRIVATE
 *
 *  Description     :   Add a result code and string to internal buffer
 ******************************************************************************************/
PROCEDURE Add_Result (
                   pv_code        IN VARCHAR2
                  ,pv_description IN VARCHAR2
                  )
IS
  lb_found BOOLEAN := FALSE;
  li       BINARY_INTEGER;
BEGIN
  gi_total := gi_total + 1;
  FOR li in 1..gt_results.COUNT LOOP
    if gt_results(li).code = NVL(pv_code, '.') then
      gt_results(li).rec_count := gt_results(li).rec_count + 1;
      lb_found := TRUE;
      exit;
    end if;
  END LOOP;
  if NOT lb_found then
    li := gt_results.COUNT + 1;
    gt_results(li).code        := NVL(pv_code, '.');
    gt_results(li).description := NVL(substr(pv_description,1,100), '.');
    gt_results(li).rec_count   := 1;
  end if;
END Add_Result;
/******************************************************************************************
 *  Procedure Name  :   Delete_Results
 *
 *  Scope           :   PRIVATE
 *
 *  Description     :   Delete the results table in memory
 ******************************************************************************************/
PROCEDURE Delete_Results
IS
BEGIN
  gt_results.DELETE;
  gi_total := 0;
END Delete_Results;
/******************************************************************************************
 *  Procedure Name  :   Show_Results
 *
 *  Scope           :   PRIVATE
 *
 *  Description     :   Show the results table in DBMS_OUTPUT
 ******************************************************************************************/
PROCEDURE Show_Results
IS
  li       BINARY_INTEGER;
  li_total BINARY_INTEGER := 0;
BEGIN
  dbms_output.put_line('---------------------------');
  dbms_output.put_line('--     ERROR SUMMARY     --');
  dbms_output.put_line('---------------------------');
  dbms_output.put_line(
        rpad('Code' ,11)
      ||rpad('Description', 51)
      ||'Count'
      );
  dbms_output.put_line(
        rpad('-' ,10,'-')||' '
      ||rpad('-', 50,'-')||' '
      ||'------------------'
      );
  FOR li in 1..gt_results.COUNT LOOP
    if gi_total = 0 then
      dbms_output.put_line(
          rpad(NVL(gt_results(li).code,'.'),11)
        ||rpad(substr(gt_results(li).description,1,50),51)
        ||to_char(gt_results(li).rec_count)
        );
    else
      dbms_output.put_line(
          rpad(NVL(gt_results(li).code,'.'),11)
        ||rpad(substr(gt_results(li).description,1,50),51)
        ||to_char(gt_results(li).rec_count)
        ||' ('||to_char(round(100*gt_results(li).rec_count/gi_total,1))||'%)'
        );
    end if;
    li_total := li_total + gt_results(li).rec_count;
    if length(gt_results(li).description) > 50 then
      dbms_output.put_line(
          rpad(' ',11)
        ||rpad(substr(gt_results(li).description,51,50),51)
        );
    end if;
    if length(gt_results(li).description) > 100 then
      dbms_output.put_line(
          rpad(' ',11)
        ||rpad(substr(gt_results(li).description,101,50),51)
        );
    end if;
  END LOOP;
  -- Show total
  dbms_output.put_line(
        rpad('-' ,10,'-')||' '
      ||rpad('-', 50,'-')||' '
      ||'----------'
      );
  dbms_output.put_line(
        rpad('Total:' ,61)||' '
      ||to_char(li_total)
      );
END Show_Results;
/******************************************************************************************
 *  Procedure Name  :   Trim_Address_Line
 *
 *  Scope           :   PRIVATE
 *
 *  Description     :   Do a smart trim on address lines over gn_Max_Address_Length characters
 *                      instead of truncing
 *                      - trim at the last space before the max length
 ******************************************************************************************/
PROCEDURE Trim_Address_Line (
                     fv_address                     IN OUT VARCHAR2
                    ,fn_max_length                  IN     NUMBER   DEFAULT NULL
                    )
IS
  ln_space  PLS_INTEGER;
  ln_max    PLS_INTEGER;
BEGIN
  ln_max := NVL(fn_max_length, gn_Max_Address_Length);
  WHILE NVL(length(fv_address),0) > ln_max LOOP
    ln_space := instr(substr(fv_address,1,ln_max+1), ' ', -1);
    if ln_space > 1 then
      -- trim at the last space
      fv_address := substr(fv_address, 1, ln_space-1);
    else
      -- last resort - do the trunc
      fv_address := substr(fv_address, 1, ln_max);
    end if;
  END LOOP;
END Trim_Address_Line;
-----------------------------------------------------------------------------------------
-- PUBLIC PROCEDURES
-----------------------------------------------------------------------------------------
/******************************************************************************************
 *  Procedure Name  :   Verify_Address_Record
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Verify a single address for Banner
 *                      - accepts CLEAN_Address Address record type as input
 ******************************************************************************************/
PROCEDURE Verify_Address_Record (
                     f_address_rec                  IN OUT CLEAN_Address.Address_rec
                    ,fv_plsql_error                 IN OUT VARCHAR2
                    ,fv_address_type                IN     VARCHAR2 DEFAULT NULL /* SPRADDR_ATYP_CODE */
                    ,fv_object_name                 IN     VARCHAR2 DEFAULT NULL /* Object Name where procedure is called from */
                    )
IS
  --
  -- Cursor to get the Banner county based on state and county name
  --
  cursor c_county (pv_county_code VARCHAR2
                  ,pv_county_name VARCHAR2) IS
    select STVCNTY_CODE
    from   stvcnty
    where  upper(STVCNTY_DESC) like upper(pv_county_name)
    and    STVCNTY_CODE like pv_county_code;
  -- cursor to get the county information from cln_postal_codes
  -- if county code is not returned
  cursor c_postal_county (pv_postal_code VARCHAR2) IS
    select gtvzipc_cnty_code
    from   gtvzipc
    where  gtvzipc_code = pv_postal_code;
  --
  -- Cursor to get the Banner Nation Name based on country code
  --
  cursor c_nation (pv_country_code VARCHAR2) IS
    select STVNATN_NATION
    from   stvnatn
    where  STVNATN_CODE = pv_country_code;
  lv_county_code         VARCHAR2(100) := NULL;
  lv_country_code_backup VARCHAR2(100) := NULL;
  lv_new_county_code     VARCHAR2(100) := NULL;
  lb_verified            BOOLEAN       := FALSE;
  lb_suite_appended      BOOLEAN       := FALSE;
  lv_parsed_address      VARCHAR2(100);
  lb_company_input       BOOLEAN       := FALSE;
  l_address_rec_bak      CLEAN_Address.Address_rec;
BEGIN
  fv_plsql_error := NULL;
  -- 3/6/08 - Set the object name so user exit procedure can identify where the call was made from
  Set_Object_Name(fv_object_name);
  -- Backup existing Banner county code
  lv_county_code         := f_address_rec.county_code;
  lv_country_code_backup := f_address_rec.country_code;
  l_address_rec_bak      := f_address_rec;
  -- 3/1/08 - Get the Nation name for international address verifications instead of the code
  --          because many institutions do not use the ISO-2, ISO-3, or ISO-Numeric format for country code
  if     f_address_rec.country_code IS NOT NULL
     -- Don't pull country name back for US and Canada since that data is accessed locally
     and f_address_rec.country_code NOT IN ('US', 'USA', 'U.S.', 'U.S.A.', '840', '157')
     and f_address_rec.country_code NOT IN ('CA', 'CAN', 'CANADA', '124')
     then
    open c_nation(f_address_rec.country_code);
    fetch c_nation
    into  f_address_rec.country_name;
    close c_nation;
  end if;
  -- Set boolean if Company is present on Input - use for verification but don't add to address lines
  if f_address_rec.company IS NOT NULL then
    lb_company_input := TRUE;
  end if;
  --
  -- Verify the address
  --
  BEGIN
    /*PREBANNER82*/gn_Address_Lines := 4;
    --
    -- Use the House Number if no address lines are specified
    --
    /*PREBANNER82*/if f_address_rec.Address_Line_1||f_address_rec.Address_Line_2||f_address_rec.Address_Line_3||f_address_rec.Address_Line_4 IS NULL
    /*PREBANNER82*/   and f_address_rec.Parsed_Address_Number IS NOT NULL then
    /*PREBANNER82*/  f_address_rec.Address_Line_1 := f_address_rec.Parsed_Address_Number;
    /*PREBANNER82*/end if;
    --
    -- Pre Address Verify User Exit
    -- - Make any custom changes in the CLEAN_Address_Banner_UE User Exit package
    --
    CLEAN_Address_Banner_UE.Pre_Address_Verify(
             f_address_rec     => f_address_rec
            ,fv_plsql_error    => fv_plsql_error
            ,fv_address_type   => fv_address_type
            ,fb_verified       => lb_verified
            );
    --
    -- Exit if the address was verified or overridden in the User Exit
    --
    if lb_verified then
      return;
    end if;
    --
    -- Remove country code for verifying international addresses - use country name exclusively for better match
    --
    if lv_country_code_backup IS NOT NULL
       and f_address_rec.country_name IS NOT NULL then
      f_address_rec.country_code := NULL;
    end if;
    -- 7/31/09 - Fix Attn being dropped from line 3
    if upper(f_address_rec.Address_Line_3) like 'ATTN%'
       and trim(f_address_rec.Address_Line_4) IS NULL
       then
      f_address_rec.Address_Line_4 := f_address_rec.Address_Line_3;
      f_address_rec.Address_Line_3 := f_address_rec.Address_Line_2;
      f_address_rec.Address_Line_2 := f_address_rec.Address_Line_1;
      f_address_rec.Address_Line_1 := f_address_rec.Address_Line_4;
      f_address_rec.Address_Line_4 := NULL;
    end if;
    -- 7/31/09 - Fix "P O Box" duplication
    if upper(f_address_rec.Address_Line_1) like 'P O BOX%' then
      f_address_rec.Address_Line_1 := 'PO Box '||substr(f_address_rec.Address_Line_1, 8);
    end if;
    if upper(f_address_rec.Address_Line_2) like 'P O BOX%' then
      f_address_rec.Address_Line_2 := 'PO Box '||substr(f_address_rec.Address_Line_2, 8);
    end if;
    if upper(f_address_rec.Address_Line_3) like 'P O BOX%' then
      f_address_rec.Address_Line_3 := 'PO Box '||substr(f_address_rec.Address_Line_3, 8);
    end if;
    -- 7/31/09 - Fix Address on first line of 3 lines issue
    if     substr(trim(f_address_rec.Address_Line_1),1,1)     between '0' and '9'
       and substr(trim(f_address_rec.Address_Line_2),1,1) NOT between '0' and '9'
       and substr(trim(f_address_rec.Address_Line_3),1,1) NOT between '0' and '9'
       and trim(f_address_rec.Address_Line_4) IS NULL
       then
      f_address_rec.Address_Line_4 := f_address_rec.Address_Line_1;
      f_address_rec.Address_Line_1 := f_address_rec.Address_Line_2;
      f_address_rec.Address_Line_2 := f_address_rec.Address_Line_3;
      f_address_rec.Address_Line_3 := f_address_rec.Address_Line_4;
      f_address_rec.Address_Line_4 := NULL;
    end if;
    --
    -- Process an Address with Standardized component fields
    --
    CLEAN_Address.Verify_Generic(
           f_address_rec              => f_address_rec
          ,fn_number_address_lines    => gn_Address_Lines
          ,fb_generic_address_name    => FALSE
          ,fb_generic_company         => FALSE
          ,fb_address_suite_same_line => FALSE
          ,fn_Error_Suggest_Count     => gn_Error_Suggest_Count
          ,fb_Verbose_Suggest_list    => FALSE
          ,fb_upper_case_results      => gb_Upper_Case_Results
          );
    -- Set the error string to NULL if there is no error
    if upper(f_address_rec.error_string) = 'NO ERROR' then
      f_address_rec.error_string := NULL;
    end if;
    --
    -- Fix the last line if present in the returned address line
    -- Don't return line 4 as the city/state/ZIP line (last_line)
    --
    if f_address_rec.address_line_4 = f_address_rec.last_line then
      f_address_rec.address_line_4 := NULL;
      -- 7/31/09 check for missing suite issue
      if f_address_rec.suite IS NOT NULL
         and f_address_rec.Address_Line_1||' '||f_address_rec.Address_Line_2||' '||f_address_rec.Address_Line_3 NOT LIKE '%'||f_address_rec.suite||'%'
         then
        f_address_rec.address_line_4 := f_address_rec.suite;
      end if;
    end if;
    -- 7/31/09 Fix duplicate Company name issue
    if NOT lb_company_input
       and ( f_address_rec.company = f_address_rec.Address_Line_1
           or f_address_rec.company = f_address_rec.Address_Line_2
           or f_address_rec.company = f_address_rec.Address_Line_3
           or f_address_rec.company = f_address_rec.Address_Line_4
           )
       then
      f_address_rec.company := NULL;
    end if;
    -- 1/18/06 - PMB Fix for Private Mailboxes
    if f_address_rec.private_mailbox IS NOT NULL
       and NVL(f_address_rec.suite,'x') != f_address_rec.private_mailbox then
      f_address_rec.private_mailbox := replace(f_address_rec.private_mailbox, 'Pmb ', 'PMB ');
      if f_address_rec.suite IS NULL then
        f_address_rec.suite := f_address_rec.private_mailbox;
        if f_address_rec.address_line_2 IS NULL then
          f_address_rec.address_line_2 := f_address_rec.private_mailbox;
        elsif f_address_rec.address_line_3 IS NULL then
        f_address_rec.address_line_3 := f_address_rec.private_mailbox;
        end if;
      elsif f_address_rec.Address_Name IS NULL then
        f_address_rec.Address_Name := f_address_rec.private_mailbox;
      elsif f_address_rec.Company IS NULL then
        f_address_rec.Company := f_address_rec.private_mailbox;
      elsif f_address_rec.Attention IS NULL then
        f_address_rec.Attention := f_address_rec.private_mailbox;
      end if;
    end if;
    -- 3/1/08 - Make sure address data was returned for international verification, otherwise restore original record
    if   f_address_rec.Address_Line_1||f_address_rec.Address_Line_2||f_address_rec.Address_Line_3
       ||f_address_rec.City||f_address_rec.State||f_address_rec.Postal_code IS NULL then
      -- Restore original input address
      f_address_rec.Company        := l_address_rec_bak.company;
      f_address_rec.Address_Line_1 := l_address_rec_bak.address_line_1;
      f_address_rec.Address_Line_2 := l_address_rec_bak.address_line_2;
      f_address_rec.Address_Line_3 := l_address_rec_bak.address_line_3;
      f_address_rec.Address_Line_4 := l_address_rec_bak.address_line_4;
      f_address_rec.Parsed_Address_Number := l_address_rec_bak.Parsed_Address_Number;
      f_address_rec.City           := l_address_rec_bak.City;
      f_address_rec.State          := l_address_rec_bak.State;
      f_address_rec.Province       := l_address_rec_bak.Province;
      f_address_rec.Postal_code    := l_address_rec_bak.postal_code;
      f_address_rec.country_code   := l_address_rec_bak.country_code;
    end if;
    -- Restore the backed up country code in case it was reset by international address verification
    if lv_country_code_backup IS NOT NULL
       and NVL(f_address_rec.country_code,lv_country_code_backup) != lv_country_code_backup
       and NVL(f_address_rec.country_code,'US') != 'US'
       then
      f_address_rec.country_code := lv_country_code_backup;
    end if;
    --
    -- Post Address Verify User Exit
    -- - Make any custom changes in the CLEAN_Address_Banner_UE User Exit package
    --
    CLEAN_Address_Banner_UE.Post_Address_Verify(
           f_address_rec     => f_address_rec
          ,fv_plsql_error    => fv_plsql_error
          ,fv_address_type   => fv_address_type
          ,fb_verified       => lb_verified
          );
    --
    -- Exit if the address was verified or overridden in the User Exit
    --
    if lb_verified then
      return;
    end if;
    --
    -- Append the ZIP+4 if returned
    --
    if f_address_rec.POSTAL_CODE_EXTENDED IS NOT NULL
       -- Only use Zip+4 if the parameter is set to use it
       and gb_Use_ZIP_Plus4 then
      f_address_rec.POSTAL_CODE := f_address_rec.POSTAL_CODE||'-'||f_address_rec.POSTAL_CODE_EXTENDED;
    end if;
    --
    -- Fix International Addresses where Address 1-4 contains city, province, postal code, country
    --
    if NVL(f_address_rec.Status_Code,'x') like 'AD%' then
      --
      -- GLOBAL Address
      --
      -- Only use new address if we have a valid city or postal_code
      if    f_address_rec.city IS NOT NULL
         or f_address_rec.postal_code IS NOT NULL
         then
        if f_address_rec.address IS NOT NULL then
          f_address_rec.address_line_1 := f_address_rec.address;
          f_address_rec.address_line_2 := f_address_rec.suite;
          f_address_rec.address_line_3 := NULL;
          f_address_rec.address_line_4 := NULL;
        end if;
        -- Append State to City for international addresses since state field not long enough - except for Canada
        if NVL(length(NVL(f_address_rec.State, f_address_rec.Province)),0) > 0
           and upper(NVL(f_address_rec.country_name,'x')) != 'CANADA' then
          if length(f_address_rec.city||', '||NVL(f_address_rec.State, f_address_rec.Province)) <= gn_Max_City_Length then
            -- try to append state to city field if it will fit
            f_address_rec.city := f_address_rec.city||', '||NVL(f_address_rec.State, f_address_rec.Province);
            f_address_rec.state := NULL;
            f_address_rec.province := NULL;
          elsif f_address_rec.address_line_2 IS NULL
            and length(NVL(f_address_rec.State, f_address_rec.Province)) <= gn_Max_Address_Length
            and NVL(upper(f_address_rec.suite),'x') != upper(NVL(f_address_rec.State, f_address_rec.Province)) then
            f_address_rec.address_line_2 := NVL(f_address_rec.State, f_address_rec.Province);
            f_address_rec.state := NULL;
            f_address_rec.province := NULL;
          elsif f_address_rec.address_line_3 IS NULL
            and length(NVL(f_address_rec.State, f_address_rec.Province)) <= gn_Max_Address_Length
            and NVL(upper(f_address_rec.suite),'x') != upper(NVL(f_address_rec.State, f_address_rec.Province)) then
            f_address_rec.address_line_3 := NVL(f_address_rec.State, f_address_rec.Province);
            f_address_rec.state := NULL;
            f_address_rec.province := NULL;
/*PREBANNER82*/          elsif f_address_rec.address_line_4 IS NULL
/*PREBANNER82*/            and length(NVL(f_address_rec.State, f_address_rec.Province)) <= gn_Max_Address_Length
/*PREBANNER82*/            and NVL(upper(f_address_rec.suite),'x') != upper(NVL(f_address_rec.State, f_address_rec.Province)) then
/*PREBANNER82*/            f_address_rec.address_line_4 := NVL(f_address_rec.State, f_address_rec.Province);
/*PREBANNER82*/            f_address_rec.state := NULL;
/*PREBANNER82*/            f_address_rec.province := NULL;
          else
            -- default state to NULL if it won't fit - international states not in STVSTAT
            f_address_rec.state := NULL;
            f_address_rec.province := NULL;
          end if;
        end if;
      end if;
    end if;
    --
    -- Replace # sign with Apt on address lines because # confuses downstream printing applications
    -- Make sure address does not have APT already in it
    -- And only if this is not a "Unique" ZIP Code - ZIP+4 = 0001
    --
    if NVL(f_address_rec.Postal_Code_Extended,'0001') != '0001' then
      if instr(f_address_rec.address_line_1, '#') > 0 then
        if instr(upper(f_address_rec.address_line_1), 'APT') = 0 then
          f_address_rec.address_line_1 := replace(f_address_rec.address_line_1, '#', 'Apt');
          f_address_rec.suite          := replace(f_address_rec.suite, '#', 'Apt');
        else
          f_address_rec.address_line_1 := replace(replace(f_address_rec.address_line_1, '#', ' '), '  ', ' ');
          f_address_rec.suite          := trim(replace(replace(f_address_rec.suite, '#', ' '), '  ', ' '));
        end if;
      end if;
      if instr(f_address_rec.address_line_2, '#') > 0 then
        if instr(upper(f_address_rec.address_line_2), ' APT') = 0 then
          f_address_rec.address_line_2 := replace(f_address_rec.address_line_2, '#', 'Apt');
          f_address_rec.suite          := replace(f_address_rec.suite, '#', 'Apt');
        else
          f_address_rec.address_line_2 := replace(replace(f_address_rec.address_line_2, '#', ' '), '  ', ' ');
          f_address_rec.suite          := trim(replace(replace(f_address_rec.suite, '#', ' '), '  ', ' '));
        end if;
      end if;
      if instr(f_address_rec.address_line_3, '#') > 0 then
        if instr(upper(f_address_rec.address_line_3), 'APT') = 0 then
          f_address_rec.address_line_3 := replace(f_address_rec.address_line_3, '#', 'Apt');
          f_address_rec.suite          := replace(f_address_rec.suite, '#', 'Apt');
        else
          f_address_rec.address_line_3 := replace(replace(f_address_rec.address_line_3, '#', ' '), '  ', ' ');
          f_address_rec.suite          := trim(replace(replace(f_address_rec.suite, '#', ' '), '  ', ' '));
        end if;
      end if;
      if instr(f_address_rec.address_line_4, '#') > 0 then
        if instr(upper(f_address_rec.address_line_4), 'APT') = 0 then
          f_address_rec.address_line_4 := replace(f_address_rec.address_line_4, '#', 'Apt');
          f_address_rec.suite          := replace(f_address_rec.suite, '#', 'Apt');
        else
          f_address_rec.address_line_4 := replace(replace(f_address_rec.address_line_4, '#', ' '), '  ', ' ');
          f_address_rec.suite          := trim(replace(replace(f_address_rec.suite, '#', ' '), '  ', ' '));
        end if;
      end if;
    end if;
    --
    -- Truncate the Address Lines if they are too long - avoid database error
    -- - Truncate rather than wrap to avoid directionals being confused as apartment numbers
    --
    Trim_Address_Line(f_address_rec.address_line_1);
    Trim_Address_Line(f_address_rec.address_line_2);
    Trim_Address_Line(f_address_rec.address_line_3);
    Trim_Address_Line(f_address_rec.address_line_4);
    Trim_Address_Line(f_address_rec.address);
    --
    -- See if we should expand the street suffix
    --
    lv_parsed_address := NULL;
    if gb_expand_street_suffix
       and f_address_rec.Parsed_Street_Suffix IS NOT NULL
       and length(f_address_rec.address) <= gn_Max_Address_Length
       then
      --
      -- Expand the street suffix into a temporary address line
      --
      lv_parsed_address := CLEAN_Address.Expand_Street_Suffix (
                                 fv_address       => f_address_rec.address
                                ,fv_street_suffix => f_address_rec.Parsed_Street_Suffix
                                );
    end if;
    --
    -- Suite / Apartment Business Rules
    --
    if f_address_rec.suite IS NOT NULL
       -- 5/12/08 - don't append suite for international addresses
       and NVL(f_address_rec.Status_Code,'x') NOT like 'AD%' then
      --
      -- Check global parameter to see if we should append suite/apt to address line
      --
      if     f_address_rec.suite not like 'PO %'
         and f_address_rec.Address not like 'PO %'
         -- 6/18/09 - Make sure Unit exists before appending suite
         and f_address_rec.Parsed_Unit_Type IS NOT NULL
         and (   gb_Append_Suite_To_Street
             -- Always append suite if we have extra address fields in Company AND Address_Name that
             -- we need to add back in
             or (    f_address_rec.Company IS NOT NULL
                 and NOT lb_company_input
                 and f_address_rec.Address_Name IS NOT NULL
                )
             or (    f_address_rec.Attention IS NOT NULL
                 and lb_company_input
                 and f_address_rec.Address_Name IS NOT NULL
                )
             or (    f_address_rec.Company IS NOT NULL
                 and NOT lb_company_input
                 and f_address_rec.Attention IS NOT NULL
                )
            )
         then
        --------------------------------------------------
        -- PARSED ADDRESS - STREET SUFFIX EXPANSION
        --
        -- Put the suite on the same line as the address if there is room - and suite is on line 2
        --
        if f_address_rec.suite = f_address_rec.address_line_2
           and lv_parsed_address IS NOT NULL
           and length(lv_parsed_address||' '||f_address_rec.address_line_2) <= gn_Max_Address_Length
           then
          f_address_rec.address_line_1 := lv_parsed_address||' '||f_address_rec.address_line_2;
          f_address_rec.address_line_2 := f_address_rec.address_line_3;
          f_address_rec.address_line_3 := NULL;
          f_address_rec.address_line_4 := NULL;
          lb_suite_appended := TRUE;
          f_address_rec.address := lv_parsed_address;
          lv_parsed_address := NULL;
        end if;
        --
        -- Put the suite on the same line as the address if there is room - and suite is on line 3
        --
        if f_address_rec.suite = f_address_rec.address_line_3
           and lv_parsed_address IS NOT NULL
           and length(lv_parsed_address||' '||f_address_rec.address_line_3) <= gn_Max_Address_Length
           then
          f_address_rec.address_line_2 := f_address_rec.address_line_2||' '||f_address_rec.address_line_3;
          f_address_rec.address_line_3 := NULL;
          f_address_rec.address_line_4 := NULL;
          lb_suite_appended := TRUE;
          f_address_rec.address := lv_parsed_address;
          lv_parsed_address := NULL;
        end if;
        --------------------------------------------------
        -- NON-PARSED ADDRESS - NO STREET SUFFIX EXPANSION
        --
        -- Put the suite on the same line as the address if there is room - and suite is on line 2
        --
        if f_address_rec.suite = f_address_rec.address_line_2
           and length(f_address_rec.address_line_1||' '||f_address_rec.address_line_2) <= gn_Max_Address_Length
           then
          f_address_rec.address_line_1 := f_address_rec.address_line_1||' '||f_address_rec.address_line_2;
          f_address_rec.address_line_2 := f_address_rec.address_line_3;
          f_address_rec.address_line_3 := NULL;
          f_address_rec.address_line_4 := NULL;
          lb_suite_appended := TRUE;
        end if;
        --
        -- Put the suite on the same line as the address if there is room - and suite is on line 3
        --
        if f_address_rec.suite = f_address_rec.address_line_3
           and length(f_address_rec.address_line_2||' '||f_address_rec.address_line_3) <= gn_Max_Address_Length
           then
          f_address_rec.address_line_2 := f_address_rec.address_line_2||' '||f_address_rec.address_line_3;
          f_address_rec.address_line_3 := NULL;
          f_address_rec.address_line_4 := NULL;
          lb_suite_appended := TRUE;
        end if;
      end if;
      --
      -- Make sure the parsed address line is <= gn_Max_Address_Length characters
      --
      if lv_parsed_address IS NOT NULL
         and length(lv_parsed_address) <= gn_Max_Address_Length then
        if    f_address_rec.address_line_1 = f_address_rec.address then
          f_address_rec.address_line_1 := lv_parsed_address;
        elsif f_address_rec.address_line_2 = f_address_rec.address then
          f_address_rec.address_line_2 := lv_parsed_address;
        elsif f_address_rec.address_line_3 = f_address_rec.address then
          f_address_rec.address_line_3 := lv_parsed_address;
        elsif f_address_rec.address_line_4 = f_address_rec.address then
          f_address_rec.address_line_4 := lv_parsed_address;
        end if;
        f_address_rec.address := lv_parsed_address;
        lv_parsed_address := NULL;
      end if;
      --
      -- See if we should switch the suite/apartment line above the address line
      --
      if    (    gb_suite_apt_before_street
             -- 7/10/09 - Make sure Unit exists before adjusting suite
             and f_address_rec.Parsed_Unit_Type IS NOT NULL
             and NOT lb_suite_appended
            )
         -- 7/31/09 Always put address after any non-suite field - i.e. attention above address - except for unique zipcodes 0001
         or (    f_address_rec.Parsed_Unit_Type IS NULL
             and f_address_rec.suite IS NOT NULL
             and f_address_rec.address IS NOT NULL
             and f_address_rec.postal_code_extended != '0001'
             and f_address_rec.postal_code_extended != '0002'
             and NOT lb_suite_appended
            )
         then
        --
        -- Put the suite on line 1 if it is on line 2
        --
        if f_address_rec.address = f_address_rec.address_line_1
           and f_address_rec.suite = f_address_rec.address_line_2
           then
          f_address_rec.address_line_1 := f_address_rec.suite;
          f_address_rec.address_line_2 := f_address_rec.address;
        end if;
        --
        -- Put the suite on line 2 if it is on line 3
        --
        if f_address_rec.address = f_address_rec.address_line_2
           and f_address_rec.suite = f_address_rec.address_line_3
           then
          f_address_rec.address_line_2 := f_address_rec.suite;
          f_address_rec.address_line_3 := f_address_rec.address;
        end if;
        --
        -- Put the suite on line 3 if it is on line 4
        --
        if f_address_rec.address = f_address_rec.address_line_3
           and f_address_rec.suite = f_address_rec.address_line_4
           then
          f_address_rec.address_line_3 := f_address_rec.suite;
          f_address_rec.address_line_4 := f_address_rec.address;
        end if;
      end if;
    end if;
    --
    -- Handle the expanded street suffix buffer when there is no suite
    -- Make sure the parsed address line is <= gn_Max_Address_Length characters
    --
    if lv_parsed_address IS NOT NULL
       and length(lv_parsed_address) <= gn_Max_Address_Length then
      if    f_address_rec.address_line_1 = f_address_rec.address then
        f_address_rec.address_line_1 := lv_parsed_address;
      elsif f_address_rec.address_line_2 = f_address_rec.address then
        f_address_rec.address_line_2 := lv_parsed_address;
      elsif f_address_rec.address_line_3 = f_address_rec.address then
        f_address_rec.address_line_3 := lv_parsed_address;
      elsif f_address_rec.address_line_4 = f_address_rec.address then
        f_address_rec.address_line_4 := lv_parsed_address;
      end if;
      f_address_rec.address := lv_parsed_address;
      lv_parsed_address := NULL;
    end if;
    --
    -- Add Attention and/or Company back on to address if it was parsed out of address line 3
    -- - Could be stored in company or attention
    --
    if f_address_rec.attention||f_address_rec.address_name||f_address_rec.company IS NOT NULL
       and f_address_rec.address_line_3 IS NULL
       then
      -- make sure Company is not already listed on line 1
      if f_address_rec.company IS NOT NULL
         and upper(f_address_rec.company) != upper(NVL(f_address_rec.address_line_1, ' '))
         and upper(f_address_rec.company) != upper(NVL(f_address_rec.address_line_2, ' '))
         and f_address_rec.address_line_3 IS NULL
         and NOT lb_company_input  -- don't append company if it was input separately (Finance)
         then
        f_address_rec.address_line_3 := f_address_rec.address_line_2;
        f_address_rec.address_line_2 := f_address_rec.address_line_1;
        f_address_rec.address_line_1 := f_address_rec.company;
        f_address_rec.company        := NULL;
      end if;
      -- make sure Attention is not already listed on line 1
      if f_address_rec.attention IS NOT NULL
         and upper(f_address_rec.attention) != upper(NVL(f_address_rec.address_line_1, ' '))
         and upper(f_address_rec.attention) != upper(NVL(f_address_rec.address_line_2, ' '))
         and f_address_rec.address_line_3 IS NULL
         then
        f_address_rec.address_line_3 := f_address_rec.address_line_2;
        f_address_rec.address_line_2 := f_address_rec.address_line_1;
        f_address_rec.address_line_1 := f_address_rec.attention;
        f_address_rec.attention      := NULL;
      end if;
      -- make sure Address_Name is not already listed on line 1
      if f_address_rec.Address_Name IS NOT NULL
         and upper(f_address_rec.Address_Name) != upper(NVL(f_address_rec.address_line_1, ' '))
         and upper(f_address_rec.Address_Name) != upper(NVL(f_address_rec.address_line_2, ' '))
         and f_address_rec.address_line_3 IS NULL
         then
        f_address_rec.address_line_3 := f_address_rec.address_line_2;
        f_address_rec.address_line_2 := f_address_rec.address_line_1;
        f_address_rec.address_line_1 := f_address_rec.Address_Name;
        f_address_rec.Address_Name   := NULL;
      end if;
    end if;
    --
    -- 7/10/09 - account for address_line_4
    -- Add Attention and/or Company back on to address if it was parsed out of address line 3
    -- - Could be stored in company or attention
    --
    if f_address_rec.attention||f_address_rec.address_name||f_address_rec.company IS NOT NULL
       and f_address_rec.address_line_4 IS NULL
       then
      -- make sure Company is not already listed on line 1
      if f_address_rec.company IS NOT NULL
         and upper(f_address_rec.company) != upper(NVL(f_address_rec.address_line_1, ' '))
         and upper(f_address_rec.company) != upper(NVL(f_address_rec.address_line_2, ' '))
         and f_address_rec.address_line_4 IS NULL
         and NOT lb_company_input  -- don't append company if it was input separately (Finance)
         then
        f_address_rec.address_line_4 := f_address_rec.address_line_3;
        f_address_rec.address_line_3 := f_address_rec.address_line_2;
        f_address_rec.address_line_2 := f_address_rec.address_line_1;
        f_address_rec.address_line_1 := f_address_rec.company;
        f_address_rec.company        := NULL;
      end if;
      -- make sure Attention is not already listed on line 1
      if f_address_rec.attention IS NOT NULL
         and upper(f_address_rec.attention) != upper(NVL(f_address_rec.address_line_1, ' '))
         and upper(f_address_rec.attention) != upper(NVL(f_address_rec.address_line_2, ' '))
         and f_address_rec.address_line_4 IS NULL
         then
        f_address_rec.address_line_4 := f_address_rec.address_line_3;
        f_address_rec.address_line_3 := f_address_rec.address_line_2;
        f_address_rec.address_line_2 := f_address_rec.address_line_1;
        f_address_rec.address_line_1 := f_address_rec.attention;
        f_address_rec.attention      := NULL;
      end if;
      -- make sure Address_Name is not already listed on line 1
      if f_address_rec.Address_Name IS NOT NULL
         and upper(f_address_rec.Address_Name) != upper(NVL(f_address_rec.address_line_1, ' '))
         and upper(f_address_rec.Address_Name) != upper(NVL(f_address_rec.address_line_2, ' '))
         and f_address_rec.address_line_4 IS NULL
         then
        f_address_rec.address_line_4 := f_address_rec.address_line_3;
        f_address_rec.address_line_3 := f_address_rec.address_line_2;
        f_address_rec.address_line_2 := f_address_rec.address_line_1;
        f_address_rec.address_line_1 := f_address_rec.Address_Name;
        f_address_rec.Address_Name   := NULL;
      end if;
    end if;
    --
    -- make sure city does not go over 20 characters for Banner - else use City abbreviation
    --
    if length(f_address_rec.city) > gn_Max_City_Length then
      f_address_rec.city := NVL(substr(f_address_rec.city_abbreviation,1,gn_Max_City_Length), substr(f_address_rec.city,1,gn_Max_City_Length));
    end if;
    --
    -- Get the default county if %000 is returned (unique zip code scenario)
    --
    if f_address_rec.county_code like '%000' then
      open c_postal_county(substr(f_address_rec.postal_code,1,5));
      fetch c_postal_county
      into  f_address_rec.county_code;
      if c_postal_county%NOTFOUND then
        f_address_rec.county_code := NULL;
      end if;
      close c_postal_county;
    end if;
    --
    -- Try to get the correct county from the Banner county table
    -- based on State and last 3 digits of FIPS code
    --
    if f_address_rec.county_code IS NOT NULL then
      --
      -- Try to get the county based on the CLEAN_Address returned 5-digit numeric FIPS County Code first
      --
      open c_county(f_address_rec.county_code
                   ,'%');
      fetch c_county
      into  lv_new_county_code;
      if c_county%NOTFOUND then
        close c_county;
        --
        -- Next try to get the county code based on the character state code prefixed on the 3-digit county FIPS
        --
        open c_county(f_address_rec.state||substr(f_address_rec.county_code,3,3)
                     ,'%');
        fetch c_county
        into  lv_new_county_code;
        if c_county%NOTFOUND then
          close c_county;
          --
          -- Next try to get the county code based on the 3-digit county code
          --
          open c_county(substr(f_address_rec.county_code,3,3)
                       ,'%');
          fetch c_county
          into  lv_new_county_code;
          if c_county%NOTFOUND then
            close c_county;
            --
            -- Try to get the correct county from the Banner county table
            -- based on State numeric FIPS and County Name
            --
            open c_county(substr(f_address_rec.county_code,1,2)||'%'
                         ,f_address_rec.county_name);
            fetch c_county
            into  lv_new_county_code;
            if c_county%NOTFOUND then
              close c_county;
              --
              -- Try to get the correct county from the Banner county table
              -- based on State and County Name
              --
              open c_county(f_address_rec.state||'%'
                           ,f_address_rec.county_name);
              fetch c_county
              into  lv_new_county_code;
              if c_county%NOTFOUND then
                lv_new_county_code := NULL;
              end if;
            end if;
          end if;
        end if;
      end if;
      close c_county;
    end if;
    -- assign the county code - use the input as a default
    f_address_rec.county_code := NVL(lv_new_county_code, lv_county_code);
    --
    -- Check for DPV error and use this if the error code is null
    --
    if NVL(f_address_rec.DPV_Status,'AA') NOT IN ('AA','BB','RR')
       and f_address_rec.error_code IS NULL then
      -- Use DPV Status for Error Code
      f_address_rec.error_code   := f_address_rec.DPV_Status;
      f_address_rec.error_string := NVL(cln$lookup.Help_DPV_Status(f_address_rec.DPV_Status)
                                       ,cln$lookup.Get_DPV_Status(f_address_rec.DPV_Status)
                                       );
      -- Append the address suggestion list to the end of the error string - limit to 500 characters
      if f_address_rec.suggestion_list IS NOT NULL then
        f_address_rec.error_string := substr(f_address_rec.error_string||chr(10)||'Suggestions:'||chr(10)||replace(f_address_rec.suggestion_list,';',chr(10)),1,500);
      end if;
    --
    -- get the address status code for the error code for international addresses
    --
    elsif NVL(f_address_rec.Status_Code,'x') like 'AD%' then
      BEGIN
        -- only use address status if the error code is a number
        if to_number(replace(f_address_rec.error_code,'%')) >= 0 then
          --
          -- Check for Valid International Address
          --
          if NVL(f_address_rec.Status_Code,'ADC') IN ('ADC','ADV') then
            f_address_rec.error_code   := NULL;
            f_address_rec.error_string := NULL;
          else
            f_address_rec.error_code   := f_address_rec.Status_Code;
            f_address_rec.error_string := cln$lookup.Get_Address_Status(f_address_rec.Status_Code);
          end if;
        end if;
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
    elsif f_address_rec.error_code IS NOT NULL then
      f_address_rec.error_string := NVL(cln$lookup.Get_Address_Error(f_address_rec.Error_Code)
                                       ,f_address_rec.error_string
                                       );
      -- Append the address suggestion list to the end of the error string - limit to 500 characters
      if f_address_rec.suggestion_list IS NOT NULL then
        f_address_rec.error_string := substr(f_address_rec.error_string||chr(10)||'Suggestions:'||chr(10)||replace(f_address_rec.suggestion_list,';',chr(10)),1,500);
      end if;
    end if;
    --
    -- Get the address_date_and_status field - use Address_Line_8 for the buffer
    --
    -- address_line_7 = [Address_Verified_Flag]
    if f_address_rec.error_code IS NOT NULL then
      f_address_rec.address_line_7   := 'N';
    else
      f_address_rec.address_line_7   := 'Y';
    end if;
    -- address_line_8 = [Verified_Date]:[Address_Error_Code]
    f_address_rec.address_line_8 := rtrim(to_char(sysdate, 'YYYYMMDD')
                                    ||':'||f_address_rec.error_code);
    -- Cleanup the Address Number for US and Canada addresses - don't return so it doesn't get used on mailling labels by accident
    if f_address_rec.Parsed_Address_Number IS NOT NULL
       and f_address_rec.Parsed_Street_Name IS NOT NULL
       then
      f_address_rec.Parsed_Address_Number := NULL;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      fv_plsql_error := SQLERRM;
  END;
  --
  -- Post Address Process User Exit
  -- - Make any custom changes in the CLEAN_Address_Banner_UE User Exit package
  --
  CLEAN_Address_Banner_UE.Post_Address_Process(
         f_address_rec     => f_address_rec
        ,fv_plsql_error    => fv_plsql_error
        ,fv_address_type   => fv_address_type
        );
END Verify_Address_Record;
/******************************************************************************************
 *  Procedure Name  :   Check_Telephone_Record
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Verify a single telephone number for Banner
 *                      - accepts CLEAN_Address Telephone record type as input
 ******************************************************************************************/
PROCEDURE Check_Telephone_Record (
                     f_telephone_rec                IN OUT CLEAN_Address.Phone_rec
                    ,fv_phone_area                  IN     VARCHAR2
                    ,fv_plsql_error                 IN OUT VARCHAR2
                    ,fv_telephone_code              IN     VARCHAR2 DEFAULT NULL
                    ,fv_object_name                 IN     VARCHAR2 DEFAULT NULL /* Object Name where procedure is called from */
                    )
IS
  lb_verified         BOOLEAN       := FALSE;
BEGIN
  fv_plsql_error := NULL;
  -- 3/6/08 - Set the object name so user exit procedure can identify where the call was made from
  Set_Object_Name(fv_object_name);
  --
  -- Verify the Telephone
  --
  BEGIN
    --
    -- Pre Telephone Check User Exit
    -- - Make any custom changes in the CLEAN_Address_Banner_UE User Exit package
    --
    CLEAN_Address_Banner_UE.Pre_Telephone_Check(
             f_telephone_rec   => f_telephone_rec
            ,fv_plsql_error    => fv_plsql_error
            ,fv_telephone_type => fv_telephone_code
            ,fb_verified       => lb_verified
            );
    --
    -- Exit if the record was verified or overridden in the User Exit
    --
    if lb_verified then
      return;
    end if;
    --
    -- Process a Telephone Check
    --
    CLEAN_Address.Phone_Check(
           f_phone_rec => f_telephone_rec
          );
    --
    -- Post Telephone Check User Exit
    -- - Make any custom changes in the CLEAN_Address_Banner_UE User Exit package
    --
    CLEAN_Address_Banner_UE.Post_Telephone_Check(
             f_telephone_rec   => f_telephone_rec
            ,fv_plsql_error    => fv_plsql_error
            ,fv_telephone_type => fv_telephone_code
            ,fb_verified       => lb_verified
            );
    --
    -- Exit if the record was verified or overridden in the User Exit
    --
    if lb_verified then
      return;
    end if;
    --
    -- Get the suggested area code and distance
    --
    -- Assign the area code back only if this was an area code SPLIT
    if f_telephone_rec.Phone_Check_Status_Code = 'U'
       or fv_phone_area IS NULL then
      -- override the Phone area if there was a split of no area on input
      f_telephone_rec.Phone_Area_Code := f_telephone_rec.New_Phone_Area_Code;
    else
      -- Use original phone area if we didn't have an area code split and it was present before
      f_telephone_rec.Phone_Area_Code := fv_phone_area;
    end if;
    -- Only get suggested area code for certain status
    --  C - corrected area code based on Zip if falls into
    if NVL(f_telephone_rec.Phone_Check_Status_Code,'x') != 'C' then
      f_telephone_rec.New_Phone_Area_Code := NULL;
    end if;
    -- Ignore pseudo distances (800 and cell phone numbers)
    if NVL(f_telephone_rec.Phone_Distance, 9999) = 9999 then
      f_telephone_rec.Phone_Distance := NULL;
    end if;
    -- Only accept certain error codes
    --  A - Bad Area Code
    --  B - Phone number is Blank
    --  E - Bad Phone Number - too many or too few digits
    --  P - Bad Prefix
    if NVL(f_telephone_rec.Phone_Check_Error_Code, 'x') not in ('A','B','E','P') then
      f_telephone_rec.Phone_Check_Error_Code := NULL;
    end if;
    --
    -- Get the telephone results field - use Country_Name for the buffer
    --
    f_telephone_rec.Country_Name := substr(rtrim(f_telephone_rec.New_Phone_Area_Code
                                          ||':'||to_char(f_telephone_rec.Phone_Distance)
                                          ||':'||f_telephone_rec.Phone_Check_Error_Code)
                                          ,1,30);
  EXCEPTION
    WHEN OTHERS THEN
      fv_plsql_error := SQLERRM;
  END;
  --
  -- Post Telephone Check User Exit
  -- - Make any custom changes in the CLEAN_Address_Banner_UE User Exit package
  --
  CLEAN_Address_Banner_UE.Post_Telephone_Process(
           f_telephone_rec   => f_telephone_rec
          ,fv_plsql_error    => fv_plsql_error
          ,fv_telephone_type => fv_telephone_code
          );
END Check_Telephone_Record;
/******************************************************************************************
 *  Procedure Name  :   Standardize_Name_Record
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Standardize a single name record for Banner
 *                      - accepts CLEAN_Address Name record type as input
 ******************************************************************************************/
PROCEDURE Standardize_Name_Record (
                     f_name_rec                     IN OUT CLEAN_Address.Name_rec
                    ,fv_name_error_text             IN OUT VARCHAR2
                    ,fv_plsql_error                 IN OUT VARCHAR2
                    ,fv_name_type_code              IN     VARCHAR2 DEFAULT NULL /* SPRIDEN_NTYP_CODE */
                    ,fv_object_name                 IN     VARCHAR2 DEFAULT NULL /* Object Name where procedure is called from */
                    )
IS
  lb_verified         BOOLEAN       := FALSE;
  l_name_rec_bak      CLEAN_Address.Name_rec;
BEGIN
  fv_plsql_error := NULL;
  fv_name_error_text := NULL;
  -- Set the object name so user exit procedure can identify where the call was made from
  Set_Object_Name(fv_object_name);
  -- Backup the name record in case we get unknown first name
  l_name_rec_bak := f_name_rec;
  --
  -- Standardize the Name
  --
  BEGIN
    -- Build the full name field
    f_name_rec.full_name := trim(     f_name_rec.name_prefix
                               ||' '||f_name_rec.first_name
                               ||' '||f_name_rec.middle_name
                               ||' '||f_name_rec.last_name
                               ||' '||f_name_rec.name_suffix
                               );
    --
    -- Pre Name Standardize User Exit
    -- - Make any custom changes in the CLEAN_Address_Banner_UE User Exit package
    --
    CLEAN_Address_Banner_UE.Pre_Name_Standardize(
             f_name_rec         => f_name_rec
            ,fv_name_error_text => fv_name_error_text
            ,fv_plsql_error     => fv_plsql_error
            ,fv_name_type_code  => fv_name_type_code
            ,fb_verified        => lb_verified
            );
    --
    -- Exit if the record was verified or overridden in the User Exit
    --
    if lb_verified then
      return;
    end if;
    --
    -- Process a Name record
    --
    CLEAN_Address.Name_Parse(
           f_name_rec => f_name_rec
          );
    -- Check for other name errors
    if f_name_rec.Name_Parse_Status_Code IS NOT NULL then
      fv_name_error_text := Get_Name_Error_Text(f_name_rec.Name_Parse_Status_Code);
    end if;
    -- Check for Unknown Gender - possible misspelling of first name
    if f_name_rec.Gender = 'U' then
      f_name_rec.Name_Parse_Status_Code := 'U';
      fv_name_error_text := Get_Name_Error_Text(f_name_rec.Gender);
    end if;
    --  Restore the original name fields if we got an error message
    if f_name_rec.Name_Parse_Status_Code IS NOT NULL then
      f_name_rec.name_prefix := clean_address.InitName(l_name_rec_bak.name_prefix);
      f_name_rec.first_name  := clean_address.InitName(l_name_rec_bak.first_name);
      f_name_rec.middle_name := clean_address.InitName(l_name_rec_bak.middle_name);
      f_name_rec.last_name   := clean_address.InitName(l_name_rec_bak.last_name);
      f_name_rec.name_suffix := clean_address.InitName(l_name_rec_bak.name_suffix);
    end if;
    --
    -- Post Name Standardize User Exit
    -- - Make any custom changes in the CLEAN_Address_Banner_UE User Exit package
    --
    CLEAN_Address_Banner_UE.Post_Name_Standardize(
             f_name_rec         => f_name_rec
            ,fv_name_error_text => fv_name_error_text
            ,fv_plsql_error     => fv_plsql_error
            ,fv_name_type_code  => fv_name_type_code
            ,fb_verified        => lb_verified
            );
    --
    -- Exit if the record was verified or overridden in the User Exit
    --
    if lb_verified then
      return;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      fv_plsql_error := SQLERRM;
  END;
END Standardize_Name_Record;
/******************************************************************************************
 *  Procedure Name  :   Validate_Email_Record
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Standardize a single name record for Banner
 *                      - accepts CLEAN_Address Name record type as input
 ******************************************************************************************/
PROCEDURE Validate_Email_Record (
                     f_email_rec                    IN OUT CLEAN_Address.Email_rec
                    ,fv_email_error_text            IN OUT VARCHAR2
                    ,fv_plsql_error                 IN OUT VARCHAR2
                    ,fv_email_type_code             IN     VARCHAR2 DEFAULT NULL /* GOREMAL_EMAL_CODE */
                    ,fv_object_name                 IN     VARCHAR2 DEFAULT NULL /* Object Name where procedure is called from */
                    )
IS
  lb_verified         BOOLEAN       := FALSE;
  lb_dns_lookup       BOOLEAN       := TRUE;
BEGIN
  fv_plsql_error := NULL;
  fv_email_error_text := NULL;
  -- Set the object name so user exit procedure can identify where the call was made from
  Set_Object_Name(fv_object_name);
  --
  -- Validate the Email
  --
  BEGIN
    --
    -- Pre Email Validate User Exit
    -- - Make any custom changes in the CLEAN_Address_Banner_UE User Exit package
    --
    CLEAN_Address_Banner_UE.Pre_Email_Validate(
             f_email_rec         => f_email_rec
            ,fv_email_error_text => fv_email_error_text
            ,fv_plsql_error      => fv_plsql_error
            ,fv_email_type_code  => fv_email_type_code
            ,fb_dns_lookup       => lb_dns_lookup
            ,fb_verified         => lb_verified
            );
    --
    -- Exit if the record was verified or overridden in the User Exit
    --
    if lb_verified then
      return;
    end if;
    --
    -- Process an Email record
    --
    -- See if we need to do the DNS lookup or just parse/standardize
    if lb_dns_lookup then
      CLEAN_Address.Email_Check(
           f_email_rec => f_email_rec
          );
    else
      CLEAN_Address.Email_Parse(
           f_email_rec => f_email_rec
          );
    end if;
    -- Get the Email Error Text
    fv_email_error_text := Get_Email_Error_Text(f_email_rec.email_status_code);
    --
    -- Post Email Validate User Exit
    -- - Make any custom changes in the CLEAN_Address_Banner_UE User Exit package
    --
    CLEAN_Address_Banner_UE.Post_Email_Validate(
             f_email_rec         => f_email_rec
            ,fv_email_error_text => fv_email_error_text
            ,fv_plsql_error      => fv_plsql_error
            ,fv_email_type_code  => fv_email_type_code
            ,fb_dns_lookup       => lb_dns_lookup
            ,fb_verified         => lb_verified
            );
    --
    -- Exit if the record was verified or overridden in the User Exit
    --
    if lb_verified then
      return;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      fv_plsql_error := SQLERRM;
  END;
END Validate_Email_Record;
/******************************************************************************************
 *  Procedure Name  :   Verify (OVERLOADED FOR 3 ADDRESS LINES FOR LEGACY SUPPORT)
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Verify a single address for Banner
 *
 *                      This call wraps the Address record implementation and excepts pure
 *                      VARCHAR2 IN/OUT parameters.
 *
 *                      address_date_and_error_code - YYYYMMDD:<error_code>
 *                         - address verified date concatenated with error code, separated by :
 ******************************************************************************************/
PROCEDURE Verify (
                     address_1                      IN OUT VARCHAR2 /* SPRADDR_STREET_LINE1 */
                    ,address_2                      IN OUT VARCHAR2 /* SPRADDR_STREET_LINE2 */
                    ,address_3                      IN OUT VARCHAR2 /* SPRADDR_STREET_LINE3 */
                    ,city                           IN OUT VARCHAR2 /* SPRADDR_CITY */
                    ,state                          IN OUT VARCHAR2 /* SPRADDR_STAT_CODE */
                    ,postal_code                    IN OUT VARCHAR2 /* SPRADDR_ZIP */
                    ,county_code                    IN OUT VARCHAR2 /* SPRADDR_CNTY_CODE */
                    ,country_code                   IN OUT VARCHAR2 /* SPRADDR_NATN_CODE */
                    ,delivery_point                 IN OUT NUMBER   /* SPRADDR_DELIVERY_POINT */
                    ,check_digit                    IN OUT NUMBER   /* SPRADDR_CORRECTION_DIGIT */
                    ,carrier_route                  IN OUT VARCHAR2 /* SPRADDR_CARRIER_ROUTE */
                    ,address_error_flag             IN OUT VARCHAR2 /* SPRADDR_REVIEWED_IND */
                    ,address_date_and_error_code    IN OUT VARCHAR2 /* SPRADDR_REVIEWED_USER */
                    ,address_error_text             IN OUT VARCHAR2
                    ,fv_plsql_error                 IN OUT VARCHAR2
                    ,fv_address_type                IN     VARCHAR2 DEFAULT NULL /* SPRADDR_ATYP_CODE */
                    ,company_name                   IN     VARCHAR2 DEFAULT NULL /* SPRIDEN_LAST_NAME */
                    ,fv_object_name                 IN     VARCHAR2 DEFAULT NULL /* Object Name where procedure is called from */
                    )
IS
  -- CLEAN_Address record structure
  l_address_rec       CLEAN_Address.Address_rec;
BEGIN
  --
  -- Assign the input variables to the address record
  --
  l_address_rec.Company        := company_name;
  l_address_rec.Address_Line_1 := address_1;
  l_address_rec.Address_Line_2 := address_2;
  l_address_rec.Address_Line_3 := address_3;
  l_address_rec.City           := city;
  l_address_rec.State          := state;
  l_address_rec.Postal_code    := postal_code;
  l_address_rec.county_code    := county_code;
  l_address_rec.country_code   := country_code;
  --
  -- Verify the Address
  --
  Verify_Address_Record(
                f_address_rec   => l_address_rec
               ,fv_plsql_error  => fv_plsql_error
               ,fv_address_type => fv_address_type
               ,fv_object_name  => fv_object_name
               );
  --
  -- Assign the address record back to the output variables
  --
  address_1                 := l_address_rec.Address_Line_1;
  address_2                 := l_address_rec.Address_Line_2;
  address_3                 := l_address_rec.Address_Line_3;
  city                      := substr(l_address_rec.City,1,gn_Max_City_Length);
  state                     := substr(NVL(l_address_rec.State, l_address_rec.Province),1,3);
  postal_code               := l_address_rec.Postal_code;
  county_code               := l_address_rec.county_code;
  delivery_point            := l_address_rec.delivery_point;
  check_digit               := l_address_rec.check_digit;
  carrier_route             := l_address_rec.carrier_route;
  country_code              := l_address_rec.country_code;
  address_error_flag          := l_address_rec.Address_Line_7;
  address_date_and_error_code := l_address_rec.Address_Line_8;
  address_error_text          := substr(l_address_rec.error_string,1,500);
END Verify;
/******************************************************************************************
 *  Procedure Name  :   Verify (Works with 4 address lines and house number)
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Verify a single address for Banner
 *
 *                      This call wraps the Address record implementation and excepts pure
 *                      VARCHAR2 IN/OUT parameters.
 *
 *                      address_date_and_error_code - YYYYMMDD:<error_code>
 *                         - address verified date concatenated with error code, separated by :
 ******************************************************************************************/
PROCEDURE Verify (
                     address_1                      IN OUT VARCHAR2 /* SPRADDR_STREET_LINE1 */
                    ,address_2                      IN OUT VARCHAR2 /* SPRADDR_STREET_LINE2 */
                    ,address_3                      IN OUT VARCHAR2 /* SPRADDR_STREET_LINE3 */
                    ,address_4                      IN OUT VARCHAR2 /* SPRADDR_STREET_LINE4 */
                    ,house_number                   IN OUT VARCHAR2 /* SPRADDR_HOUSE_NUMBER */
                    ,city                           IN OUT VARCHAR2 /* SPRADDR_CITY */
                    ,state                          IN OUT VARCHAR2 /* SPRADDR_STAT_CODE */
                    ,postal_code                    IN OUT VARCHAR2 /* SPRADDR_ZIP */
                    ,county_code                    IN OUT VARCHAR2 /* SPRADDR_CNTY_CODE */
                    ,country_code                   IN OUT VARCHAR2 /* SPRADDR_NATN_CODE */
                    ,delivery_point                 IN OUT NUMBER   /* SPRADDR_DELIVERY_POINT */
                    ,check_digit                    IN OUT NUMBER   /* SPRADDR_CORRECTION_DIGIT */
                    ,carrier_route                  IN OUT VARCHAR2 /* SPRADDR_CARRIER_ROUTE */
                    ,address_error_flag             IN OUT VARCHAR2 /* SPRADDR_REVIEWED_IND */
                    ,address_date_and_error_code    IN OUT VARCHAR2 /* SPRADDR_REVIEWED_USER */
                    ,address_error_text             IN OUT VARCHAR2
                    ,fv_plsql_error                 IN OUT VARCHAR2
                    ,fv_address_type                IN     VARCHAR2 DEFAULT NULL /* SPRADDR_ATYP_CODE */
                    ,company_name                   IN     VARCHAR2 DEFAULT NULL /* SPRIDEN_LAST_NAME */
                    ,fv_object_name                 IN     VARCHAR2 DEFAULT NULL /* Object Name where procedure is called from */
                    )
IS
  -- CLEAN_Address record structure
  l_address_rec       CLEAN_Address.Address_rec;
BEGIN
  --
  -- Assign the input variables to the address record
  --
  l_address_rec.Company        := company_name;
  l_address_rec.Address_Line_1 := address_1;
  l_address_rec.Address_Line_2 := address_2;
  l_address_rec.Address_Line_3 := address_3;
  l_address_rec.Address_Line_4 := address_4;
  l_address_rec.Parsed_Address_Number := house_number;
  l_address_rec.City           := city;
  l_address_rec.State          := state;
  l_address_rec.Postal_code    := postal_code;
  l_address_rec.county_code    := county_code;
  l_address_rec.country_code   := country_code;
  --
  -- Verify the Address
  --
  Verify_Address_Record(
                f_address_rec   => l_address_rec
               ,fv_plsql_error  => fv_plsql_error
               ,fv_address_type => fv_address_type
               ,fv_object_name  => fv_object_name
               );
  --
  -- Assign the address record back to the output variables
  --
  address_1                 := l_address_rec.Address_Line_1;
  address_2                 := l_address_rec.Address_Line_2;
  address_3                 := l_address_rec.Address_Line_3;
  address_4                 := l_address_rec.Address_Line_4;
  house_number              := l_address_rec.Parsed_Address_Number;
  city                      := substr(l_address_rec.City,1,gn_Max_City_Length);
  state                     := substr(NVL(l_address_rec.State, l_address_rec.Province),1,3);
  postal_code               := l_address_rec.Postal_code;
  county_code               := l_address_rec.county_code;
  delivery_point            := l_address_rec.delivery_point;
  check_digit               := l_address_rec.check_digit;
  carrier_route             := l_address_rec.carrier_route;
  country_code              := l_address_rec.country_code;
  address_error_flag          := l_address_rec.Address_Line_7;
  address_date_and_error_code := l_address_rec.Address_Line_8;
  address_error_text          := substr(l_address_rec.error_string,1,500);
END Verify;
/******************************************************************************************
 *  Procedure Name  :   Verify_SPRADDR_Record
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Verify a single address by passing in the SPRADDR Record type
 ******************************************************************************************/
PROCEDURE Verify_SPRADDR_Record (
                     f_SPRADDR_rec                  IN OUT SPRADDR%ROWTYPE
                    ,address_error_text             IN OUT VARCHAR2
                    ,fv_plsql_error                 IN OUT VARCHAR2
                    ,fv_address_type                IN     VARCHAR2 DEFAULT NULL /* SPRADDR_ATYP_CODE */
                    ,company_name                   IN     VARCHAR2 DEFAULT NULL /* SPRIDEN_LAST_NAME */
                    ,fv_object_name                 IN     VARCHAR2 DEFAULT NULL /* Object Name where procedure is called from */
                    )
IS
BEGIN
  --
  -- Call the CLEAN_Address Banner Verify procedure with individual parameters
  --
  CLEAN_Address_Banner.Verify (
           address_1                   => f_SPRADDR_rec.SPRADDR_STREET_LINE1
          ,address_2                   => f_SPRADDR_rec.SPRADDR_STREET_LINE2
          ,address_3                   => f_SPRADDR_rec.SPRADDR_STREET_LINE3
          /*PREBANNER82*/,address_4    => f_SPRADDR_rec.SPRADDR_STREET_LINE4
          /*PREBANNER82*/,house_number => f_SPRADDR_rec.SPRADDR_HOUSE_NUMBER
          ,city                        => f_SPRADDR_rec.SPRADDR_CITY
          ,state                       => f_SPRADDR_rec.SPRADDR_STAT_CODE
          ,postal_code                 => f_SPRADDR_rec.SPRADDR_ZIP
          ,county_code                 => f_SPRADDR_rec.SPRADDR_CNTY_CODE
          ,country_code                => f_SPRADDR_rec.SPRADDR_NATN_CODE
          ,delivery_point              => f_SPRADDR_rec.SPRADDR_DELIVERY_POINT
          ,check_digit                 => f_SPRADDR_rec.SPRADDR_CORRECTION_DIGIT
          ,carrier_route               => f_SPRADDR_rec.SPRADDR_CARRIER_ROUTE
          ,address_error_flag          => f_SPRADDR_rec.SPRADDR_REVIEWED_IND
          ,address_date_and_error_code => f_SPRADDR_rec.SPRADDR_REVIEWED_USER
          ,address_error_text          => address_error_text
          ,fv_plsql_error              => fv_plsql_error
          ,fv_address_type             => fv_address_type
          ,company_name                => company_name
          ,fv_object_name              => fv_object_name
          );
END Verify_SPRADDR_Record;
/******************************************************************************************
 *  Procedure Name  :   Batch_Verify_SPRADDR
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the addresses in the Banner Student Address table (SPRADDR)
 *                      * fn_max_verify
 *                           - maximum number of address to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fv_address_type
 *                           - Banner Address Type (SPRADDR_ATYP_CODE) - only verify this type
 *                           - NULL = to verify all address types
 *                      * fn_days_back
 *                           - Verify Addresses that were updated/created this many days back
 *                             - Note: You can enter fractional days such as 0.25, 0.5, etc
 *                           - NULL = all addresses
 *                      * fb_skip_international
 *                           - Skip international address verification in batch mode
 *                      * fb_verify_inactive
 *                           - Verify inactive addresses as well as active ones
 *                           - This essentially ignores the "to date" and status indicator on the record
 *                      * fd_from_date / fd_to_date
 *                           - Specify a date range for the verification based on SPRADDR_ACTIVITY_DATE
 *                           - This is useful for segmenting the data to create parallel batch procedures,
 *                             increasing throughput
 *                      * fb_set_activity_date_user
 *                           - Set the activity date (SPRADDR_ACTIVITY_DATE) and user (SPRADDR_USER)
 *                             audit fields if any of the address fields change after an update
 *                      * fv_set_source_code
 *                           - Set the address source code (SPRADDR_ASRC_CODE) to the value specified
 *                             if any of the address fields change after an update
 ******************************************************************************************/
PROCEDURE Batch_Verify (
                     fn_max_verify                  IN     NUMBER   DEFAULT 3000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fv_address_type                IN     VARCHAR2 DEFAULT NULL /* SPRADDR_ATYP_CODE */
                    ,fn_days_back                   IN     NUMBER   DEFAULT NULL
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    ,fb_verify_inactive             IN     BOOLEAN  DEFAULT FALSE
                    ,fd_from_date                   IN     DATE     DEFAULT NULL
                    ,fd_to_date                     IN     DATE     DEFAULT NULL
                    ,fb_set_activity_date_user      IN     BOOLEAN  DEFAULT FALSE
                    ,fv_set_source_code             IN     VARCHAR2 DEFAULT NULL
                    )
IS
  ln_only_unverified    NUMBER := 0;
  ln_skip_international NUMBER := 0;
  ln_verify_inactive    NUMBER := 0;
  --
  -- Define the cursor to get the addresses from Banner
  --
  cursor c_addr IS
    select a.rowid
          ,a.SPRADDR_PIDM
          ,a.SPRADDR_ATYP_CODE
          ,a.SPRADDR_SEQNO
          ,a.SPRADDR_FROM_DATE
          ,a.SPRADDR_TO_DATE
          ,decode(NVL(i.spriden_entity_ind,'x'), 'C', i.spriden_last_name, NULL) company
          ,a.SPRADDR_STREET_LINE1  address_line_1
          ,a.SPRADDR_STREET_LINE2  address_line_2
          ,a.SPRADDR_STREET_LINE3  address_line_3
          /*PREBANNER82*/,a.SPRADDR_STREET_LINE4  address_line_4
          /*PREBANNER82*/,a.SPRADDR_HOUSE_NUMBER  house_number
          ,a.SPRADDR_CITY          city
          ,a.SPRADDR_STAT_CODE     state
          ,a.SPRADDR_ZIP           postal_code
          ,a.SPRADDR_CNTY_CODE     county_code
          ,a.SPRADDR_NATN_CODE     country_code
          ,a.SPRADDR_ACTIVITY_DATE audit_date
          ,a.SPRADDR_USER          audit_user
          ,a.SPRADDR_ASRC_CODE     address_source
          ,a.SPRADDR_REVIEWED_IND  address_line_7
          ,a.SPRADDR_REVIEWED_USER address_line_8
    from   spriden i
          ,spraddr a
    where  a.spraddr_pidm = i.spriden_pidm(+)
    and    i.spriden_change_ind(+) IS NULL
    -- Only active records between from and to date
    and    (   (    sysdate <= NVL(a.spraddr_to_date,sysdate+1)
               and  NVL(ltrim(upper(a.spraddr_status_ind)),'A') = 'A'
               )
           or ln_verify_inactive = 1
           )
    -- 2/23/08 - don't overwrite addresses where user checked "Override Address Verification"
    and    NVL(a.SPRADDR_REVIEWED_USER,'x') NOT LIKE '%:!'
    -- Only Local Country addresses if fb_Skip_International is set TRUE
    and    (  NVL(a.SPRADDR_NATN_CODE,NVL(gv_local_country,'x')) = NVL(gv_local_country,'x')
           or ln_skip_international = 0
           )
    -- Only get addresses not verified if specified
    and    (  ln_only_unverified = 0
           or ltrim(a.SPRADDR_REVIEWED_USER) IS NULL
           )
    and    (  fv_address_type IS NULL
           or a.SPRADDR_ATYP_CODE = fv_address_type
           )
    and    a.SPRADDR_ACTIVITY_DATE between NVL(trunc(fd_from_date),a.SPRADDR_ACTIVITY_DATE)
                                       and NVL(trunc(fd_to_date)+0.99999,a.SPRADDR_ACTIVITY_DATE)
    and    (  fn_days_back IS NULL
           or a.SPRADDR_ACTIVITY_DATE > sysdate - fn_days_back
           );
  c_addr_rec c_addr%ROWTYPE;
  -- CLEAN_Address record structures
  l_address_rec       CLEAN_Address.Address_rec;
  l_address_rec_NULL  CLEAN_Address.Address_rec;
  -- Internal Statistic Variables
  li_start            NUMBER;
  li_end              NUMBER;
  li_count            NUMBER := 0;
  li_error_count      NUMBER := 0;
  li_change_count     NUMBER := 0;
  lv_plsql_error      VARCHAR2(2000);
  ln_seqno            NUMBER;
  --
  -- Function to get the next sequence number for a PIDM and Address Type
  --
  FUNCTION Get_New_Seqno (
              fn_pidm         IN NUMBER
             ,fv_address_type IN VARCHAR2
             ) RETURN NUMBER
  IS
    ln_seqno   NUMBER;
  BEGIN
    SELECT NVL(MAX(spraddr_seqno), 0) + 1
    INTO   ln_seqno
    FROM   spraddr
    WHERE  spraddr_pidm = fn_pidm
    AND    spraddr_atyp_code = fv_address_type;
    RETURN ln_seqno;
  END Get_New_Seqno;
BEGIN
  dbms_output.enable(1000000);
  --
  -- Call the Pre Batch Address Verify User Exit
  --
  CLEAN_Address_Banner_UE.Pre_Batch_Address_Verify;
  -- Clear the results table
  Delete_Results;
  -- Set the only unverified flag
  if fb_only_unverified then
    ln_only_unverified := 1;
  end if;
  -- Set the Skip International flag
  if fb_skip_international then
    ln_skip_international := 1;
    Set_Skip_International(TRUE);
  else
    Set_Skip_International(FALSE);
  end if;
  -- Set the Verify Inactive flag
  if fb_verify_inactive then
    ln_verify_inactive := 1;
  end if;
  --
  -- Start the timer
  --
  li_start := dbms_utility.get_time;
  --
  -- Loop through all addresses from the cursor
  --
  OPEN c_addr;
  LOOP
  BEGIN
    FETCH c_addr
    INTO  c_addr_rec;
    EXIT WHEN c_addr%NOTFOUND;
    --
    -- Initialize address record to NULL
    --
    l_address_rec := l_address_rec_NULL;
    --
    -- Assign the input variables to the address record
    --
    l_address_rec.Company        := c_addr_rec.company;
    l_address_rec.Address_Line_1 := c_addr_rec.address_line_1;
    l_address_rec.Address_Line_2 := c_addr_rec.address_line_2;
    l_address_rec.Address_Line_3 := c_addr_rec.address_line_3;
    /*PREBANNER82*/l_address_rec.Address_Line_4 := c_addr_rec.address_line_4;
    /*PREBANNER82*/l_address_rec.Parsed_Address_Number := c_addr_rec.house_number;
    l_address_rec.City           := c_addr_rec.City;
    l_address_rec.State          := c_addr_rec.State;
    l_address_rec.Postal_code    := c_addr_rec.postal_code;
    l_address_rec.county_code    := c_addr_rec.county_code;
    l_address_rec.country_code   := c_addr_rec.country_code;
    --
    -- Verify the address
    --
    Verify_Address_Record(
                  f_address_rec   => l_address_rec
                 ,fv_plsql_error  => lv_plsql_error
                 ,fv_address_type => c_addr_rec.SPRADDR_ATYP_CODE
                 ,fv_object_name  => 'Batch_Verify_SPRADDR'
                 );
    -- Make sure there wasn't an Oracle Pipe error
    if lv_plsql_error IS NOT NULL then
      raise NO_DATA_FOUND;
    end if;
    --
    -- Add the error statistic to the output table
    --
    Add_Result(l_address_rec.error_code, l_address_rec.error_string);
    --
    -- See if we need to update the record
    --
    if fb_update then
      --
      -- Update the Banner Address record
      --
      -- Only update existing if parameter is set, otherwise, inactivate and create a new address
      if gb_Batch_Update_Existing then
        --
        -- 7/21/08
        -- Reset the audit fields and address source if an address field changes
        --
        if l_address_rec.Address_Line_1||l_address_rec.Address_Line_2||l_address_rec.Address_Line_3
            /*PREBANNER82*/||l_address_rec.Address_Line_4||l_address_rec.Parsed_Address_Number
         ||l_address_rec.City||l_address_rec.State||l_address_rec.Postal_code||l_address_rec.country_code||l_address_rec.county_code
        != c_addr_rec.address_line_1||c_addr_rec.address_line_2||c_addr_rec.address_line_3
            /*PREBANNER82*/||c_addr_rec.address_line_4||c_addr_rec.house_number
         ||c_addr_rec.City||c_addr_rec.State||c_addr_rec.postal_code||c_addr_rec.country_code||c_addr_rec.county_code
           then
          if fb_set_activity_date_user then
            c_addr_rec.audit_date := sysdate;
            c_addr_rec.audit_user := USER;
          end if;
          c_addr_rec.address_source := NVL(fv_set_source_code, c_addr_rec.address_source);
          li_change_count := li_change_count + 1;
        end if;
        --
        -- Update the existing record
        --
        update spraddr set
            SPRADDR_STREET_LINE1     = l_address_rec.address_line_1
           ,SPRADDR_STREET_LINE2     = l_address_rec.address_line_2
           ,SPRADDR_STREET_LINE3     = l_address_rec.address_line_3
           /*PREBANNER82*/,SPRADDR_STREET_LINE4     = l_address_rec.address_line_4
           /*PREBANNER82*/,SPRADDR_HOUSE_NUMBER     = l_address_rec.parsed_address_number
           ,SPRADDR_CITY             = l_address_rec.city
           ,SPRADDR_STAT_CODE        = NVL(l_address_rec.state, l_address_rec.province)
           ,SPRADDR_ZIP              = l_address_rec.postal_code
           ,SPRADDR_CNTY_CODE        = l_address_rec.county_code
           -- Comment out next line if country code should not be standardized or updated
           ,SPRADDR_NATN_CODE        = l_address_rec.country_code
           ,SPRADDR_DELIVERY_POINT   = l_address_rec.delivery_point
           ,SPRADDR_CORRECTION_DIGIT = l_address_rec.check_digit
           ,SPRADDR_CARRIER_ROUTE    = l_address_rec.carrier_route
           ,SPRADDR_REVIEWED_IND     = l_address_rec.address_line_7
           ,SPRADDR_REVIEWED_USER    = l_address_rec.address_line_8
           ,SPRADDR_ACTIVITY_DATE    = c_addr_rec.audit_date
           ,SPRADDR_USER             = c_addr_rec.audit_user
           ,SPRADDR_ASRC_CODE        = c_addr_rec.address_source -- keep existing source code if none is specified
        where rowid = c_addr_rec.rowid;
      elsif -- Make sure the address record changed before creating a new record
            l_address_rec.Address_Line_1||l_address_rec.Address_Line_2||l_address_rec.Address_Line_3
          /*PREBANNER82*/||l_address_rec.Address_Line_4||l_address_rec.Parsed_Address_Number
          ||l_address_rec.City||l_address_rec.State||l_address_rec.Postal_code||l_address_rec.country_code
          ||l_address_rec.address_line_7||substr(l_address_rec.address_line_8, instr(l_address_rec.address_line_8, ':'))
         != c_addr_rec.address_line_1||c_addr_rec.address_line_2||c_addr_rec.address_line_3
          /*PREBANNER82*/||c_addr_rec.address_line_4||c_addr_rec.house_number
          ||c_addr_rec.City||c_addr_rec.State||c_addr_rec.postal_code||c_addr_rec.country_code
          ||c_addr_rec.address_line_7||substr(c_addr_rec.address_line_8, instr(c_addr_rec.address_line_8, ':'))
         then
        --
        -- Inactivate the old address
        --
        update spraddr
        set    spraddr_status_ind = 'I'
              ,spraddr_activity_date = SYSDATE
              ,spraddr_user = USER
        where  rowid = c_addr_rec.rowid;
        --
        -- Insert a new SPRADDR Record
        --
        ln_seqno := Get_New_Seqno(c_addr_rec.spraddr_pidm, c_addr_rec.spraddr_atyp_code);
        insert into spraddr (
             spraddr_pidm
            ,spraddr_atyp_code
            ,spraddr_seqno
            ,spraddr_from_date
            ,spraddr_to_date
            ,spraddr_street_line1
            ,spraddr_street_line2
            ,spraddr_street_line3
            /*PREBANNER82*/,spraddr_street_line4
            /*PREBANNER82*/,spraddr_house_number
            ,spraddr_city
            ,spraddr_stat_code
            ,spraddr_zip
            ,spraddr_cnty_code
            ,spraddr_natn_code
            ,spraddr_status_ind
            ,spraddr_activity_date
            ,spraddr_user
            ,spraddr_asrc_code
            ,spraddr_delivery_point
            ,spraddr_correction_digit
            ,spraddr_carrier_route
            ,spraddr_reviewed_ind
            ,spraddr_reviewed_user
        ) VALUES (
             c_addr_rec.spraddr_pidm
            ,c_addr_rec.spraddr_atyp_code
            ,ln_seqno
            ,c_addr_rec.spraddr_from_date
            ,c_addr_rec.spraddr_to_date
            ,l_address_rec.address_line_1
            ,l_address_rec.address_line_2
            ,l_address_rec.address_line_3
            /*PREBANNER82*/,l_address_rec.address_line_4
            /*PREBANNER82*/,l_address_rec.parsed_address_number
            ,l_address_rec.city
            ,l_address_rec.state
            ,l_address_rec.postal_code
            ,l_address_rec.county_code
            ,c_addr_rec.country_code
            ,NULL -- status_ind
            ,SYSDATE -- activity_date
            ,USER
            ,fv_set_source_code
            ,l_address_rec.delivery_point
            ,l_address_rec.check_digit
            ,l_address_rec.carrier_route
            ,l_address_rec.address_line_7
            ,l_address_rec.address_line_8
        );
        --
        -- Update telephone numbers associated with the old address to point to the new address
        --
        update SPRTELE
        set    SPRTELE_ADDR_SEQNO = ln_seqno
              -- Comment the next 2 lines if you don't want to touch original audit fields on old address
              ,sprtele_activity_date = SYSDATE
              ,sprtele_user_id = substr('NCOA-'||USER,1,30)
        where  SPRTELE_PIDM       = c_addr_rec.spraddr_pidm
        and    SPRTELE_ATYP_CODE  = c_addr_rec.spraddr_atyp_code
        and    SPRTELE_ADDR_SEQNO = c_addr_rec.spraddr_seqno;
        --
        -- Update PURCHASE ORDERS associated with the old address to point to the new address
        -- FTVVEND_PIDM, FTVVEND_ATYP_CODE, FTVVEND_ADDR_SEQNO
        --
        update FTVVEND
        set    FTVVEND_ADDR_SEQNO = ln_seqno
              -- Comment the next 2 lines if you don't want to touch original audit fields on old address
              ,FTVVEND_ACTIVITY_DATE = SYSDATE
              ,FTVVEND_USER_ID = USER
        where  FTVVEND_PIDM       = c_addr_rec.spraddr_pidm
        and    FTVVEND_ATYP_CODE  = c_addr_rec.spraddr_atyp_code
        and    FTVVEND_ADDR_SEQNO = c_addr_rec.spraddr_seqno;
        --
        -- Update DIRECT DEPOSITS associated with the old address to point to the new address
        -- FTVVEND_VEND_CHECK_PIDM, FTVVEND_VEND_CHECK_ATYP_CODE, FTVVEND_VEND_CHECK_ADDR_SEQNO
        --
        update FTVVEND
        set    FTVVEND_VEND_CHECK_ADDR_SEQNO = ln_seqno
              -- Comment the next 2 lines if you don't want to touch original audit fields on old address
              ,FTVVEND_ACTIVITY_DATE = SYSDATE
              ,FTVVEND_USER_ID = USER
        where  FTVVEND_VEND_CHECK_PIDM       = c_addr_rec.spraddr_pidm
        and    FTVVEND_VEND_CHECK_ATYP_CODE  = c_addr_rec.spraddr_atyp_code
        and    FTVVEND_VEND_CHECK_ADDR_SEQNO = c_addr_rec.spraddr_seqno;
        --
        -- Update DIRECT DEPOSITS associated with the old address to point to the new address
        -- FTVVEND_PIDM, FTVVEND_VEND_CHECK_ATYP_CODE, FTVVEND_VEND_CHECK_ADDR_SEQNO
        -- - where Check_PIDM is null
        update FTVVEND
        set    FTVVEND_VEND_CHECK_ADDR_SEQNO = ln_seqno
              -- Comment the next 2 lines if you don't want to touch original audit fields on old address
              ,FTVVEND_ACTIVITY_DATE = SYSDATE
              ,FTVVEND_USER_ID = USER
        where  FTVVEND_VEND_CHECK_PIDM       IS NULL
        and    FTVVEND_PIDM                  = c_addr_rec.spraddr_pidm
        and    FTVVEND_VEND_CHECK_ATYP_CODE  = c_addr_rec.spraddr_atyp_code
        and    FTVVEND_VEND_CHECK_ADDR_SEQNO = c_addr_rec.spraddr_seqno;
        --
        -- Update DIRECT DEPOSITS for ACCOUNTS PAYABLE and PAYROLL associated with the old address to point to the new address
        -- GXRDIRD_PIDM, GXRDIRD_ATYP_CODE, GXRDIRD_ADDR_SEQNO
        --
        update GXRDIRD
        set    GXRDIRD_ADDR_SEQNO = ln_seqno
              -- Comment the next 2 lines if you don't want to touch original audit fields on old address
              ,GXRDIRD_ACTIVITY_DATE = SYSDATE
              ,GXRDIRD_USER_ID = USER
        where  GXRDIRD_PIDM       = c_addr_rec.spraddr_pidm
        and    GXRDIRD_ATYP_CODE  = c_addr_rec.spraddr_atyp_code
        and    GXRDIRD_ADDR_SEQNO = c_addr_rec.spraddr_seqno;
        --
        -- 1/25/10 Update Vendor information for other related tables (ie SciQuest)
        --
        BEGIN
          fb_vendor.p_update(c_addr_rec.spraddr_pidm);
          --
          -- Perform Banner API for commit
          --
          gb_common.p_commit;
        EXCEPTION
          WHEN OTHERS THEN NULL;
        END;
      end if;
      --
      -- Commit every 50 records
      --
      if mod(li_count+1, 50) = 0 then
        commit;
      end if;
    end if;
    --
    -- Increment the counter
    --
    li_count := li_count + 1;
    --
    -- Leave if we've reached the max counter
    --
    if li_count >= NVL(fn_max_verify, li_count+1) then
      EXIT;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      lv_plsql_error := NVL(lv_plsql_error, SQLERRM);
      --
      -- fv_plsql_error was found - Oracle Pipe error
      --
      -- Show the error message and input/verified address
      dbms_output.put_line(chr(10)||'Unable to Verify Address:'
                ||chr(10)||lv_plsql_error);
      dbms_output.put_line('*** Primary Key: '
                         ||to_char(c_addr_rec.spraddr_pidm)
                    ||'-'||c_addr_rec.spraddr_atyp_code
                    ||'-'||to_char(c_addr_rec.spraddr_seqno)
                    );
      dbms_output.put_line('*** INPUT Address: '
                ||chr(10)||c_addr_rec.address_line_1
                ||ltrim(chr(10)||c_addr_rec.address_line_2
                ||chr(10)||c_addr_rec.address_line_3, chr(10))
                ||chr(10)||c_addr_rec.city||', '||c_addr_rec.state||' '||c_addr_rec.postal_code
                );
      dbms_output.put_line('*** VERIFIED Address: '
                ||chr(10)||l_address_rec.address_line_1
                ||ltrim(chr(10)||l_address_rec.address_line_2
                ||chr(10)||l_address_rec.address_line_3, chr(10))
                ||chr(10)||l_address_rec.city||', '||l_address_rec.state||' '||l_address_rec.postal_code
                );
      -- Increment error count
      li_error_count := li_error_count + 1;
      -- Leave if we get more than 50 errors
      if li_error_count > 50 then
        EXIT;
      end if;
  END;
  END LOOP;
  CLOSE c_addr;
  commit;
  --
  -- Get the end time
  --
  li_end := dbms_utility.get_time;
  --
  -- Display the Output
  --
  dbms_output.put_line(chr(10));
  -- Show Update Flag
  if fb_update then
    commit;
    dbms_output.put_line('Record Update : ON');
  else
    dbms_output.put_line('Record Update : OFF');
  end if;
  --
  -- Show Statistics
  --
  dbms_output.put_line('# Records  : '||to_char(li_count));
  dbms_output.put_line('# Errors   : '||to_char(li_error_count));
  dbms_output.put_line('# Changes  : '||to_char(li_change_count));
  dbms_output.put_line('Total Time : '||to_char((li_end - li_start)/100)||' sec');
  if (li_end - li_start) != 0 then
    dbms_output.put_line('# Rec/Hour : '||to_char(round(360000*li_count/(li_end - li_start))));
  end if;
  --
  -- Call the Post Batch Address Verify User Exit
  --
  CLEAN_Address_Banner_UE.Post_Batch_Address_Verify;
  --
  -- Show the error summary
  --
  Show_Results;
  -- Delete the results table to clean up
  Delete_Results;
END Batch_Verify;
--
-- Batch_Verify_SPRADDR - overloaded version of Batch_Verify
--
PROCEDURE Batch_Verify_SPRADDR (
                     fn_max_verify                  IN     NUMBER   DEFAULT 3000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fv_address_type                IN     VARCHAR2 DEFAULT NULL /* SPRADDR_ATYP_CODE */
                    ,fn_days_back                   IN     NUMBER   DEFAULT NULL
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    ,fb_verify_inactive             IN     BOOLEAN  DEFAULT FALSE
                    ,fd_from_date                   IN     DATE     DEFAULT NULL
                    ,fd_to_date                     IN     DATE     DEFAULT NULL
                    ,fb_set_activity_date_user      IN     BOOLEAN  DEFAULT FALSE
                    ,fv_set_source_code             IN     VARCHAR2 DEFAULT NULL
                    )
IS
BEGIN
  Batch_Verify (
       fn_max_verify         => fn_max_verify
      ,fb_update             => fb_update
      ,fb_only_unverified    => fb_only_unverified
      ,fv_address_type       => fv_address_type
      ,fn_days_back          => fn_days_back
      ,fb_skip_international => fb_skip_international
      ,fb_verify_inactive    => fb_verify_inactive
      ,fd_from_date          => fd_from_date
      ,fd_to_date            => fd_to_date
      ,fb_set_activity_date_user => fb_set_activity_date_user
      ,fv_set_source_code        => fv_set_source_code
      );
END Batch_Verify_SPRADDR;
/******************************************************************************************
 *  Procedure Name  :   Batch_Verify_SZRUMEX
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the custom Unimarket Remittance Address Table (SZRUMEX)
 *                      * fn_max_verify
 *                           - maximum number of address to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                        NOTE: This option only works if you have made the CLEAN_Address
 *                              mods to this table for missing address columns
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fb_skip_international
 *                           - Skip international address verification in batch mode
 ******************************************************************************************/
PROCEDURE Batch_Verify_SZRUMEX (
                     fn_max_verify                  IN     NUMBER   DEFAULT 1000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    )
IS
  ln_only_unverified    NUMBER := 0;
  ln_skip_international NUMBER := 0;
  --
  -- Define the cursor to get the addresses from Banner
  --
  cursor c_addr IS
    select rowid
          ,SZRUMEX_BUS_NAME
          ,SZRUMEX_ADDR_LINE1 address_line_1
          ,SZRUMEX_ADDR_LINE2 address_line_2
          ,SZRUMEX_ADDR_LINE3 address_line_3
          ,SZRUMEX_CITY         city
          ,SZRUMEX_STATE        state
          ,SZRUMEX_ZIP          postal_code
          ,SZRUMEX_COUNTRY      country_code
    from   SZRUMEX a
    where
    -- Only Local Country addresses if fb_Skip_International is set TRUE
           (  NVL(a.szrumex_country,NVL(gv_local_country,'x')) = NVL(gv_local_country,'x')
           or ln_skip_international = 0
           )
    -- Only get addresses not verified if specified
    --CLNMOD--and    (ln_only_unverified = 0 or ltrim(SOBSBGI_REVIEWED_USER) IS NULL)
    -- 2/23/08 - don't overwrite addresses where user checked "Override Address Verification"
    --CLNMOD--and    NVL(a.SOBSBGI_REVIEWED_USER,'x') NOT LIKE '%:!'
    ;
  c_addr_rec c_addr%ROWTYPE;
  -- CLEAN_Address record structures
  l_address_rec       CLEAN_Address.Address_rec;
  l_address_rec_NULL  CLEAN_Address.Address_rec;
  -- Internal Statistic Variables
  li_start            NUMBER;
  li_end              NUMBER;
  li_count            NUMBER := 0;
  li_error_count      NUMBER := 0;
  lv_plsql_error      VARCHAR2(2000);
BEGIN
  dbms_output.enable(1000000);
  --
  -- Call the Pre Batch Address Verify User Exit
  --
  CLEAN_Address_Banner_UE.Pre_Batch_Address_Verify;
  -- Clear the results table
  Delete_Results;
  -- Set the only unverified flag
  if fb_only_unverified then
    ln_only_unverified := 1;
  end if;
  -- Set the Skip International flag
  if fb_skip_international then
    ln_skip_international := 1;
    Set_Skip_International(TRUE);
  else
    Set_Skip_International(FALSE);
  end if;
  --
  -- Start the timer
  --
  li_start := dbms_utility.get_time;
  --
  -- Loop through all addresses from the cursor
  --
  OPEN c_addr;
  LOOP
  BEGIN
    FETCH c_addr
    INTO  c_addr_rec;
    EXIT WHEN c_addr%NOTFOUND;
    --
    -- Initialize address record to NULL
    --
    l_address_rec := l_address_rec_NULL;
    --
    -- Assign the input variables to the address record
    --
    l_address_rec.Address_Line_1 := c_addr_rec.address_line_1;
    l_address_rec.Address_Line_2 := c_addr_rec.address_line_2;
    l_address_rec.Address_Line_3 := c_addr_rec.address_line_3;
 --   /*PREBANNER82*/l_address_rec.Address_Line_4 := c_addr_rec.address_line_4;
 --   /*PREBANNER82*/l_address_rec.Parsed_Address_Number := c_addr_rec.house_number;
    l_address_rec.City           := c_addr_rec.City;
    l_address_rec.State          := c_addr_rec.State;
--    l_address_rec.county_code    := c_addr_rec.county_code;
    l_address_rec.Postal_code    := c_addr_rec.postal_code;
 --   l_address_rec.county_code    := c_addr_rec.county_code;
    l_address_rec.country_code   := c_addr_rec.country_code;
    --
    -- Verify the address
    --
    Verify_Address_Record(
                  f_address_rec   => l_address_rec
                 ,fv_plsql_error  => lv_plsql_error
                 ,fv_address_type => 'SZRUMEX'
                 ,fv_object_name  => 'Batch_Verify_SZRUMEX'
                 );
    -- Make sure there wasn't an Oracle Pipe error
    if lv_plsql_error IS NOT NULL then
      raise NO_DATA_FOUND;
    end if;
    --
    -- Add the error statistic to the output table
    --
    Add_Result(l_address_rec.error_code, l_address_rec.error_string);
    --
    -- See if we need to update the record
    --
    if fb_update then
      --
      -- Update the existing SOBSBGI Banner Address record
      --
      update SZRUMEX set
          SZRUMEX_ADDR_LINE1     = l_address_rec.address_line_1
         ,SZRUMEX_ADDR_LINE2     = l_address_rec.address_line_2
         ,SZRUMEX_ADDR_LINE3     = l_address_rec.address_line_3
        -- /*PREBANNER82*/,SOBSBGI_STREET_LINE4     = l_address_rec.address_line_4
       --  /*PREBANNER82*/,SOBSBGI_HOUSE_NUMBER     = l_address_rec.parsed_address_number
         ,SZRUMEX_CITY             = l_address_rec.city
         ,SZRUMEX_STATE        = NVL(l_address_rec.state, l_address_rec.province)
         ,SZRUMEX_ZIP              = l_address_rec.postal_code
         --,SOBSBGI_CNTY_CODE        = l_address_rec.county_code
         --CLNMOD--,SOBSBGI_DELIVERY_POINT   = l_address_rec.delivery_point
         --CLNMOD--,SOBSBGI_CORRECTION_DIGIT = l_address_rec.check_digit
         --CLNMOD--,SOBSBGI_CARRIER_ROUTE    = l_address_rec.carrier_route
         --CLNMOD--,SOBSBGI_REVIEWED_IND     = l_address_rec.address_line_7
         --CLNMOD--,SOBSBGI_REVIEWED_USER    = l_address_rec.address_line_8
      where rowid = c_addr_rec.rowid;
      --
      -- Commit every 50 records
      --
      if mod(li_count+1, 50) = 0 then
        commit;
      end if;
    end if;
    --
    -- Increment the counter
    --
    li_count := li_count + 1;
    --
    -- Leave if we've reached the max counter
    --
    if li_count >= NVL(fn_max_verify, li_count+1) then
      EXIT;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      lv_plsql_error := NVL(lv_plsql_error, SQLERRM);
      --
      -- fv_plsql_error was found - Oracle Pipe error
      --
      -- Show the error message and input/verified address
      dbms_output.put_line(chr(10)||'Unable to Verify Address:'
                ||chr(10)||lv_plsql_error);
      dbms_output.put_line('*** Primary Key: '
                         ||c_addr_rec.SZRUMEX_BUS_NAME
                         );
      dbms_output.put_line('*** INPUT Address: '
                ||chr(10)||c_addr_rec.address_line_1
                ||ltrim(chr(10)||c_addr_rec.address_line_2
                ||chr(10)||c_addr_rec.address_line_3, chr(10))
                ||chr(10)||c_addr_rec.city||', '||c_addr_rec.state||' '||c_addr_rec.postal_code
                );
      dbms_output.put_line('*** VERIFIED Address: '
                ||chr(10)||l_address_rec.address_line_1
                ||ltrim(chr(10)||l_address_rec.address_line_2
                ||chr(10)||l_address_rec.address_line_3, chr(10))
                ||chr(10)||l_address_rec.city||', '||l_address_rec.state||' '||l_address_rec.postal_code
                );
      -- Increment error count
      li_error_count := li_error_count + 1;
      -- Leave if we get more than 50 errors
      if li_error_count > 50 then
        EXIT;
      end if;
  END;
  END LOOP;
  CLOSE c_addr;
  commit;
  --
  -- Get the end time
  --
  li_end := dbms_utility.get_time;
  --
  -- Display the Output
  --
  dbms_output.put_line(chr(10));
  -- Show Update Flag
  if fb_update then
    commit;
    dbms_output.put_line('Record Update : ON');
  else
    dbms_output.put_line('Record Update : OFF');
  end if;
  --
  -- Show Statistics
  --
  dbms_output.put_line('# Records  : '||to_char(li_count));
  dbms_output.put_line('# Errors   : '||to_char(li_error_count));
  dbms_output.put_line('Total Time : '||to_char((li_end - li_start)/100)||' sec');
  if (li_end - li_start) != 0 then
    dbms_output.put_line('# Rec/Hour : '||to_char(round(360000*li_count/(li_end - li_start))));
  end if;
  --
  -- Call the Post Batch Address Verify User Exit
  --
  CLEAN_Address_Banner_UE.Post_Batch_Address_Verify;
  --
  -- Show the error summary
  --
  Show_Results;
  -- Delete the results table to clean up
  Delete_Results;
END Batch_Verify_SZRUMEX;
/******************************************************************************************
 *  Procedure Name  :   Batch_Verify_SARADDR
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the Banner Electronics Admission Address Table (SARADDR)
 *                      * fn_max_verify
 *                           - maximum number of address to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                        NOTE: This option only works if you have made the CLEAN_Address
 *                              mods to this table for missing address columns
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fn_days_back
 *                           - Verify Addresses that were updated/created this many days back
 *                             - Note: You can enter fractional days such as 0.25, 0.5, etc
 *                           - NULL = all addresses
 *                      * fb_skip_international
 *                           - Skip international address verification in batch mode
 *                      * fd_from_date / fd_to_date
 *                           - Specify a date range for the verification based on SARADDR_ACTIVITY_DATE
 *                           - This is useful for segmenting the data to create parallel batch procedures,
 *                             increasing throughput
 ******************************************************************************************/
PROCEDURE Batch_Verify_SARADDR (
                     fn_max_verify                  IN     NUMBER   DEFAULT 1000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fn_days_back                   IN     NUMBER   DEFAULT NULL
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    ,fd_from_date                   IN     DATE     DEFAULT NULL
                    ,fd_to_date                     IN     DATE     DEFAULT NULL
                    )
IS
  ln_only_unverified    NUMBER := 0;
  ln_skip_international NUMBER := 0;
  --
  -- Define the cursor to get the addresses from Banner
  --
  cursor c_addr IS
    select rowid
          ,SARADDR_AIDM
          ,SARADDR_SEQNO
          ,SARADDR_STREET_LINE1 address_line_1
          ,SARADDR_STREET_LINE2 address_line_2
          ,SARADDR_STREET_LINE3 address_line_3
          /*PREBANNER82*/,SARADDR_STREET_LINE4  address_line_4
          /*PREBANNER82*/,SARADDR_HOUSE_NUMBER  house_number
          ,SARADDR_CITY         city
          ,SARADDR_STAT_CDE     state
          ,SARADDR_ZIP          postal_code
          ,SARADDR_CNTY_CDE     county_code
          ,SARADDR_NATN_CDE     country_code
    from   SARADDR a
    where  (  fn_days_back IS NULL
           or SARADDR_ACTIVITY_DATE > sysdate - fn_days_back
           )
    and    a.SARADDR_ACTIVITY_DATE between NVL(trunc(fd_from_date),a.SARADDR_ACTIVITY_DATE)
                                       and NVL(trunc(fd_to_date)+0.99999,a.SARADDR_ACTIVITY_DATE)
    -- Only Local Country addresses if fb_Skip_International is set TRUE
    and    (  NVL(a.SARADDR_NATN_CDE,NVL(gv_local_country,'x')) = NVL(gv_local_country,'x')
           or ln_skip_international = 0
           )
    -- Only get addresses not verified if specified
    --CLNMOD--and    (ln_only_unverified = 0 or ltrim(SARADDR_REVIEWED_USER) IS NULL)
    -- 2/23/08 - don't overwrite addresses where user checked "Override Address Verification"
    --CLNMOD--and    NVL(a.SARADDR_REVIEWED_USER,'x') NOT LIKE '%:!'
    ;
  c_addr_rec c_addr%ROWTYPE;
  -- CLEAN_Address record structures
  l_address_rec       CLEAN_Address.Address_rec;
  l_address_rec_NULL  CLEAN_Address.Address_rec;
  -- Internal Statistic Variables
  li_start            NUMBER;
  li_end              NUMBER;
  li_count            NUMBER := 0;
  li_error_count      NUMBER := 0;
  lv_plsql_error      VARCHAR2(2000);
BEGIN
  dbms_output.enable(1000000);
  --
  -- Call the Pre Batch Address Verify User Exit
  --
  CLEAN_Address_Banner_UE.Pre_Batch_Address_Verify;
  -- Clear the results table
  Delete_Results;
  -- Set the only unverified flag
  if fb_only_unverified then
    ln_only_unverified := 1;
  end if;
  -- Set the Skip International flag
  if fb_skip_international then
    ln_skip_international := 1;
    Set_Skip_International(TRUE);
  else
    Set_Skip_International(FALSE);
  end if;
  --
  -- Start the timer
  --
  li_start := dbms_utility.get_time;
  --
  -- Loop through all addresses from the cursor
  --
  OPEN c_addr;
  LOOP
  BEGIN
    FETCH c_addr
    INTO  c_addr_rec;
    EXIT WHEN c_addr%NOTFOUND;
    --
    -- Initialize address record to NULL
    --
    l_address_rec := l_address_rec_NULL;
    --
    -- Assign the input variables to the address record
    --
    l_address_rec.Address_Line_1 := c_addr_rec.address_line_1;
    l_address_rec.Address_Line_2 := c_addr_rec.address_line_2;
    l_address_rec.Address_Line_3 := c_addr_rec.address_line_3;
    /*PREBANNER82*/l_address_rec.Address_Line_4 := c_addr_rec.address_line_4;
    /*PREBANNER82*/l_address_rec.Parsed_Address_Number := c_addr_rec.house_number;
    l_address_rec.City           := c_addr_rec.City;
    l_address_rec.State          := c_addr_rec.State;
    l_address_rec.Postal_code    := c_addr_rec.postal_code;
    l_address_rec.county_code    := c_addr_rec.county_code;
    l_address_rec.country_code   := c_addr_rec.country_code;
    --
    -- Verify the address
    --
    Verify_Address_Record(
                  f_address_rec   => l_address_rec
                 ,fv_plsql_error  => lv_plsql_error
                 ,fv_address_type => 'SARADDR'
                 ,fv_object_name  => 'Batch_Verify_SARADDR'
                 );
    -- Make sure there wasn't an Oracle Pipe error
    if lv_plsql_error IS NOT NULL then
      raise NO_DATA_FOUND;
    end if;
    --
    -- Add the error statistic to the output table
    --
    Add_Result(l_address_rec.error_code, l_address_rec.error_string);
    --
    -- See if we need to update the record
    --
    if fb_update then
      --
      -- Update the existing SARADDR Banner Address record
      --
      update SARADDR set
          SARADDR_STREET_LINE1     = l_address_rec.address_line_1
         ,SARADDR_STREET_LINE2     = l_address_rec.address_line_2
         ,SARADDR_STREET_LINE3     = l_address_rec.address_line_3
         /*PREBANNER82*/,SARADDR_STREET_LINE4     = l_address_rec.address_line_4
         /*PREBANNER82*/,SARADDR_HOUSE_NUMBER     = l_address_rec.parsed_address_number
         ,SARADDR_CITY             = l_address_rec.city
         ,SARADDR_STAT_CDE         = NVL(l_address_rec.state, l_address_rec.province)
         ,SARADDR_ZIP              = l_address_rec.postal_code
         ,SARADDR_CNTY_CDE         = l_address_rec.county_code
         --CLNMOD--,SARADDR_DELIVERY_POINT   = l_address_rec.delivery_point
         --CLNMOD--,SARADDR_CORRECTION_DIGIT = l_address_rec.check_digit
         --CLNMOD--,SARADDR_CARRIER_ROUTE    = l_address_rec.carrier_route
         --CLNMOD--,SARADDR_REVIEWED_IND     = l_address_rec.address_line_7
         --CLNMOD--,SARADDR_REVIEWED_USER    = l_address_rec.address_line_8
      where rowid = c_addr_rec.rowid;
      --
      -- Commit every 50 records
      --
      if mod(li_count+1, 50) = 0 then
        commit;
      end if;
    end if;
    --
    -- Increment the counter
    --
    li_count := li_count + 1;
    --
    -- Leave if we've reached the max counter
    --
    if li_count >= NVL(fn_max_verify, li_count+1) then
      EXIT;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      lv_plsql_error := NVL(lv_plsql_error, SQLERRM);
      --
      -- fv_plsql_error was found - Oracle Pipe error
      --
      -- Show the error message and input/verified address
      dbms_output.put_line(chr(10)||'Unable to Verify Address:'
                ||chr(10)||lv_plsql_error);
      dbms_output.put_line('*** Primary Key: '
                         ||to_char(c_addr_rec.SARADDR_AIDM)
                    ||'-'||to_char(c_addr_rec.SARADDR_SEQNO)
                    );
      dbms_output.put_line('*** INPUT Address: '
                ||chr(10)||c_addr_rec.address_line_1
                ||ltrim(chr(10)||c_addr_rec.address_line_2
                ||chr(10)||c_addr_rec.address_line_3, chr(10))
                ||chr(10)||c_addr_rec.city||', '||c_addr_rec.state||' '||c_addr_rec.postal_code
                );
      dbms_output.put_line('*** VERIFIED Address: '
                ||chr(10)||l_address_rec.address_line_1
                ||ltrim(chr(10)||l_address_rec.address_line_2
                ||chr(10)||l_address_rec.address_line_3, chr(10))
                ||chr(10)||l_address_rec.city||', '||l_address_rec.state||' '||l_address_rec.postal_code
                );
      -- Increment error count
      li_error_count := li_error_count + 1;
      -- Leave if we get more than 50 errors
      if li_error_count > 50 then
        EXIT;
      end if;
  END;
  END LOOP;
  CLOSE c_addr;
  commit;
  --
  -- Get the end time
  --
  li_end := dbms_utility.get_time;
  --
  -- Display the Output
  --
  dbms_output.put_line(chr(10));
  -- Show Update Flag
  if fb_update then
    commit;
    dbms_output.put_line('Record Update : ON');
  else
    dbms_output.put_line('Record Update : OFF');
  end if;
  --
  -- Show Statistics
  --
  dbms_output.put_line('# Records  : '||to_char(li_count));
  dbms_output.put_line('# Errors   : '||to_char(li_error_count));
  dbms_output.put_line('Total Time : '||to_char((li_end - li_start)/100)||' sec');
  if (li_end - li_start) != 0 then
    dbms_output.put_line('# Rec/Hour : '||to_char(round(360000*li_count/(li_end - li_start))));
  end if;
  --
  -- Call the Post Batch Address Verify User Exit
  --
  CLEAN_Address_Banner_UE.Post_Batch_Address_Verify;
  --
  -- Show the error summary
  --
  Show_Results;
  -- Delete the results table to clean up
  Delete_Results;
END Batch_Verify_SARADDR;
/******************************************************************************************
 *  Procedure Name  :   Batch_Verify_SHBDIPL
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the Banner Diploma Address Table (SHBDIPL)
 *                      * fn_max_verify
 *                           - maximum number of address to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                        NOTE: This option only works if you have made the CLEAN_Address
 *                              mods to this table for missing address columns
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fn_days_back
 *                           - Verify Addresses that were updated/created this many days back
 *                             - Note: You can enter fractional days such as 0.25, 0.5, etc
 *                           - NULL = all addresses
 *                      * fb_skip_international
 *                           - Skip international address verification in batch mode
 *                      * fd_from_date / fd_to_date
 *                           - Specify a date range for the verification based on SHBDIPL_ACTIVITY_DATE
 *                           - This is useful for segmenting the data to create parallel batch procedures,
 *                             increasing throughput
 ******************************************************************************************/
PROCEDURE Batch_Verify_SHBDIPL (
                     fn_max_verify                  IN     NUMBER   DEFAULT 1000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fn_days_back                   IN     NUMBER   DEFAULT NULL
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    ,fd_from_date                   IN     DATE     DEFAULT NULL
                    ,fd_to_date                     IN     DATE     DEFAULT NULL
                    )
IS
  ln_only_unverified    NUMBER := 0;
  ln_skip_international NUMBER := 0;
  --
  -- Define the cursor to get the addresses from Banner
  --
  cursor c_addr IS
    select rowid
          ,SHBDIPL_PIDM
          ,SHBDIPL_DGMR_SEQ_NO
          ,SHBDIPL_STREET1      address_line_1
          ,SHBDIPL_STREET2      address_line_2
          ,SHBDIPL_STREET3      address_line_3
          /*PREBANNER82*/,SHBDIPL_STREET_LINE4  address_line_4
          /*PREBANNER82*/,SHBDIPL_HOUSE_NUMBER  house_number
          ,SHBDIPL_CITY         city
          ,SHBDIPL_STAT_CODE    state
          ,SHBDIPL_ZIP          postal_code
          ,SHBDIPL_NATN_CODE    country_code
          --CLNMOD--,SHBDIPL_CNTY_CODE     county_code
    from   SHBDIPL a
    where  (  fn_days_back IS NULL
           or SHBDIPL_ACTIVITY_DATE > sysdate - fn_days_back
           )
    and    a.SHBDIPL_ACTIVITY_DATE between NVL(trunc(fd_from_date),a.SHBDIPL_ACTIVITY_DATE)
                                       and NVL(trunc(fd_to_date)+0.99999,a.SHBDIPL_ACTIVITY_DATE)
    -- Only Local Country addresses if fb_Skip_International is set TRUE
    and    (  NVL(a.SHBDIPL_NATN_CODE,NVL(gv_local_country,'x')) = NVL(gv_local_country,'x')
           or ln_skip_international = 0
           )
    -- Only get addresses not verified if specified
    --CLNMOD--and    (ln_only_unverified = 0 or ltrim(SHBDIPL_REVIEWED_USER) IS NULL)
    -- 2/23/08 - don't overwrite addresses where user checked "Override Address Verification"
    --CLNMOD--and    NVL(a.SHBDIPL_REVIEWED_USER,'x') NOT LIKE '%:!'
    ;
  c_addr_rec c_addr%ROWTYPE;
  -- CLEAN_Address record structures
  l_address_rec       CLEAN_Address.Address_rec;
  l_address_rec_NULL  CLEAN_Address.Address_rec;
  -- Internal Statistic Variables
  li_start            NUMBER;
  li_end              NUMBER;
  li_count            NUMBER := 0;
  li_error_count      NUMBER := 0;
  lv_plsql_error      VARCHAR2(2000);
BEGIN
  dbms_output.enable(1000000);
  --
  -- Call the Pre Batch Address Verify User Exit
  --
  CLEAN_Address_Banner_UE.Pre_Batch_Address_Verify;
  -- Clear the results table
  Delete_Results;
  -- Set the only unverified flag
  if fb_only_unverified then
    ln_only_unverified := 1;
  end if;
  -- Set the Skip International flag
  if fb_skip_international then
    ln_skip_international := 1;
    Set_Skip_International(TRUE);
  else
    Set_Skip_International(FALSE);
  end if;
  --
  -- Start the timer
  --
  li_start := dbms_utility.get_time;
  --
  -- Loop through all addresses from the cursor
  --
  OPEN c_addr;
  LOOP
  BEGIN
    FETCH c_addr
    INTO  c_addr_rec;
    EXIT WHEN c_addr%NOTFOUND;
    --
    -- Initialize address record to NULL
    --
    l_address_rec := l_address_rec_NULL;
    --
    -- Assign the input variables to the address record
    --
    l_address_rec.Address_Line_1 := c_addr_rec.address_line_1;
    l_address_rec.Address_Line_2 := c_addr_rec.address_line_2;
    l_address_rec.Address_Line_3 := c_addr_rec.address_line_3;
    /*PREBANNER82*/l_address_rec.Address_Line_4 := c_addr_rec.address_line_4;
    /*PREBANNER82*/l_address_rec.Parsed_Address_Number := c_addr_rec.house_number;
    l_address_rec.City           := c_addr_rec.City;
    l_address_rec.State          := c_addr_rec.State;
    l_address_rec.Postal_code    := c_addr_rec.postal_code;
    --CLNMOD--l_address_rec.county_code    := c_addr_rec.county_code;
    l_address_rec.country_code   := c_addr_rec.country_code;
    --
    -- Verify the address
    --
    Verify_Address_Record(
                  f_address_rec   => l_address_rec
                 ,fv_plsql_error  => lv_plsql_error
                 ,fv_address_type => 'SHBDIPL'
                 ,fv_object_name  => 'Batch_Verify_SHBDIPL'
                 );
    -- Make sure there wasn't an Oracle Pipe error
    if lv_plsql_error IS NOT NULL then
      raise NO_DATA_FOUND;
    end if;
    --
    -- Add the error statistic to the output table
    --
    Add_Result(l_address_rec.error_code, l_address_rec.error_string);
    --
    -- See if we need to update the record
    --
    if fb_update then
      --
      -- Update the existing SHBDIPL Banner Address record
      --
      update SHBDIPL set
          SHBDIPL_STREET1          = l_address_rec.address_line_1
         ,SHBDIPL_STREET2          = l_address_rec.address_line_2
         ,SHBDIPL_STREET3          = l_address_rec.address_line_3
         /*PREBANNER82*/,SHBDIPL_STREET_LINE4     = l_address_rec.address_line_4
         /*PREBANNER82*/,SHBDIPL_HOUSE_NUMBER     = l_address_rec.parsed_address_number
         ,SHBDIPL_CITY             = l_address_rec.city
         ,SHBDIPL_STAT_CODE        = NVL(l_address_rec.state, l_address_rec.province)
         ,SHBDIPL_ZIP              = l_address_rec.postal_code
         --CLNMOD--,SHBDIPL_CNTY_CODE        = l_address_rec.county_code
         --CLNMOD--,SHBDIPL_DELIVERY_POINT   = l_address_rec.delivery_point
         --CLNMOD--,SHBDIPL_CORRECTION_DIGIT = l_address_rec.check_digit
         --CLNMOD--,SHBDIPL_CARRIER_ROUTE    = l_address_rec.carrier_route
         --CLNMOD--,SHBDIPL_REVIEWED_IND     = l_address_rec.address_line_7
         --CLNMOD--,SHBDIPL_REVIEWED_USER    = l_address_rec.address_line_8
      where rowid = c_addr_rec.rowid;
      --
      -- Commit every 50 records
      --
      if mod(li_count+1, 50) = 0 then
        commit;
      end if;
    end if;
    --
    -- Increment the counter
    --
    li_count := li_count + 1;
    --
    -- Leave if we've reached the max counter
    --
    if li_count >= NVL(fn_max_verify, li_count+1) then
      EXIT;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      lv_plsql_error := NVL(lv_plsql_error, SQLERRM);
      --
      -- fv_plsql_error was found - Oracle Pipe error
      --
      -- Show the error message and input/verified address
      dbms_output.put_line(chr(10)||'Unable to Verify Address:'
                ||chr(10)||lv_plsql_error);
      dbms_output.put_line('*** Primary Key: '
                         ||to_char(c_addr_rec.SHBDIPL_PIDM)
                    ||'-'||to_char(c_addr_rec.SHBDIPL_DGMR_SEQ_NO)
                    );
      dbms_output.put_line('*** INPUT Address: '
                ||chr(10)||c_addr_rec.address_line_1
                ||ltrim(chr(10)||c_addr_rec.address_line_2
                ||chr(10)||c_addr_rec.address_line_3, chr(10))
                ||chr(10)||c_addr_rec.city||', '||c_addr_rec.state||' '||c_addr_rec.postal_code
                );
      dbms_output.put_line('*** VERIFIED Address: '
                ||chr(10)||l_address_rec.address_line_1
                ||ltrim(chr(10)||l_address_rec.address_line_2
                ||chr(10)||l_address_rec.address_line_3, chr(10))
                ||chr(10)||l_address_rec.city||', '||l_address_rec.state||' '||l_address_rec.postal_code
                );
      -- Increment error count
      li_error_count := li_error_count + 1;
      -- Leave if we get more than 50 errors
      if li_error_count > 50 then
        EXIT;
      end if;
  END;
  END LOOP;
  CLOSE c_addr;
  commit;
  --
  -- Get the end time
  --
  li_end := dbms_utility.get_time;
  --
  -- Display the Output
  --
  dbms_output.put_line(chr(10));
  -- Show Update Flag
  if fb_update then
    commit;
    dbms_output.put_line('Record Update : ON');
  else
    dbms_output.put_line('Record Update : OFF');
  end if;
  --
  -- Show Statistics
  --
  dbms_output.put_line('# Records  : '||to_char(li_count));
  dbms_output.put_line('# Errors   : '||to_char(li_error_count));
  dbms_output.put_line('Total Time : '||to_char((li_end - li_start)/100)||' sec');
  if (li_end - li_start) != 0 then
    dbms_output.put_line('# Rec/Hour : '||to_char(round(360000*li_count/(li_end - li_start))));
  end if;
  --
  -- Call the Post Batch Address Verify User Exit
  --
  CLEAN_Address_Banner_UE.Post_Batch_Address_Verify;
  --
  -- Show the error summary
  --
  Show_Results;
  -- Delete the results table to clean up
  Delete_Results;
END Batch_Verify_SHBDIPL;
/******************************************************************************************
 *  Procedure Name  :   Batch_Verify_SPTADDR
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the Banner Temporary SPRADDR Address Table (SPTADDR)
 *                      * fn_max_verify
 *                           - maximum number of address to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                        NOTE: This option only works if you have made the CLEAN_Address
 *                              mods to this table for missing address columns
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fv_address_type
 *                           - Banner Address Type (SPTADDR_ATYP_CODE) - only verify this type
 *                           - NULL = to verify all address types
 *                      * fn_days_back
 *                           - Verify Addresses that were updated/created this many days back
 *                             - Note: You can enter fractional days such as 0.25, 0.5, etc
 *                           - NULL = all addresses
 *                      * fb_skip_international
 *                           - Skip international address verification in batch mode
 *                      * fb_verify_inactive
 *                           - Verify inactive addresses as well as active ones
 *                           - This essentially ignores the "to date" and status indicator on the record
 *                      * fd_from_date / fd_to_date
 *                           - Specify a date range for the verification based on SPTADDR_ACTIVITY_DATE
 *                           - This is useful for segmenting the data to create parallel batch procedures,
 *                             increasing throughput
 ******************************************************************************************/
PROCEDURE Batch_Verify_SPTADDR (
                     fn_max_verify                  IN     NUMBER   DEFAULT 1000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fv_address_type                IN     VARCHAR2 DEFAULT NULL /* SPTADDR_ATYP_CODE */
                    ,fn_days_back                   IN     NUMBER   DEFAULT NULL
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    ,fb_verify_inactive             IN     BOOLEAN  DEFAULT FALSE
                    ,fd_from_date                   IN     DATE     DEFAULT NULL
                    ,fd_to_date                     IN     DATE     DEFAULT NULL
                    )
IS
  ln_only_unverified    NUMBER := 0;
  ln_skip_international NUMBER := 0;
  ln_verify_inactive    NUMBER := 0;
  --
  -- Define the cursor to get the addresses from Banner
  --
  cursor c_addr IS
    select rowid
          ,SPTADDR_PIDM
          ,SPTADDR_ATYP_CODE
          ,SPTADDR_SEQNO
          ,SPTADDR_STREET_LINE1 address_line_1
          ,SPTADDR_STREET_LINE2 address_line_2
          ,SPTADDR_STREET_LINE3 address_line_3
          -- NO STREET LINE 4 FIELD YET /*PREBANNER82*/,SPTADDR_STREET_LINE4  address_line_4
          -- NO HOUSE NUMBER FIELD YET /*PREBANNER82*/,SPTADDR_HOUSE_NUMBER  house_number
          ,SPTADDR_CITY         city
          ,SPTADDR_STAT_CODE    state
          ,SPTADDR_ZIP          postal_code
          ,SPTADDR_CNTY_CODE    county_code
          ,SPTADDR_NATN_CODE    country_code
    from   SPTADDR a
    -- Only active records between from and to date
    where  sysdate >= NVL(SPTADDR_from_date,sysdate-1)
    -- NOTE: Don't use NVL(SPTADDR_to_date,sysdate+1) since future addresses should be updated, too
    and    (  NVL(ltrim(upper(SPTADDR_status_ind)),'A') = 'A'
           or ln_verify_inactive = 1
           )
    -- 2/23/08 - don't overwrite addresses where user checked "Override Address Verification"
    --CLNMOD--and    NVL(a.SPTADDR_REVIEWED_USER,'x') NOT LIKE '%:!'
    -- Only Local Country addresses if fb_Skip_International is set TRUE
    and    (  NVL(a.SPTADDR_NATN_CODE,NVL(gv_local_country,'x')) = NVL(gv_local_country,'x')
           or ln_skip_international = 0
           )
    and    (  fv_address_type IS NULL
           or SPTADDR_ATYP_CODE = fv_address_type
           )
    -- Only get addresses not verified if specified
    --CLNMOD--and    (ln_only_unverified = 0 or ltrim(SPTADDR_REVIEWED_USER) IS NULL)
    and    a.SPTADDR_ACTIVITY_DATE between NVL(trunc(fd_from_date),a.SPTADDR_ACTIVITY_DATE)
                                       and NVL(trunc(fd_to_date)+0.99999,a.SPTADDR_ACTIVITY_DATE)
    and    (  fn_days_back IS NULL
           or SPTADDR_ACTIVITY_DATE > sysdate - fn_days_back
           );
  c_addr_rec c_addr%ROWTYPE;
  -- CLEAN_Address record structures
  l_address_rec       CLEAN_Address.Address_rec;
  l_address_rec_NULL  CLEAN_Address.Address_rec;
  -- Internal Statistic Variables
  li_start            NUMBER;
  li_end              NUMBER;
  li_count            NUMBER := 0;
  li_error_count      NUMBER := 0;
  lv_plsql_error      VARCHAR2(2000);
BEGIN
  dbms_output.enable(1000000);
  --
  -- Call the Pre Batch Address Verify User Exit
  --
  CLEAN_Address_Banner_UE.Pre_Batch_Address_Verify;
  -- Clear the results table
  Delete_Results;
  -- Set the only unverified flag
  if fb_only_unverified then
    ln_only_unverified := 1;
  end if;
  -- Set the Skip International flag
  if fb_skip_international then
    ln_skip_international := 1;
    Set_Skip_International(TRUE);
  else
    Set_Skip_International(FALSE);
  end if;
  -- Set the Verify Inactive flag
  if fb_verify_inactive then
    ln_verify_inactive := 1;
  end if;
  --
  -- Start the timer
  --
  li_start := dbms_utility.get_time;
  --
  -- Loop through all addresses from the cursor
  --
  OPEN c_addr;
  LOOP
  BEGIN
    FETCH c_addr
    INTO  c_addr_rec;
    EXIT WHEN c_addr%NOTFOUND;
    --
    -- Initialize address record to NULL
    --
    l_address_rec := l_address_rec_NULL;
    --
    -- Assign the input variables to the address record
    --
    l_address_rec.Address_Line_1 := c_addr_rec.address_line_1;
    l_address_rec.Address_Line_2 := c_addr_rec.address_line_2;
    l_address_rec.Address_Line_3 := c_addr_rec.address_line_3;
    -- NO STREET LINE 4 FIELD YET /*PREBANNER82*/l_address_rec.Address_Line_4 := c_addr_rec.address_line_4;
    -- NO HOUSE NUMBER FIELD YET /*PREBANNER82*/l_address_rec.Parsed_Address_Number := c_addr_rec.house_number;
    l_address_rec.City           := c_addr_rec.City;
    l_address_rec.State          := c_addr_rec.State;
    l_address_rec.Postal_code    := c_addr_rec.postal_code;
    l_address_rec.county_code    := c_addr_rec.county_code;
    l_address_rec.country_code   := c_addr_rec.country_code;
    --
    -- Verify the address
    --
    Verify_Address_Record(
                  f_address_rec   => l_address_rec
                 ,fv_plsql_error  => lv_plsql_error
                 ,fv_address_type => c_addr_rec.SPTADDR_ATYP_CODE
                 ,fv_object_name  => 'Batch_Verify_SPTADDR'
                 );
    -- Make sure there wasn't an Oracle Pipe error
    if lv_plsql_error IS NOT NULL then
      raise NO_DATA_FOUND;
    end if;
    --
    -- Add the error statistic to the output table
    --
    Add_Result(l_address_rec.error_code, l_address_rec.error_string);
    --
    -- See if we need to update the record
    --
    if fb_update then
      --
      -- Update the existing SPTADDR Banner Address record
      --
      update SPTADDR set
          SPTADDR_STREET_LINE1     = l_address_rec.address_line_1
         ,SPTADDR_STREET_LINE2     = l_address_rec.address_line_2
         ,SPTADDR_STREET_LINE3     = l_address_rec.address_line_3
         -- NO STREET LINE 4 FIELD YET /*PREBANNER82*/,SPTADDR_STREET_LINE4     = l_address_rec.address_line_4
         -- NO HOUSE NUMBER FIELD YET /*PREBANNER82*/,SPTADDR_HOUSE_NUMBER     = l_address_rec.parsed_address_number
         ,SPTADDR_CITY             = l_address_rec.city
         ,SPTADDR_STAT_CODE        = NVL(l_address_rec.state, l_address_rec.province)
         ,SPTADDR_ZIP              = l_address_rec.postal_code
         ,SPTADDR_CNTY_CODE        = l_address_rec.county_code
         --CLNMOD--,SPTADDR_DELIVERY_POINT   = l_address_rec.delivery_point
         --CLNMOD--,SPTADDR_CORRECTION_DIGIT = l_address_rec.check_digit
         --CLNMOD--,SPTADDR_CARRIER_ROUTE    = l_address_rec.carrier_route
         --CLNMOD--,SPTADDR_REVIEWED_IND     = l_address_rec.address_line_7
         --CLNMOD--,SPTADDR_REVIEWED_USER    = l_address_rec.address_line_8
      where rowid = c_addr_rec.rowid;
      --
      -- Commit every 50 records
      --
      if mod(li_count+1, 50) = 0 then
        commit;
      end if;
    end if;
    --
    -- Increment the counter
    --
    li_count := li_count + 1;
    --
    -- Leave if we've reached the max counter
    --
    if li_count >= NVL(fn_max_verify, li_count+1) then
      EXIT;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      lv_plsql_error := NVL(lv_plsql_error, SQLERRM);
      --
      -- fv_plsql_error was found - Oracle Pipe error
      --
      -- Show the error message and input/verified address
      dbms_output.put_line(chr(10)||'Unable to Verify Address:'
                ||chr(10)||lv_plsql_error);
      dbms_output.put_line('*** Primary Key: '
                         ||to_char(c_addr_rec.SPTADDR_PIDM)
                    ||'-'||c_addr_rec.SPTADDR_ATYP_CODE
                    ||'-'||to_char(c_addr_rec.SPTADDR_SEQNO)
                    );
      dbms_output.put_line('*** INPUT Address: '
                ||chr(10)||c_addr_rec.address_line_1
                ||ltrim(chr(10)||c_addr_rec.address_line_2
                ||chr(10)||c_addr_rec.address_line_3, chr(10))
                ||chr(10)||c_addr_rec.city||', '||c_addr_rec.state||' '||c_addr_rec.postal_code
                );
      dbms_output.put_line('*** VERIFIED Address: '
                ||chr(10)||l_address_rec.address_line_1
                ||ltrim(chr(10)||l_address_rec.address_line_2
                ||chr(10)||l_address_rec.address_line_3, chr(10))
                ||chr(10)||l_address_rec.city||', '||l_address_rec.state||' '||l_address_rec.postal_code
                );
      -- Increment error count
      li_error_count := li_error_count + 1;
      -- Leave if we get more than 50 errors
      if li_error_count > 50 then
        EXIT;
      end if;
  END;
  END LOOP;
  CLOSE c_addr;
  commit;
  --
  -- Get the end time
  --
  li_end := dbms_utility.get_time;
  --
  -- Display the Output
  --
  dbms_output.put_line(chr(10));
  -- Show Update Flag
  if fb_update then
    commit;
    dbms_output.put_line('Record Update : ON');
  else
    dbms_output.put_line('Record Update : OFF');
  end if;
  --
  -- Show Statistics
  --
  dbms_output.put_line('# Records  : '||to_char(li_count));
  dbms_output.put_line('# Errors   : '||to_char(li_error_count));
  dbms_output.put_line('Total Time : '||to_char((li_end - li_start)/100)||' sec');
  if (li_end - li_start) != 0 then
    dbms_output.put_line('# Rec/Hour : '||to_char(round(360000*li_count/(li_end - li_start))));
  end if;
  --
  -- Call the Post Batch Address Verify User Exit
  --
  CLEAN_Address_Banner_UE.Post_Batch_Address_Verify;
  --
  -- Show the error summary
  --
  Show_Results;
  -- Delete the results table to clean up
  Delete_Results;
END Batch_Verify_SPTADDR;
/******************************************************************************************
 *  Procedure Name  :   Batch_Verify_ROTADDR
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the Banner Financial Aid Temporary SPRADDR Table (ROTADDR)
 *                      * fn_max_verify
 *                           - maximum number of address to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                        NOTE: This option only works if you have made the CLEAN_Address
 *                              mods to this table for missing address columns
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fv_address_type
 *                           - Banner Address Type (ROTADDR_ATYP_CODE) - only verify this type
 *                           - NULL = to verify all address types
 *                      * fn_days_back
 *                           - Verify Addresses that were updated/created this many days back
 *                             - Note: You can enter fractional days such as 0.25, 0.5, etc
 *                           - NULL = all addresses
 *                      * fb_skip_international
 *                           - Skip international address verification in batch mode
 *                      * fb_verify_inactive
 *                           - Verify inactive addresses as well as active ones
 *                           - This essentially ignores the "to date" and status indicator on the record
 *                      * fd_from_date / fd_to_date
 *                           - Specify a date range for the verification based on ROTADDR_ACTIVITY_DATE
 *                           - This is useful for segmenting the data to create parallel batch procedures,
 *                             increasing throughput
 ******************************************************************************************/
PROCEDURE Batch_Verify_ROTADDR (
                     fn_max_verify                  IN     NUMBER   DEFAULT 1000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fv_address_type                IN     VARCHAR2 DEFAULT NULL /* ROTADDR_ATYP_CODE */
                    ,fn_days_back                   IN     NUMBER   DEFAULT NULL
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    ,fb_verify_inactive             IN     BOOLEAN  DEFAULT FALSE
                    ,fd_from_date                   IN     DATE     DEFAULT NULL
                    ,fd_to_date                     IN     DATE     DEFAULT NULL
                    )
IS
  ln_only_unverified    NUMBER := 0;
  ln_skip_international NUMBER := 0;
  ln_verify_inactive    NUMBER := 0;
  --
  -- Define the cursor to get the addresses from Banner
  --
  cursor c_addr IS
    select rowid
          ,ROTADDR_PIDM
          ,ROTADDR_ATYP_CODE
          ,ROTADDR_SEQNO
          ,ROTADDR_STREET_LINE1 address_line_1
          ,ROTADDR_STREET_LINE2 address_line_2
          ,ROTADDR_STREET_LINE3 address_line_3
          /*PREBANNER82*/,ROTADDR_STREET_LINE4  address_line_4
          /*PREBANNER82*/,ROTADDR_HOUSE_NUMBER  house_number
          ,ROTADDR_CITY         city
          ,ROTADDR_STAT_CODE    state
          ,ROTADDR_ZIP          postal_code
          ,ROTADDR_CNTY_CODE    county_code
          ,ROTADDR_NATN_CODE    country_code
    from   ROTADDR a
    -- Only active records between from and to date
    where  sysdate >= NVL(ROTADDR_from_date,sysdate-1)
    -- NOTE: Don't use NVL(ROTADDR_to_date,sysdate+1) since future addresses should be updated, too
    and    (  NVL(ltrim(upper(ROTADDR_status_ind)),'A') = 'A'
           or ln_verify_inactive = 1
           )
    -- 2/23/08 - don't overwrite addresses where user checked "Override Address Verification"
    --CLNMOD--and    NVL(a.ROTADDR_REVIEWED_USER,'x') NOT LIKE '%:!'
    -- Only Local Country addresses if fb_Skip_International is set TRUE
    and    (  NVL(a.ROTADDR_NATN_CODE,NVL(gv_local_country,'x')) = NVL(gv_local_country,'x')
           or ln_skip_international = 0
           )
    and    (  fv_address_type IS NULL
           or ROTADDR_ATYP_CODE = fv_address_type
           )
    -- Only get addresses not verified if specified
    --CLNMOD--and    (ln_only_unverified = 0 or ltrim(ROTADDR_REVIEWED_USER) IS NULL)
    and    a.ROTADDR_ACTIVITY_DATE between NVL(trunc(fd_from_date),a.ROTADDR_ACTIVITY_DATE)
                                       and NVL(trunc(fd_to_date)+0.99999,a.ROTADDR_ACTIVITY_DATE)
    and    (  fn_days_back IS NULL
           or ROTADDR_ACTIVITY_DATE > sysdate - fn_days_back
           );
  c_addr_rec c_addr%ROWTYPE;
  -- CLEAN_Address record structures
  l_address_rec       CLEAN_Address.Address_rec;
  l_address_rec_NULL  CLEAN_Address.Address_rec;
  -- Internal Statistic Variables
  li_start            NUMBER;
  li_end              NUMBER;
  li_count            NUMBER := 0;
  li_error_count      NUMBER := 0;
  lv_plsql_error      VARCHAR2(2000);
BEGIN
  dbms_output.enable(1000000);
  --
  -- Call the Pre Batch Address Verify User Exit
  --
  CLEAN_Address_Banner_UE.Pre_Batch_Address_Verify;
  -- Clear the results table
  Delete_Results;
  -- Set the only unverified flag
  if fb_only_unverified then
    ln_only_unverified := 1;
  end if;
  -- Set the Skip International flag
  if fb_skip_international then
    ln_skip_international := 1;
    Set_Skip_International(TRUE);
  else
    Set_Skip_International(FALSE);
  end if;
  -- Set the Verify Inactive flag
  if fb_verify_inactive then
    ln_verify_inactive := 1;
  end if;
  --
  -- Start the timer
  --
  li_start := dbms_utility.get_time;
  --
  -- Loop through all addresses from the cursor
  --
  OPEN c_addr;
  LOOP
  BEGIN
    FETCH c_addr
    INTO  c_addr_rec;
    EXIT WHEN c_addr%NOTFOUND;
    --
    -- Initialize address record to NULL
    --
    l_address_rec := l_address_rec_NULL;
    --
    -- Assign the input variables to the address record
    --
    l_address_rec.Address_Line_1 := c_addr_rec.address_line_1;
    l_address_rec.Address_Line_2 := c_addr_rec.address_line_2;
    l_address_rec.Address_Line_3 := c_addr_rec.address_line_3;
    /*PREBANNER82*/l_address_rec.Address_Line_4 := c_addr_rec.address_line_4;
    /*PREBANNER82*/l_address_rec.Parsed_Address_Number := c_addr_rec.house_number;
    l_address_rec.City           := c_addr_rec.City;
    l_address_rec.State          := c_addr_rec.State;
    l_address_rec.Postal_code    := c_addr_rec.postal_code;
    l_address_rec.county_code    := c_addr_rec.county_code;
    l_address_rec.country_code   := c_addr_rec.country_code;
    --
    -- Verify the address
    --
    Verify_Address_Record(
                  f_address_rec   => l_address_rec
                 ,fv_plsql_error  => lv_plsql_error
                 ,fv_address_type => c_addr_rec.ROTADDR_ATYP_CODE
                 ,fv_object_name  => 'Batch_Verify_ROTADDR'
                 );
    -- Make sure there wasn't an Oracle Pipe error
    if lv_plsql_error IS NOT NULL then
      raise NO_DATA_FOUND;
    end if;
    --
    -- Add the error statistic to the output table
    --
    Add_Result(l_address_rec.error_code, l_address_rec.error_string);
    --
    -- See if we need to update the record
    --
    if fb_update then
      --
      -- Update the existing ROTADDR Banner Address record
      --
      update ROTADDR set
          ROTADDR_STREET_LINE1     = l_address_rec.address_line_1
         ,ROTADDR_STREET_LINE2     = l_address_rec.address_line_2
         ,ROTADDR_STREET_LINE3     = l_address_rec.address_line_3
         /*PREBANNER82*/,ROTADDR_STREET_LINE4     = l_address_rec.address_line_4
         /*PREBANNER82*/,ROTADDR_HOUSE_NUMBER     = l_address_rec.parsed_address_number
         ,ROTADDR_CITY             = l_address_rec.city
         ,ROTADDR_STAT_CODE        = NVL(l_address_rec.state, l_address_rec.province)
         ,ROTADDR_ZIP              = l_address_rec.postal_code
         ,ROTADDR_CNTY_CODE        = l_address_rec.county_code
         --CLNMOD--,ROTADDR_DELIVERY_POINT   = l_address_rec.delivery_point
         --CLNMOD--,ROTADDR_CORRECTION_DIGIT = l_address_rec.check_digit
         --CLNMOD--,ROTADDR_CARRIER_ROUTE    = l_address_rec.carrier_route
         --CLNMOD--,ROTADDR_REVIEWED_IND     = l_address_rec.address_line_7
         --CLNMOD--,ROTADDR_REVIEWED_USER    = l_address_rec.address_line_8
      where rowid = c_addr_rec.rowid;
      --
      -- Commit every 50 records
      --
      if mod(li_count+1, 50) = 0 then
        commit;
      end if;
    end if;
    --
    -- Increment the counter
    --
    li_count := li_count + 1;
    --
    -- Leave if we've reached the max counter
    --
    if li_count >= NVL(fn_max_verify, li_count+1) then
      EXIT;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      lv_plsql_error := NVL(lv_plsql_error, SQLERRM);
      --
      -- fv_plsql_error was found - Oracle Pipe error
      --
      -- Show the error message and input/verified address
      dbms_output.put_line(chr(10)||'Unable to Verify Address:'
                ||chr(10)||lv_plsql_error);
      dbms_output.put_line('*** Primary Key: '
                         ||to_char(c_addr_rec.ROTADDR_PIDM)
                    ||'-'||c_addr_rec.ROTADDR_ATYP_CODE
                    ||'-'||to_char(c_addr_rec.ROTADDR_SEQNO)
                    );
      dbms_output.put_line('*** INPUT Address: '
                ||chr(10)||c_addr_rec.address_line_1
                ||ltrim(chr(10)||c_addr_rec.address_line_2
                ||chr(10)||c_addr_rec.address_line_3, chr(10))
                ||chr(10)||c_addr_rec.city||', '||c_addr_rec.state||' '||c_addr_rec.postal_code
                );
      dbms_output.put_line('*** VERIFIED Address: '
                ||chr(10)||l_address_rec.address_line_1
                ||ltrim(chr(10)||l_address_rec.address_line_2
                ||chr(10)||l_address_rec.address_line_3, chr(10))
                ||chr(10)||l_address_rec.city||', '||l_address_rec.state||' '||l_address_rec.postal_code
                );
      -- Increment error count
      li_error_count := li_error_count + 1;
      -- Leave if we get more than 50 errors
      if li_error_count > 50 then
        EXIT;
      end if;
  END;
  END LOOP;
  CLOSE c_addr;
  commit;
  --
  -- Get the end time
  --
  li_end := dbms_utility.get_time;
  --
  -- Display the Output
  --
  dbms_output.put_line(chr(10));
  -- Show Update Flag
  if fb_update then
    commit;
    dbms_output.put_line('Record Update : ON');
  else
    dbms_output.put_line('Record Update : OFF');
  end if;
  --
  -- Show Statistics
  --
  dbms_output.put_line('# Records  : '||to_char(li_count));
  dbms_output.put_line('# Errors   : '||to_char(li_error_count));
  dbms_output.put_line('Total Time : '||to_char((li_end - li_start)/100)||' sec');
  if (li_end - li_start) != 0 then
    dbms_output.put_line('# Rec/Hour : '||to_char(round(360000*li_count/(li_end - li_start))));
  end if;
  --
  -- Call the Post Batch Address Verify User Exit
  --
  CLEAN_Address_Banner_UE.Post_Batch_Address_Verify;
  --
  -- Show the error summary
  --
  Show_Results;
  -- Delete the results table to clean up
  Delete_Results;
END Batch_Verify_ROTADDR;
/******************************************************************************************
 *  Procedure Name  :   Batch_Verify_SRTADDR
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the Banner Electronic Prospect Address Table (SRTADDR)
 *                      * fn_max_verify
 *                           - maximum number of address to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                        NOTE: This option only works if you have made the CLEAN_Address
 *                              mods to this table for missing address columns
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fv_address_type
 *                           - Banner Address Type (SRTADDR_ATYP_CODE) - only verify this type
 *                           - NULL = to verify all address types
 *                      * fn_days_back
 *                           - Verify Addresses that were updated/created this many days back
 *                             - Note: You can enter fractional days such as 0.25, 0.5, etc
 *                           - NULL = all addresses
 *                      * fb_skip_international
 *                           - Skip international address verification in batch mode
 *                      * fb_verify_inactive
 *                           - Verify inactive addresses as well as active ones
 *                           - This essentially ignores the "to date" and status indicator on the record
 *                      * fd_from_date / fd_to_date
 *                           - Specify a date range for the verification based on SRTADDR_ACTIVITY_DATE
 *                           - This is useful for segmenting the data to create parallel batch procedures,
 *                             increasing throughput
 ******************************************************************************************/
PROCEDURE Batch_Verify_SRTADDR (
                     fn_max_verify                  IN     NUMBER   DEFAULT 1000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fv_address_type                IN     VARCHAR2 DEFAULT NULL /* SRTADDR_ATYP_CODE */
                    ,fn_days_back                   IN     NUMBER   DEFAULT NULL
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    ,fb_verify_inactive             IN     BOOLEAN  DEFAULT FALSE
                    ,fd_from_date                   IN     DATE     DEFAULT NULL
                    ,fd_to_date                     IN     DATE     DEFAULT NULL
                    )
IS
  ln_only_unverified    NUMBER := 0;
  ln_skip_international NUMBER := 0;
  ln_verify_inactive    NUMBER := 0;
  --
  -- Define the cursor to get the addresses from Banner
  --
  cursor c_addr IS
    select rowid
          ,SRTADDR_RIDM
          ,SRTADDR_ATYP_CODE
          ,SRTADDR_SEQNO
          ,SRTADDR_STREET_LINE1 address_line_1
          ,SRTADDR_STREET_LINE2 address_line_2
          ,SRTADDR_STREET_LINE3 address_line_3
          /*PREBANNER82*/,SRTADDR_STREET_LINE4  address_line_4
          /*PREBANNER82*/,SRTADDR_HOUSE_NUMBER  house_number
          ,SRTADDR_CITY         city
          ,SRTADDR_STAT_CODE    state
          ,SRTADDR_ZIP          postal_code
          ,SRTADDR_CNTY_CODE    county_code
          ,SRTADDR_NATN_CODE    country_code
    from   SRTADDR a
    -- Only active records between from and to date
    where  sysdate >= NVL(SRTADDR_from_date,sysdate-1)
    -- NOTE: Don't use NVL(SRTADDR_to_date,sysdate+1) since future addresses should be updated, too
    and    (  NVL(ltrim(upper(SRTADDR_status_ind)),'A') = 'A'
           or ln_verify_inactive = 1
           )
    -- 2/23/08 - don't overwrite addresses where user checked "Override Address Verification"
    --CLNMOD--and    NVL(a.SRTADDR_REVIEWED_USER,'x') NOT LIKE '%:!'
    -- Only Local Country addresses if fb_Skip_International is set TRUE
    and    (  NVL(a.SRTADDR_NATN_CODE,NVL(gv_local_country,'x')) = NVL(gv_local_country,'x')
           or ln_skip_international = 0
           )
    and    (  fv_address_type IS NULL
           or SRTADDR_ATYP_CODE = fv_address_type
           )
    -- Only get addresses not verified if specified
    --CLNMOD--and    (ln_only_unverified = 0 or ltrim(SRTADDR_REVIEWED_USER) IS NULL)
    and    a.SRTADDR_ACTIVITY_DATE between NVL(trunc(fd_from_date),a.SRTADDR_ACTIVITY_DATE)
                                       and NVL(trunc(fd_to_date)+0.99999,a.SRTADDR_ACTIVITY_DATE)
    and    (  fn_days_back IS NULL
           or SRTADDR_ACTIVITY_DATE > sysdate - fn_days_back
           );
  c_addr_rec c_addr%ROWTYPE;
  -- CLEAN_Address record structures
  l_address_rec       CLEAN_Address.Address_rec;
  l_address_rec_NULL  CLEAN_Address.Address_rec;
  -- Internal Statistic Variables
  li_start            NUMBER;
  li_end              NUMBER;
  li_count            NUMBER := 0;
  li_error_count      NUMBER := 0;
  lv_plsql_error      VARCHAR2(2000);
BEGIN
  dbms_output.enable(1000000);
  --
  -- Call the Pre Batch Address Verify User Exit
  --
  CLEAN_Address_Banner_UE.Pre_Batch_Address_Verify;
  -- Clear the results table
  Delete_Results;
  -- Set the only unverified flag
  if fb_only_unverified then
    ln_only_unverified := 1;
  end if;
  -- Set the Skip International flag
  if fb_skip_international then
    ln_skip_international := 1;
    Set_Skip_International(TRUE);
  else
    Set_Skip_International(FALSE);
  end if;
  -- Set the Verify Inactive flag
  if fb_verify_inactive then
    ln_verify_inactive := 1;
  end if;
  --
  -- Start the timer
  --
  li_start := dbms_utility.get_time;
  --
  -- Loop through all addresses from the cursor
  --
  OPEN c_addr;
  LOOP
  BEGIN
    FETCH c_addr
    INTO  c_addr_rec;
    EXIT WHEN c_addr%NOTFOUND;
    --
    -- Initialize address record to NULL
    --
    l_address_rec := l_address_rec_NULL;
    --
    -- Assign the input variables to the address record
    --
    l_address_rec.Address_Line_1 := c_addr_rec.address_line_1;
    l_address_rec.Address_Line_2 := c_addr_rec.address_line_2;
    l_address_rec.Address_Line_3 := c_addr_rec.address_line_3;
    /*PREBANNER82*/l_address_rec.Address_Line_4 := c_addr_rec.address_line_4;
    /*PREBANNER82*/l_address_rec.Parsed_Address_Number := c_addr_rec.house_number;
    l_address_rec.City           := c_addr_rec.City;
    l_address_rec.State          := c_addr_rec.State;
    l_address_rec.Postal_code    := c_addr_rec.postal_code;
    l_address_rec.county_code    := c_addr_rec.county_code;
    l_address_rec.country_code   := c_addr_rec.country_code;
    --
    -- Verify the address
    --
    Verify_Address_Record(
                  f_address_rec   => l_address_rec
                 ,fv_plsql_error  => lv_plsql_error
                 ,fv_address_type => c_addr_rec.SRTADDR_ATYP_CODE
                 ,fv_object_name  => 'Batch_Verify_SRTADDR'
                 );
    -- Make sure there wasn't an Oracle Pipe error
    if lv_plsql_error IS NOT NULL then
      raise NO_DATA_FOUND;
    end if;
    --
    -- Add the error statistic to the output table
    --
    Add_Result(l_address_rec.error_code, l_address_rec.error_string);
    --
    -- See if we need to update the record
    --
    if fb_update then
      --
      -- Update the existing SRTADDR Banner Address record
      --
      update SRTADDR set
          SRTADDR_STREET_LINE1     = l_address_rec.address_line_1
         ,SRTADDR_STREET_LINE2     = l_address_rec.address_line_2
         ,SRTADDR_STREET_LINE3     = l_address_rec.address_line_3
         /*PREBANNER82*/,SRTADDR_STREET_LINE4     = l_address_rec.address_line_4
         /*PREBANNER82*/,SRTADDR_HOUSE_NUMBER     = l_address_rec.parsed_address_number
         ,SRTADDR_CITY             = l_address_rec.city
         ,SRTADDR_STAT_CODE        = NVL(l_address_rec.state, l_address_rec.province)
         ,SRTADDR_ZIP              = l_address_rec.postal_code
         ,SRTADDR_CNTY_CODE        = l_address_rec.county_code
         --CLNMOD--,SRTADDR_DELIVERY_POINT   = l_address_rec.delivery_point
         --CLNMOD--,SRTADDR_CORRECTION_DIGIT = l_address_rec.check_digit
         --CLNMOD--,SRTADDR_CARRIER_ROUTE    = l_address_rec.carrier_route
         --CLNMOD--,SRTADDR_REVIEWED_IND     = l_address_rec.address_line_7
         --CLNMOD--,SRTADDR_REVIEWED_USER    = l_address_rec.address_line_8
      where rowid = c_addr_rec.rowid;
      --
      -- Commit every 50 records
      --
      if mod(li_count+1, 50) = 0 then
        commit;
      end if;
    end if;
    --
    -- Increment the counter
    --
    li_count := li_count + 1;
    --
    -- Leave if we've reached the max counter
    --
    if li_count >= NVL(fn_max_verify, li_count+1) then
      EXIT;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      lv_plsql_error := NVL(lv_plsql_error, SQLERRM);
      --
      -- fv_plsql_error was found - Oracle Pipe error
      --
      -- Show the error message and input/verified address
      dbms_output.put_line(chr(10)||'Unable to Verify Address:'
                ||chr(10)||lv_plsql_error);
      dbms_output.put_line('*** Primary Key: '
                         ||to_char(c_addr_rec.SRTADDR_RIDM)
                    ||'-'||c_addr_rec.SRTADDR_ATYP_CODE
                    ||'-'||to_char(c_addr_rec.SRTADDR_SEQNO)
                    );
      dbms_output.put_line('*** INPUT Address: '
                ||chr(10)||c_addr_rec.address_line_1
                ||ltrim(chr(10)||c_addr_rec.address_line_2
                ||chr(10)||c_addr_rec.address_line_3, chr(10))
                ||chr(10)||c_addr_rec.city||', '||c_addr_rec.state||' '||c_addr_rec.postal_code
                );
      dbms_output.put_line('*** VERIFIED Address: '
                ||chr(10)||l_address_rec.address_line_1
                ||ltrim(chr(10)||l_address_rec.address_line_2
                ||chr(10)||l_address_rec.address_line_3, chr(10))
                ||chr(10)||l_address_rec.city||', '||l_address_rec.state||' '||l_address_rec.postal_code
                );
      -- Increment error count
      li_error_count := li_error_count + 1;
      -- Leave if we get more than 50 errors
      if li_error_count > 50 then
        EXIT;
      end if;
  END;
  END LOOP;
  CLOSE c_addr;
  commit;
  --
  -- Get the end time
  --
  li_end := dbms_utility.get_time;
  --
  -- Display the Output
  --
  dbms_output.put_line(chr(10));
  -- Show Update Flag
  if fb_update then
    commit;
    dbms_output.put_line('Record Update : ON');
  else
    dbms_output.put_line('Record Update : OFF');
  end if;
  --
  -- Show Statistics
  --
  dbms_output.put_line('# Records  : '||to_char(li_count));
  dbms_output.put_line('# Errors   : '||to_char(li_error_count));
  dbms_output.put_line('Total Time : '||to_char((li_end - li_start)/100)||' sec');
  if (li_end - li_start) != 0 then
    dbms_output.put_line('# Rec/Hour : '||to_char(round(360000*li_count/(li_end - li_start))));
  end if;
  --
  -- Call the Post Batch Address Verify User Exit
  --
  CLEAN_Address_Banner_UE.Post_Batch_Address_Verify;
  --
  -- Show the error summary
  --
  Show_Results;
  -- Delete the results table to clean up
  Delete_Results;
END Batch_Verify_SRTADDR;
/******************************************************************************************
 *  Procedure Name  :   Batch_Verify_SOBSBGI
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the Banner Electronic Prospect Address Table (SOBSBGI)
 *                      * fn_max_verify
 *                           - maximum number of address to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                        NOTE: This option only works if you have made the CLEAN_Address
 *                              mods to this table for missing address columns
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fb_skip_international
 *                           - Skip international address verification in batch mode
 ******************************************************************************************/
PROCEDURE Batch_Verify_SOBSBGI (
                     fn_max_verify                  IN     NUMBER   DEFAULT 1000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    )
IS
  ln_only_unverified    NUMBER := 0;
  ln_skip_international NUMBER := 0;
  --
  -- Define the cursor to get the addresses from Banner
  --
  cursor c_addr IS
    select rowid
          ,SOBSBGI_SBGI_CODE
          ,SOBSBGI_STREET_LINE1 address_line_1
          ,SOBSBGI_STREET_LINE2 address_line_2
          ,SOBSBGI_STREET_LINE3 address_line_3
          /*PREBANNER82*/,SOBSBGI_STREET_LINE4  address_line_4
          /*PREBANNER82*/,SOBSBGI_HOUSE_NUMBER  house_number
          ,SOBSBGI_CITY         city
          ,SOBSBGI_STAT_CODE    state
          ,SOBSBGI_ZIP          postal_code
          ,SOBSBGI_CNTY_CODE    county_code
          ,SOBSBGI_NATN_CODE    country_code
    from   SOBSBGI a
    where
    -- Only Local Country addresses if fb_Skip_International is set TRUE
           (  NVL(a.SOBSBGI_NATN_CODE,NVL(gv_local_country,'x')) = NVL(gv_local_country,'x')
           or ln_skip_international = 0
           )
    -- Only get addresses not verified if specified
    --CLNMOD--and    (ln_only_unverified = 0 or ltrim(SOBSBGI_REVIEWED_USER) IS NULL)
    -- 2/23/08 - don't overwrite addresses where user checked "Override Address Verification"
    --CLNMOD--and    NVL(a.SOBSBGI_REVIEWED_USER,'x') NOT LIKE '%:!'
    ;
  c_addr_rec c_addr%ROWTYPE;
  -- CLEAN_Address record structures
  l_address_rec       CLEAN_Address.Address_rec;
  l_address_rec_NULL  CLEAN_Address.Address_rec;
  -- Internal Statistic Variables
  li_start            NUMBER;
  li_end              NUMBER;
  li_count            NUMBER := 0;
  li_error_count      NUMBER := 0;
  lv_plsql_error      VARCHAR2(2000);
BEGIN
  dbms_output.enable(1000000);
  --
  -- Call the Pre Batch Address Verify User Exit
  --
  CLEAN_Address_Banner_UE.Pre_Batch_Address_Verify;
  -- Clear the results table
  Delete_Results;
  -- Set the only unverified flag
  if fb_only_unverified then
    ln_only_unverified := 1;
  end if;
  -- Set the Skip International flag
  if fb_skip_international then
    ln_skip_international := 1;
    Set_Skip_International(TRUE);
  else
    Set_Skip_International(FALSE);
  end if;
  --
  -- Start the timer
  --
  li_start := dbms_utility.get_time;
  --
  -- Loop through all addresses from the cursor
  --
  OPEN c_addr;
  LOOP
  BEGIN
    FETCH c_addr
    INTO  c_addr_rec;
    EXIT WHEN c_addr%NOTFOUND;
    --
    -- Initialize address record to NULL
    --
    l_address_rec := l_address_rec_NULL;
    --
    -- Assign the input variables to the address record
    --
    l_address_rec.Address_Line_1 := c_addr_rec.address_line_1;
    l_address_rec.Address_Line_2 := c_addr_rec.address_line_2;
    l_address_rec.Address_Line_3 := c_addr_rec.address_line_3;
    /*PREBANNER82*/l_address_rec.Address_Line_4 := c_addr_rec.address_line_4;
    /*PREBANNER82*/l_address_rec.Parsed_Address_Number := c_addr_rec.house_number;
    l_address_rec.City           := c_addr_rec.City;
    l_address_rec.State          := c_addr_rec.State;
    l_address_rec.county_code    := c_addr_rec.county_code;
    l_address_rec.Postal_code    := c_addr_rec.postal_code;
    l_address_rec.county_code    := c_addr_rec.county_code;
    l_address_rec.country_code   := c_addr_rec.country_code;
    --
    -- Verify the address
    --
    Verify_Address_Record(
                  f_address_rec   => l_address_rec
                 ,fv_plsql_error  => lv_plsql_error
                 ,fv_address_type => 'SOBSBGI'
                 ,fv_object_name  => 'Batch_Verify_SOBSBGI'
                 );
    -- Make sure there wasn't an Oracle Pipe error
    if lv_plsql_error IS NOT NULL then
      raise NO_DATA_FOUND;
    end if;
    --
    -- Add the error statistic to the output table
    --
    Add_Result(l_address_rec.error_code, l_address_rec.error_string);
    --
    -- See if we need to update the record
    --
    if fb_update then
      --
      -- Update the existing SOBSBGI Banner Address record
      --
      update SOBSBGI set
          SOBSBGI_STREET_LINE1     = l_address_rec.address_line_1
         ,SOBSBGI_STREET_LINE2     = l_address_rec.address_line_2
         ,SOBSBGI_STREET_LINE3     = l_address_rec.address_line_3
         /*PREBANNER82*/,SOBSBGI_STREET_LINE4     = l_address_rec.address_line_4
         /*PREBANNER82*/,SOBSBGI_HOUSE_NUMBER     = l_address_rec.parsed_address_number
         ,SOBSBGI_CITY             = l_address_rec.city
         ,SOBSBGI_STAT_CODE        = NVL(l_address_rec.state, l_address_rec.province)
         ,SOBSBGI_ZIP              = l_address_rec.postal_code
         ,SOBSBGI_CNTY_CODE        = l_address_rec.county_code
         --CLNMOD--,SOBSBGI_DELIVERY_POINT   = l_address_rec.delivery_point
         --CLNMOD--,SOBSBGI_CORRECTION_DIGIT = l_address_rec.check_digit
         --CLNMOD--,SOBSBGI_CARRIER_ROUTE    = l_address_rec.carrier_route
         --CLNMOD--,SOBSBGI_REVIEWED_IND     = l_address_rec.address_line_7
         --CLNMOD--,SOBSBGI_REVIEWED_USER    = l_address_rec.address_line_8
      where rowid = c_addr_rec.rowid;
      --
      -- Commit every 50 records
      --
      if mod(li_count+1, 50) = 0 then
        commit;
      end if;
    end if;
    --
    -- Increment the counter
    --
    li_count := li_count + 1;
    --
    -- Leave if we've reached the max counter
    --
    if li_count >= NVL(fn_max_verify, li_count+1) then
      EXIT;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      lv_plsql_error := NVL(lv_plsql_error, SQLERRM);
      --
      -- fv_plsql_error was found - Oracle Pipe error
      --
      -- Show the error message and input/verified address
      dbms_output.put_line(chr(10)||'Unable to Verify Address:'
                ||chr(10)||lv_plsql_error);
      dbms_output.put_line('*** Primary Key: '
                         ||c_addr_rec.SOBSBGI_SBGI_CODE
                         );
      dbms_output.put_line('*** INPUT Address: '
                ||chr(10)||c_addr_rec.address_line_1
                ||ltrim(chr(10)||c_addr_rec.address_line_2
                ||chr(10)||c_addr_rec.address_line_3, chr(10))
                ||chr(10)||c_addr_rec.city||', '||c_addr_rec.state||' '||c_addr_rec.postal_code
                );
      dbms_output.put_line('*** VERIFIED Address: '
                ||chr(10)||l_address_rec.address_line_1
                ||ltrim(chr(10)||l_address_rec.address_line_2
                ||chr(10)||l_address_rec.address_line_3, chr(10))
                ||chr(10)||l_address_rec.city||', '||l_address_rec.state||' '||l_address_rec.postal_code
                );
      -- Increment error count
      li_error_count := li_error_count + 1;
      -- Leave if we get more than 50 errors
      if li_error_count > 50 then
        EXIT;
      end if;
  END;
  END LOOP;
  CLOSE c_addr;
  commit;
  --
  -- Get the end time
  --
  li_end := dbms_utility.get_time;
  --
  -- Display the Output
  --
  dbms_output.put_line(chr(10));
  -- Show Update Flag
  if fb_update then
    commit;
    dbms_output.put_line('Record Update : ON');
  else
    dbms_output.put_line('Record Update : OFF');
  end if;
  --
  -- Show Statistics
  --
  dbms_output.put_line('# Records  : '||to_char(li_count));
  dbms_output.put_line('# Errors   : '||to_char(li_error_count));
  dbms_output.put_line('Total Time : '||to_char((li_end - li_start)/100)||' sec');
  if (li_end - li_start) != 0 then
    dbms_output.put_line('# Rec/Hour : '||to_char(round(360000*li_count/(li_end - li_start))));
  end if;
  --
  -- Call the Post Batch Address Verify User Exit
  --
  CLEAN_Address_Banner_UE.Post_Batch_Address_Verify;
  --
  -- Show the error summary
  --
  Show_Results;
  -- Delete the results table to clean up
  Delete_Results;
END Batch_Verify_SOBSBGI;
/******************************************************************************************
 *  Procedure Name  :   Batch_Verify_SRTHSCH
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the Banner Electronic Prospect Address Table (SRTHSCH)
 *                      * fn_max_verify
 *                           - maximum number of address to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                        NOTE: This option only works if you have made the CLEAN_Address
 *                              mods to this table for missing address columns
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fn_days_back
 *                           - Verify Addresses that were updated/created this many days back
 *                             - Note: You can enter fractional days such as 0.25, 0.5, etc
 *                           - NULL = all addresses
 *                      * fb_skip_international
 *                           - Skip international address verification in batch mode
 ******************************************************************************************/
PROCEDURE Batch_Verify_SRTHSCH (
                     fn_max_verify                  IN     NUMBER   DEFAULT 1000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fn_days_back                   IN     NUMBER   DEFAULT NULL
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    )
IS
  ln_only_unverified    NUMBER := 0;
  ln_skip_international NUMBER := 0;
  --
  -- Define the cursor to get the addresses from Banner
  --
  cursor c_addr IS
    select rowid
          ,SRTHSCH_RIDM
          ,SRTHSCH_SBGI_CODE
          ,SRTHSCH_SBGI_DESC
          ,SRTHSCH_STREET_LINE1 address_line_1
          ,SRTHSCH_STREET_LINE2 address_line_2
          ,SRTHSCH_STREET_LINE3 address_line_3
          /*PREBANNER82*/,SRTHSCH_STREET_LINE4  address_line_4
          /*PREBANNER82*/,SRTHSCH_HOUSE_NUMBER  house_number
          ,SRTHSCH_CITY         city
          ,SRTHSCH_STAT_CODE    state
          ,SRTHSCH_ZIP          postal_code
          ,SRTHSCH_NATN_CODE    country_code
    from   SRTHSCH a
    where  (  fn_days_back IS NULL
           or SRTHSCH_ACTIVITY_DATE > sysdate - fn_days_back
           )
    -- Only Local Country addresses if fb_Skip_International is set TRUE
    and    (  NVL(a.SRTHSCH_NATN_CODE,NVL(gv_local_country,'x')) = NVL(gv_local_country,'x')
           or ln_skip_international = 0
           )
    -- Only get addresses not verified if specified
    --CLNMOD--and    (ln_only_unverified = 0 or ltrim(SRTHSCH_REVIEWED_USER) IS NULL)
    -- 2/23/08 - don't overwrite addresses where user checked "Override Address Verification"
    --CLNMOD--and    NVL(a.SRTHSCH_REVIEWED_USER,'x') NOT LIKE '%:!'
    ;
  c_addr_rec c_addr%ROWTYPE;
  -- CLEAN_Address record structures
  l_address_rec       CLEAN_Address.Address_rec;
  l_address_rec_NULL  CLEAN_Address.Address_rec;
  -- Internal Statistic Variables
  li_start            NUMBER;
  li_end              NUMBER;
  li_count            NUMBER := 0;
  li_error_count      NUMBER := 0;
  lv_plsql_error      VARCHAR2(2000);
BEGIN
  dbms_output.enable(1000000);
  --
  -- Call the Pre Batch Address Verify User Exit
  --
  CLEAN_Address_Banner_UE.Pre_Batch_Address_Verify;
  -- Clear the results table
  Delete_Results;
  -- Set the only unverified flag
  if fb_only_unverified then
    ln_only_unverified := 1;
  end if;
  -- Set the Skip International flag
  if fb_skip_international then
    ln_skip_international := 1;
    Set_Skip_International(TRUE);
  else
    Set_Skip_International(FALSE);
  end if;
  --
  -- Start the timer
  --
  li_start := dbms_utility.get_time;
  --
  -- Loop through all addresses from the cursor
  --
  OPEN c_addr;
  LOOP
  BEGIN
    FETCH c_addr
    INTO  c_addr_rec;
    EXIT WHEN c_addr%NOTFOUND;
    --
    -- Initialize address record to NULL
    --
    l_address_rec := l_address_rec_NULL;
    --
    -- Assign the input variables to the address record
    --
    l_address_rec.Address_Line_1 := c_addr_rec.address_line_1;
    l_address_rec.Address_Line_2 := c_addr_rec.address_line_2;
    l_address_rec.Address_Line_3 := c_addr_rec.address_line_3;
    /*PREBANNER82*/l_address_rec.Address_Line_4 := c_addr_rec.address_line_4;
    /*PREBANNER82*/l_address_rec.Parsed_Address_Number := c_addr_rec.house_number;
    l_address_rec.City           := c_addr_rec.City;
    l_address_rec.State          := c_addr_rec.State;
    l_address_rec.Postal_code    := c_addr_rec.postal_code;
    l_address_rec.country_code   := c_addr_rec.country_code;
    --
    -- Verify the address
    --
    Verify_Address_Record(
                  f_address_rec   => l_address_rec
                 ,fv_plsql_error  => lv_plsql_error
                 ,fv_address_type => 'SRTHSCH'
                 ,fv_object_name  => 'Batch_Verify_SRTHSCH'
                 );
    -- Make sure there wasn't an Oracle Pipe error
    if lv_plsql_error IS NOT NULL then
      raise NO_DATA_FOUND;
    end if;
    --
    -- Add the error statistic to the output table
    --
    Add_Result(l_address_rec.error_code, l_address_rec.error_string);
    --
    -- See if we need to update the record
    --
    if fb_update then
      --
      -- Update the existing SRTHSCH Banner Address record
      --
      update SRTHSCH set
          SRTHSCH_STREET_LINE1     = l_address_rec.address_line_1
         ,SRTHSCH_STREET_LINE2     = l_address_rec.address_line_2
         ,SRTHSCH_STREET_LINE3     = l_address_rec.address_line_3
         /*PREBANNER82*/,SRTHSCH_STREET_LINE4     = l_address_rec.address_line_4
         /*PREBANNER82*/,SRTHSCH_HOUSE_NUMBER     = l_address_rec.parsed_address_number
         ,SRTHSCH_CITY             = l_address_rec.city
         ,SRTHSCH_STAT_CODE        = NVL(l_address_rec.state, l_address_rec.province)
         ,SRTHSCH_ZIP              = l_address_rec.postal_code
         --CLNMOD--,SRTHSCH_DELIVERY_POINT   = l_address_rec.delivery_point
         --CLNMOD--,SRTHSCH_CORRECTION_DIGIT = l_address_rec.check_digit
         --CLNMOD--,SRTHSCH_CARRIER_ROUTE    = l_address_rec.carrier_route
         --CLNMOD--,SRTHSCH_REVIEWED_IND     = l_address_rec.address_line_7
         --CLNMOD--,SRTHSCH_REVIEWED_USER    = l_address_rec.address_line_8
      where rowid = c_addr_rec.rowid;
      --
      -- Commit every 50 records
      --
      if mod(li_count+1, 50) = 0 then
        commit;
      end if;
    end if;
    --
    -- Increment the counter
    --
    li_count := li_count + 1;
    --
    -- Leave if we've reached the max counter
    --
    if li_count >= NVL(fn_max_verify, li_count+1) then
      EXIT;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      lv_plsql_error := NVL(lv_plsql_error, SQLERRM);
      --
      -- fv_plsql_error was found - Oracle Pipe error
      --
      -- Show the error message and input/verified address
      dbms_output.put_line(chr(10)||'Unable to Verify Address:'
                ||chr(10)||lv_plsql_error);
      dbms_output.put_line('*** Primary Key: '
                         ||to_char(c_addr_rec.SRTHSCH_RIDM)
                    ||'-'||c_addr_rec.SRTHSCH_SBGI_CODE
                    ||'-'||c_addr_rec.SRTHSCH_SBGI_DESC
                    );
      dbms_output.put_line('*** INPUT Address: '
                ||chr(10)||c_addr_rec.address_line_1
                ||ltrim(chr(10)||c_addr_rec.address_line_2
                ||chr(10)||c_addr_rec.address_line_3, chr(10))
                ||chr(10)||c_addr_rec.city||', '||c_addr_rec.state||' '||c_addr_rec.postal_code
                );
      dbms_output.put_line('*** VERIFIED Address: '
                ||chr(10)||l_address_rec.address_line_1
                ||ltrim(chr(10)||l_address_rec.address_line_2
                ||chr(10)||l_address_rec.address_line_3, chr(10))
                ||chr(10)||l_address_rec.city||', '||l_address_rec.state||' '||l_address_rec.postal_code
                );
      -- Increment error count
      li_error_count := li_error_count + 1;
      -- Leave if we get more than 50 errors
      if li_error_count > 50 then
        EXIT;
      end if;
  END;
  END LOOP;
  CLOSE c_addr;
  commit;
  --
  -- Get the end time
  --
  li_end := dbms_utility.get_time;
  --
  -- Display the Output
  --
  dbms_output.put_line(chr(10));
  -- Show Update Flag
  if fb_update then
    commit;
    dbms_output.put_line('Record Update : ON');
  else
    dbms_output.put_line('Record Update : OFF');
  end if;
  --
  -- Show Statistics
  --
  dbms_output.put_line('# Records  : '||to_char(li_count));
  dbms_output.put_line('# Errors   : '||to_char(li_error_count));
  dbms_output.put_line('Total Time : '||to_char((li_end - li_start)/100)||' sec');
  if (li_end - li_start) != 0 then
    dbms_output.put_line('# Rec/Hour : '||to_char(round(360000*li_count/(li_end - li_start))));
  end if;
  --
  -- Call the Post Batch Address Verify User Exit
  --
  CLEAN_Address_Banner_UE.Post_Batch_Address_Verify;
  --
  -- Show the error summary
  --
  Show_Results;
  -- Delete the results table to clean up
  Delete_Results;
END Batch_Verify_SRTHSCH;
/******************************************************************************************
 *  Procedure Name  :   Batch_Verify_SRTPCOL
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the Banner Electronic Prospect Address Table (SRTPCOL)
 *                      * fn_max_verify
 *                           - maximum number of address to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                        NOTE: This option only works if you have made the CLEAN_Address
 *                              mods to this table for missing address columns
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fn_days_back
 *                           - Verify Addresses that were updated/created this many days back
 *                             - Note: You can enter fractional days such as 0.25, 0.5, etc
 *                           - NULL = all addresses
 *                      * fb_skip_international
 *                           - Skip international address verification in batch mode
 ******************************************************************************************/
PROCEDURE Batch_Verify_SRTPCOL (
                     fn_max_verify                  IN     NUMBER   DEFAULT 1000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fn_days_back                   IN     NUMBER   DEFAULT NULL
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    )
IS
  ln_only_unverified    NUMBER := 0;
  ln_skip_international NUMBER := 0;
  --
  -- Define the cursor to get the addresses from Banner
  --
  cursor c_addr IS
    select rowid
          ,SRTPCOL_RIDM
          ,SRTPCOL_SBGI_CODE
          ,SRTPCOL_SBGI_DESC
          ,SRTPCOL_STREET_LINE1 address_line_1
          ,SRTPCOL_STREET_LINE2 address_line_2
          ,SRTPCOL_STREET_LINE3 address_line_3
          /*PREBANNER82*/,SRTPCOL_STREET_LINE4  address_line_4
          /*PREBANNER82*/,SRTPCOL_HOUSE_NUMBER  house_number
          ,SRTPCOL_CITY         city
          ,SRTPCOL_STAT_CODE    state
          ,SRTPCOL_ZIP          postal_code
          ,SRTPCOL_NATN_CODE    country_code
    from   SRTPCOL a
    where  (  fn_days_back IS NULL
           or SRTPCOL_ACTIVITY_DATE > sysdate - fn_days_back
           )
    -- Only Local Country addresses if fb_Skip_International is set TRUE
    and    (  NVL(a.SRTPCOL_NATN_CODE,NVL(gv_local_country,'x')) = NVL(gv_local_country,'x')
           or ln_skip_international = 0
           )
    -- Only get addresses not verified if specified
    --CLNMOD--and    (ln_only_unverified = 0 or ltrim(SRTPCOL_REVIEWED_USER) IS NULL)
    -- 2/23/08 - don't overwrite addresses where user checked "Override Address Verification"
    --CLNMOD--and    NVL(a.SRTPCOL_REVIEWED_USER,'x') NOT LIKE '%:!'
           ;
  c_addr_rec c_addr%ROWTYPE;
  -- CLEAN_Address record structures
  l_address_rec       CLEAN_Address.Address_rec;
  l_address_rec_NULL  CLEAN_Address.Address_rec;
  -- Internal Statistic Variables
  li_start            NUMBER;
  li_end              NUMBER;
  li_count            NUMBER := 0;
  li_error_count      NUMBER := 0;
  lv_plsql_error      VARCHAR2(2000);
BEGIN
  dbms_output.enable(1000000);
  --
  -- Call the Pre Batch Address Verify User Exit
  --
  CLEAN_Address_Banner_UE.Pre_Batch_Address_Verify;
  -- Clear the results table
  Delete_Results;
  -- Set the only unverified flag
  if fb_only_unverified then
    ln_only_unverified := 1;
  end if;
  -- Set the Skip International flag
  if fb_skip_international then
    ln_skip_international := 1;
    Set_Skip_International(TRUE);
  else
    Set_Skip_International(FALSE);
  end if;
  --
  -- Start the timer
  --
  li_start := dbms_utility.get_time;
  --
  -- Loop through all addresses from the cursor
  --
  OPEN c_addr;
  LOOP
  BEGIN
    FETCH c_addr
    INTO  c_addr_rec;
    EXIT WHEN c_addr%NOTFOUND;
    --
    -- Initialize address record to NULL
    --
    l_address_rec := l_address_rec_NULL;
    --
    -- Assign the input variables to the address record
    --
    l_address_rec.Address_Line_1 := c_addr_rec.address_line_1;
    l_address_rec.Address_Line_2 := c_addr_rec.address_line_2;
    l_address_rec.Address_Line_3 := c_addr_rec.address_line_3;
    /*PREBANNER82*/l_address_rec.Address_Line_4 := c_addr_rec.address_line_4;
    /*PREBANNER82*/l_address_rec.Parsed_Address_Number := c_addr_rec.house_number;
    l_address_rec.City           := c_addr_rec.City;
    l_address_rec.State          := c_addr_rec.State;
    l_address_rec.Postal_code    := c_addr_rec.postal_code;
    l_address_rec.country_code   := c_addr_rec.country_code;
    --
    -- Verify the address
    --
    Verify_Address_Record(
                  f_address_rec   => l_address_rec
                 ,fv_plsql_error  => lv_plsql_error
                 ,fv_address_type => 'SRTPCOL'
                 ,fv_object_name  => 'Batch_Verify_SRTPCOL'
                 );
    -- Make sure there wasn't an Oracle Pipe error
    if lv_plsql_error IS NOT NULL then
      raise NO_DATA_FOUND;
    end if;
    --
    -- Add the error statistic to the output table
    --
    Add_Result(l_address_rec.error_code, l_address_rec.error_string);
    --
    -- See if we need to update the record
    --
    if fb_update then
      --
      -- Update the existing SRTPCOL Banner Address record
      --
      update SRTPCOL set
          SRTPCOL_STREET_LINE1     = l_address_rec.address_line_1
         ,SRTPCOL_STREET_LINE2     = l_address_rec.address_line_2
         ,SRTPCOL_STREET_LINE3     = l_address_rec.address_line_3
         /*PREBANNER82*/,SRTPCOL_STREET_LINE4     = l_address_rec.address_line_4
         /*PREBANNER82*/,SRTPCOL_HOUSE_NUMBER     = l_address_rec.parsed_address_number
         ,SRTPCOL_CITY             = l_address_rec.city
         ,SRTPCOL_STAT_CODE        = NVL(l_address_rec.state, l_address_rec.province)
         ,SRTPCOL_ZIP              = l_address_rec.postal_code
         --CLNMOD--,SRTPCOL_DELIVERY_POINT   = l_address_rec.delivery_point
         --CLNMOD--,SRTPCOL_CORRECTION_DIGIT = l_address_rec.check_digit
         --CLNMOD--,SRTPCOL_CARRIER_ROUTE    = l_address_rec.carrier_route
         --CLNMOD--,SRTPCOL_REVIEWED_IND     = l_address_rec.address_line_7
         --CLNMOD--,SRTPCOL_REVIEWED_USER    = l_address_rec.address_line_8
      where rowid = c_addr_rec.rowid;
      --
      -- Commit every 50 records
      --
      if mod(li_count+1, 50) = 0 then
        commit;
      end if;
    end if;
    --
    -- Increment the counter
    --
    li_count := li_count + 1;
    --
    -- Leave if we've reached the max counter
    --
    if li_count >= NVL(fn_max_verify, li_count+1) then
      EXIT;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      lv_plsql_error := NVL(lv_plsql_error, SQLERRM);
      --
      -- fv_plsql_error was found - Oracle Pipe error
      --
      -- Show the error message and input/verified address
      dbms_output.put_line(chr(10)||'Unable to Verify Address:'
                ||chr(10)||lv_plsql_error);
      dbms_output.put_line('*** Primary Key: '
                         ||to_char(c_addr_rec.SRTPCOL_RIDM)
                    ||'-'||c_addr_rec.SRTPCOL_SBGI_CODE
                    ||'-'||c_addr_rec.SRTPCOL_SBGI_DESC
                    );
      dbms_output.put_line('*** INPUT Address: '
                ||chr(10)||c_addr_rec.address_line_1
                ||ltrim(chr(10)||c_addr_rec.address_line_2
                ||chr(10)||c_addr_rec.address_line_3, chr(10))
                ||chr(10)||c_addr_rec.city||', '||c_addr_rec.state||' '||c_addr_rec.postal_code
                );
      dbms_output.put_line('*** VERIFIED Address: '
                ||chr(10)||l_address_rec.address_line_1
                ||ltrim(chr(10)||l_address_rec.address_line_2
                ||chr(10)||l_address_rec.address_line_3, chr(10))
                ||chr(10)||l_address_rec.city||', '||l_address_rec.state||' '||l_address_rec.postal_code
                );
      -- Increment error count
      li_error_count := li_error_count + 1;
      -- Leave if we get more than 50 errors
      if li_error_count > 50 then
        EXIT;
      end if;
  END;
  END LOOP;
  CLOSE c_addr;
  commit;
  --
  -- Get the end time
  --
  li_end := dbms_utility.get_time;
  --
  -- Display the Output
  --
  dbms_output.put_line(chr(10));
  -- Show Update Flag
  if fb_update then
    commit;
    dbms_output.put_line('Record Update : ON');
  else
    dbms_output.put_line('Record Update : OFF');
  end if;
  --
  -- Show Statistics
  --
  dbms_output.put_line('# Records  : '||to_char(li_count));
  dbms_output.put_line('# Errors   : '||to_char(li_error_count));
  dbms_output.put_line('Total Time : '||to_char((li_end - li_start)/100)||' sec');
  if (li_end - li_start) != 0 then
    dbms_output.put_line('# Rec/Hour : '||to_char(round(360000*li_count/(li_end - li_start))));
  end if;
  --
  -- Call the Post Batch Address Verify User Exit
  --
  CLEAN_Address_Banner_UE.Post_Batch_Address_Verify;
  --
  -- Show the error summary
  --
  Show_Results;
  -- Delete the results table to clean up
  Delete_Results;
END Batch_Verify_SRTPCOL;
/******************************************************************************************
 *  Procedure Name  :   Batch_Verify_SPREMRG
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the Banner Emergency Contact Table (SPREMRG)
 *                      * fn_max_verify
 *                           - maximum number of address to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                        NOTE: This option only works if you have made the CLEAN_Address
 *                              mods to this table for missing address columns
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fv_address_type
 *                           - Banner Address Type (SPREMRG_ATYP_CODE) - only verify this type
 *                           - NULL = to verify all address types
 *                      * fn_days_back
 *                           - Verify Addresses that were updated/created this many days back
 *                             - Note: You can enter fractional days such as 0.25, 0.5, etc
 *                           - NULL = all addresses
 *                      * fb_skip_international
 *                           - Skip international address verification in batch mode
 *                      * fd_from_date / fd_to_date
 *                           - Specify a date range for the verification based on SPREMRG_ACTIVITY_DATE
 *                           - This is useful for segmenting the data to create parallel batch procedures,
 *                             increasing throughput
 ******************************************************************************************/
PROCEDURE Batch_Verify_SPREMRG (
                     fn_max_verify                  IN     NUMBER   DEFAULT 1000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fv_address_type                IN     VARCHAR2 DEFAULT NULL /* SPREMRG_ATYP_CODE */
                    ,fn_days_back                   IN     NUMBER   DEFAULT NULL
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    ,fd_from_date                   IN     DATE     DEFAULT NULL
                    ,fd_to_date                     IN     DATE     DEFAULT NULL
                    )
IS
  ln_only_unverified    NUMBER := 0;
  ln_skip_international NUMBER := 0;
  --
  -- Define the cursor to get the addresses from Banner
  --
  cursor c_addr IS
    select rowid
          ,SPREMRG_PIDM
          ,SPREMRG_ATYP_CODE
          ,SPREMRG_STREET_LINE1 address_line_1
          ,SPREMRG_STREET_LINE2 address_line_2
          ,SPREMRG_STREET_LINE3 address_line_3
          /*PREBANNER82*/,SPREMRG_STREET_LINE4  address_line_4
          /*PREBANNER82*/,SPREMRG_HOUSE_NUMBER  house_number
          ,SPREMRG_CITY         city
          ,SPREMRG_STAT_CODE    state
          ,SPREMRG_ZIP          postal_code
          ,SPREMRG_NATN_CODE    country_code
          --CLNMOD--,SPREMRG_CNTY_CODE    county_code
    from   SPREMRG a
    -- Only Local Country addresses if fb_Skip_International is set TRUE
    where  (  NVL(a.SPREMRG_NATN_CODE,NVL(gv_local_country,'x')) = NVL(gv_local_country,'x')
           or ln_skip_international = 0
           )
    and    (  fv_address_type IS NULL
           or SPREMRG_ATYP_CODE = fv_address_type
           )
    -- 2/23/08 - don't overwrite addresses where user checked "Override Address Verification"
    --CLNMOD--and    NVL(a.SPREMRG_REVIEWED_USER,'x') NOT LIKE '%:!'
    -- Only get addresses not verified if specified
    --CLNMOD--and    (ln_only_unverified = 0 or ltrim(SPREMRG_REVIEWED_USER) IS NULL)
    and    a.SPREMRG_ACTIVITY_DATE between NVL(trunc(fd_from_date),a.SPREMRG_ACTIVITY_DATE)
                                       and NVL(trunc(fd_to_date)+0.99999,a.SPREMRG_ACTIVITY_DATE)
    and    (  fn_days_back IS NULL
           or SPREMRG_ACTIVITY_DATE > sysdate - fn_days_back
           );
  c_addr_rec c_addr%ROWTYPE;
  -- CLEAN_Address record structures
  l_address_rec       CLEAN_Address.Address_rec;
  l_address_rec_NULL  CLEAN_Address.Address_rec;
  -- Internal Statistic Variables
  li_start            NUMBER;
  li_end              NUMBER;
  li_count            NUMBER := 0;
  li_error_count      NUMBER := 0;
  lv_plsql_error      VARCHAR2(2000);
BEGIN
  dbms_output.enable(1000000);
  --
  -- Call the Pre Batch Address Verify User Exit
  --
  CLEAN_Address_Banner_UE.Pre_Batch_Address_Verify;
  -- Clear the results table
  Delete_Results;
  -- Set the only unverified flag
  if fb_only_unverified then
    ln_only_unverified := 1;
  end if;
  -- Set the Skip International flag
  if fb_skip_international then
    ln_skip_international := 1;
    Set_Skip_International(TRUE);
  else
    Set_Skip_International(FALSE);
  end if;
  --
  -- Start the timer
  --
  li_start := dbms_utility.get_time;
  --
  -- Loop through all addresses from the cursor
  --
  OPEN c_addr;
  LOOP
  BEGIN
    FETCH c_addr
    INTO  c_addr_rec;
    EXIT WHEN c_addr%NOTFOUND;
    --
    -- Initialize address record to NULL
    --
    l_address_rec := l_address_rec_NULL;
    --
    -- Assign the input variables to the address record
    --
    l_address_rec.Address_Line_1 := c_addr_rec.address_line_1;
    l_address_rec.Address_Line_2 := c_addr_rec.address_line_2;
    l_address_rec.Address_Line_3 := c_addr_rec.address_line_3;
    /*PREBANNER82*/l_address_rec.Address_Line_4 := c_addr_rec.address_line_4;
    /*PREBANNER82*/l_address_rec.Parsed_Address_Number := c_addr_rec.house_number;
    l_address_rec.City           := c_addr_rec.City;
    l_address_rec.State          := c_addr_rec.State;
    l_address_rec.Postal_code    := c_addr_rec.postal_code;
    --CLNMOD--l_address_rec.county_code    := c_addr_rec.county_code;
    l_address_rec.country_code   := c_addr_rec.country_code;
    --
    -- Verify the address
    --
    Verify_Address_Record(
                  f_address_rec   => l_address_rec
                 ,fv_plsql_error  => lv_plsql_error
                 ,fv_address_type => c_addr_rec.SPREMRG_ATYP_CODE
                 ,fv_object_name  => 'Batch_Verify_SPREMRG'
                 );
    -- Make sure there wasn't an Oracle Pipe error
    if lv_plsql_error IS NOT NULL then
      raise NO_DATA_FOUND;
    end if;
    --
    -- Add the error statistic to the output table
    --
    Add_Result(l_address_rec.error_code, l_address_rec.error_string);
    --
    -- See if we need to update the record
    --
    if fb_update then
      --
      -- Update the existing SPREMRG Banner Address record
      --
      update SPREMRG set
          SPREMRG_STREET_LINE1     = l_address_rec.address_line_1
         ,SPREMRG_STREET_LINE2     = l_address_rec.address_line_2
         ,SPREMRG_STREET_LINE3     = l_address_rec.address_line_3
         /*PREBANNER82*/,SPREMRG_STREET_LINE4     = l_address_rec.address_line_4
         /*PREBANNER82*/,SPREMRG_HOUSE_NUMBER     = l_address_rec.parsed_address_number
         ,SPREMRG_CITY             = l_address_rec.city
         ,SPREMRG_STAT_CODE        = NVL(l_address_rec.state, l_address_rec.province)
         ,SPREMRG_ZIP              = l_address_rec.postal_code
         --CLNMOD--,SPREMRG_CNTY_CODE        = l_address_rec.county_code
         --CLNMOD--,SPREMRG_DELIVERY_POINT   = l_address_rec.delivery_point
         --CLNMOD--,SPREMRG_CORRECTION_DIGIT = l_address_rec.check_digit
         --CLNMOD--,SPREMRG_CARRIER_ROUTE    = l_address_rec.carrier_route
         --CLNMOD--,SPREMRG_REVIEWED_IND     = l_address_rec.address_line_7
         --CLNMOD--,SPREMRG_REVIEWED_USER    = l_address_rec.address_line_8
      where rowid = c_addr_rec.rowid;
      --
      -- Commit every 50 records
      --
      if mod(li_count+1, 50) = 0 then
        commit;
      end if;
    end if;
    --
    -- Increment the counter
    --
    li_count := li_count + 1;
    --
    -- Leave if we've reached the max counter
    --
    if li_count >= NVL(fn_max_verify, li_count+1) then
      EXIT;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      lv_plsql_error := NVL(lv_plsql_error, SQLERRM);
      --
      -- fv_plsql_error was found - Oracle Pipe error
      --
      -- Show the error message and input/verified address
      dbms_output.put_line(chr(10)||'Unable to Verify Address:'
                ||chr(10)||lv_plsql_error);
      dbms_output.put_line('*** Primary Key: '
                         ||to_char(c_addr_rec.SPREMRG_PIDM)
                    ||'-'||c_addr_rec.SPREMRG_ATYP_CODE
                    );
      dbms_output.put_line('*** INPUT Address: '
                ||chr(10)||c_addr_rec.address_line_1
                ||ltrim(chr(10)||c_addr_rec.address_line_2
                ||chr(10)||c_addr_rec.address_line_3, chr(10))
                ||chr(10)||c_addr_rec.city||', '||c_addr_rec.state||' '||c_addr_rec.postal_code
                );
      dbms_output.put_line('*** VERIFIED Address: '
                ||chr(10)||l_address_rec.address_line_1
                ||ltrim(chr(10)||l_address_rec.address_line_2
                ||chr(10)||l_address_rec.address_line_3, chr(10))
                ||chr(10)||l_address_rec.city||', '||l_address_rec.state||' '||l_address_rec.postal_code
                );
      -- Increment error count
      li_error_count := li_error_count + 1;
      -- Leave if we get more than 50 errors
      if li_error_count > 50 then
        EXIT;
      end if;
  END;
  END LOOP;
  CLOSE c_addr;
  commit;
  --
  -- Get the end time
  --
  li_end := dbms_utility.get_time;
  --
  -- Display the Output
  --
  dbms_output.put_line(chr(10));
  -- Show Update Flag
  if fb_update then
    commit;
    dbms_output.put_line('Record Update : ON');
  else
    dbms_output.put_line('Record Update : OFF');
  end if;
  --
  -- Show Statistics
  --
  dbms_output.put_line('# Records  : '||to_char(li_count));
  dbms_output.put_line('# Errors   : '||to_char(li_error_count));
  dbms_output.put_line('Total Time : '||to_char((li_end - li_start)/100)||' sec');
  if (li_end - li_start) != 0 then
    dbms_output.put_line('# Rec/Hour : '||to_char(round(360000*li_count/(li_end - li_start))));
  end if;
  --
  -- Call the Post Batch Address Verify User Exit
  --
  CLEAN_Address_Banner_UE.Post_Batch_Address_Verify;
  --
  -- Show the error summary
  --
  Show_Results;
  -- Delete the results table to clean up
  Delete_Results;
END Batch_Verify_SPREMRG;
/******************************************************************************************
 *  Procedure Name  :   Batch_Verify_SHTTRAN
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the Banner Transcript Request Table (SHTTRAN)
 *                      * fn_max_verify
 *                           - maximum number of address to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                        NOTE: This option only works if you have made the CLEAN_Address
 *                              mods to this table for missing address columns
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fn_days_back
 *                           - Verify Addresses that were updated/created this many days back
 *                             - Note: You can enter fractional days such as 0.25, 0.5, etc
 *                           - NULL = all addresses
 *                      * fb_skip_international
 *                           - Skip international address verification in batch mode
 *                      * fd_from_date / fd_to_date
 *                           - Specify a date range for the verification based on SHTTRAN_ACTIVITY_DATE
 *                           - This is useful for segmenting the data to create parallel batch procedures,
 *                             increasing throughput
 ******************************************************************************************/
PROCEDURE Batch_Verify_SHTTRAN (
                     fn_max_verify                  IN     NUMBER   DEFAULT 1000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fn_days_back                   IN     NUMBER   DEFAULT NULL
                    ,fb_skip_international          IN     BOOLEAN  DEFAULT TRUE
                    ,fd_from_date                   IN     DATE     DEFAULT NULL
                    ,fd_to_date                     IN     DATE     DEFAULT NULL
                    )
IS
  ln_only_unverified    NUMBER := 0;
  ln_skip_international NUMBER := 0;
  --
  -- Define the cursor to get the addresses from Banner
  --
  cursor c_addr IS
    select rowid
          ,SHTTRAN_PIDM
          ,SHTTRAN_STREET1 address_line_1
          ,SHTTRAN_STREET2 address_line_2
          ,SHTTRAN_STREET3 address_line_3
          /*PREBANNER82*/,SHTTRAN_STREET_LINE4  address_line_4
          -- NO HOUSE NUMBER FIELD YET /*PREBANNER82*/,SHTTRAN_HOUSE_NUMBER  house_number
          ,SHTTRAN_CITY         city
          ,SHTTRAN_STAT_CODE    state
          ,SHTTRAN_ZIP          postal_code
          --CLNMOD--,SHTTRAN_CNTY_CODE    county_code
          ,SHTTRAN_NATN_CODE    country_code
    from   SHTTRAN a
    -- Only Local Country addresses if fb_Skip_International is set TRUE
    where  (  NVL(a.SHTTRAN_NATN_CODE,NVL(gv_local_country,'x')) = NVL(gv_local_country,'x')
           or ln_skip_international = 0
           )
    and    (  fn_days_back IS NULL
           or SHTTRAN_ACTIVITY_DATE > sysdate - fn_days_back
           )
    and    a.SHTTRAN_ACTIVITY_DATE between NVL(trunc(fd_from_date),a.SHTTRAN_ACTIVITY_DATE)
                                       and NVL(trunc(fd_to_date)+0.99999,a.SHTTRAN_ACTIVITY_DATE)
    -- 2/23/08 - don't overwrite addresses where user checked "Override Address Verification"
    --CLNMOD--and    NVL(a.SHTTRAN_REVIEWED_USER,'x') NOT LIKE '%:!'
    -- Only get addresses not verified if specified
    --CLNMOD--and    (ln_only_unverified = 0 or ltrim(SHTTRAN_REVIEWED_USER) IS NULL)
           ;
  c_addr_rec c_addr%ROWTYPE;
  -- CLEAN_Address record structures
  l_address_rec       CLEAN_Address.Address_rec;
  l_address_rec_NULL  CLEAN_Address.Address_rec;
  -- Internal Statistic Variables
  li_start            NUMBER;
  li_end              NUMBER;
  li_count            NUMBER := 0;
  li_error_count      NUMBER := 0;
  lv_plsql_error      VARCHAR2(2000);
BEGIN
  dbms_output.enable(1000000);
  --
  -- Call the Pre Batch Address Verify User Exit
  --
  CLEAN_Address_Banner_UE.Pre_Batch_Address_Verify;
  -- Clear the results table
  Delete_Results;
  -- Set the only unverified flag
  if fb_only_unverified then
    ln_only_unverified := 1;
  end if;
  -- Set the Skip International flag
  if fb_skip_international then
    ln_skip_international := 1;
    Set_Skip_International(TRUE);
  else
    Set_Skip_International(FALSE);
  end if;
  --
  -- Start the timer
  --
  li_start := dbms_utility.get_time;
  --
  -- Loop through all addresses from the cursor
  --
  OPEN c_addr;
  LOOP
  BEGIN
    FETCH c_addr
    INTO  c_addr_rec;
    EXIT WHEN c_addr%NOTFOUND;
    --
    -- Initialize address record to NULL
    --
    l_address_rec := l_address_rec_NULL;
    --
    -- Assign the input variables to the address record
    --
    l_address_rec.Address_Line_1 := c_addr_rec.address_line_1;
    l_address_rec.Address_Line_2 := c_addr_rec.address_line_2;
    l_address_rec.Address_Line_3 := c_addr_rec.address_line_3;
    /*PREBANNER82*/l_address_rec.Address_Line_4 := c_addr_rec.address_line_4;
    -- NO HOUSE NUMBER FIELD YET /*PREBANNER82*/l_address_rec.Parsed_Address_Number := c_addr_rec.house_number;
    l_address_rec.City           := c_addr_rec.City;
    l_address_rec.State          := c_addr_rec.State;
    l_address_rec.Postal_code    := c_addr_rec.postal_code;
    --CLNMOD--l_address_rec.county_code    := c_addr_rec.county_code;
    l_address_rec.country_code   := c_addr_rec.country_code;
    --
    -- Verify the address
    --
    Verify_Address_Record(
                  f_address_rec   => l_address_rec
                 ,fv_plsql_error  => lv_plsql_error
                 ,fv_address_type => 'SHTTRAN'
                 ,fv_object_name  => 'Batch_Verify_SHTTRAN'
                 );
    -- Make sure there wasn't an Oracle Pipe error
    if lv_plsql_error IS NOT NULL then
      raise NO_DATA_FOUND;
    end if;
    --
    -- Add the error statistic to the output table
    --
    Add_Result(l_address_rec.error_code, l_address_rec.error_string);
    --
    -- See if we need to update the record
    --
    if fb_update then
      --
      -- Update the existing SHTTRAN Banner Address record
      --
      update SHTTRAN set
          SHTTRAN_STREET1     = l_address_rec.address_line_1
         ,SHTTRAN_STREET2     = l_address_rec.address_line_2
         ,SHTTRAN_STREET3     = l_address_rec.address_line_3
         /*PREBANNER82*/,SHTTRAN_STREET_LINE4     = l_address_rec.address_line_4
         -- NO HOUSE NUMBER FIELD YET /*PREBANNER82*/,SHTTRAN_HOUSE_NUMBER     = l_address_rec.parsed_address_number
         ,SHTTRAN_CITY             = l_address_rec.city
         ,SHTTRAN_STAT_CODE        = NVL(l_address_rec.state, l_address_rec.province)
         ,SHTTRAN_ZIP              = l_address_rec.postal_code
         --CLNMOD--,SHTTRAN_CNTY_CODE        = l_address_rec.county_code
         --CLNMOD--,SHTTRAN_DELIVERY_POINT   = l_address_rec.delivery_point
         --CLNMOD--,SHTTRAN_CORRECTION_DIGIT = l_address_rec.check_digit
         --CLNMOD--,SHTTRAN_CARRIER_ROUTE    = l_address_rec.carrier_route
         --CLNMOD--,SHTTRAN_REVIEWED_IND     = l_address_rec.address_line_7
         --CLNMOD--,SHTTRAN_REVIEWED_USER    = l_address_rec.address_line_8
      where rowid = c_addr_rec.rowid;
      --
      -- Commit every 50 records
      --
      if mod(li_count+1, 50) = 0 then
        commit;
      end if;
    end if;
    --
    -- Increment the counter
    --
    li_count := li_count + 1;
    --
    -- Leave if we've reached the max counter
    --
    if li_count >= NVL(fn_max_verify, li_count+1) then
      EXIT;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      lv_plsql_error := NVL(lv_plsql_error, SQLERRM);
      --
      -- fv_plsql_error was found - Oracle Pipe error
      --
      -- Show the error message and input/verified address
      dbms_output.put_line(chr(10)||'Unable to Verify Address:'
                ||chr(10)||lv_plsql_error);
      dbms_output.put_line('*** Primary Key: '
                         ||to_char(c_addr_rec.SHTTRAN_PIDM)
                    );
      dbms_output.put_line('*** INPUT Address: '
                ||chr(10)||c_addr_rec.address_line_1
                ||ltrim(chr(10)||c_addr_rec.address_line_2
                ||chr(10)||c_addr_rec.address_line_3, chr(10))
                ||chr(10)||c_addr_rec.city||', '||c_addr_rec.state||' '||c_addr_rec.postal_code
                );
      dbms_output.put_line('*** VERIFIED Address: '
                ||chr(10)||l_address_rec.address_line_1
                ||ltrim(chr(10)||l_address_rec.address_line_2
                ||chr(10)||l_address_rec.address_line_3, chr(10))
                ||chr(10)||l_address_rec.city||', '||l_address_rec.state||' '||l_address_rec.postal_code
                );
      -- Increment error count
      li_error_count := li_error_count + 1;
      -- Leave if we get more than 50 errors
      if li_error_count > 50 then
        EXIT;
      end if;
  END;
  END LOOP;
  CLOSE c_addr;
  commit;
  --
  -- Get the end time
  --
  li_end := dbms_utility.get_time;
  --
  -- Display the Output
  --
  dbms_output.put_line(chr(10));
  -- Show Update Flag
  if fb_update then
    commit;
    dbms_output.put_line('Record Update : ON');
  else
    dbms_output.put_line('Record Update : OFF');
  end if;
  --
  -- Show Statistics
  --
  dbms_output.put_line('# Records  : '||to_char(li_count));
  dbms_output.put_line('# Errors   : '||to_char(li_error_count));
  dbms_output.put_line('Total Time : '||to_char((li_end - li_start)/100)||' sec');
  if (li_end - li_start) != 0 then
    dbms_output.put_line('# Rec/Hour : '||to_char(round(360000*li_count/(li_end - li_start))));
  end if;
  --
  -- Call the Post Batch Address Verify User Exit
  --
  CLEAN_Address_Banner_UE.Post_Batch_Address_Verify;
  --
  -- Show the error summary
  --
  Show_Results;
  -- Delete the results table to clean up
  Delete_Results;
END Batch_Verify_SHTTRAN;
/******************************************************************************************
 *  Function Name   :   Get_Error_Code
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Return the error code from the SPRADDR_REVIEWED_USER field
 *                      NOTE: The SPRADDR_REVIEWED_USER holds the date and address error
 *                            in the format YYYYMMDD:<error_code>
 ******************************************************************************************/
FUNCTION Get_Error_Code (
                     address_date_and_error_code    IN     VARCHAR2 /* SPRADDR_REVIEWED_USER */
                    ) RETURN VARCHAR2
IS
BEGIN
  return substr(address_date_and_error_code, instr(address_date_and_error_code,':')+1);
END Get_Error_Code;
/******************************************************************************************
 *  Function Name   :   Get_Error_Text
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Return the error text from the code in the SPRADDR_REVIEWED_USER field
 *                      NOTE: The SPRADDR_REVIEWED_USER holds the date and address error
 *                            in the format YYYYMMDD:<error_code>
 ******************************************************************************************/
FUNCTION Get_Error_Text (
                     address_date_and_error_code    IN     VARCHAR2 /* SPRADDR_REVIEWED_USER */
                    ) RETURN VARCHAR2
IS
  lv_error_code   VARCHAR2(30);
BEGIN
  lv_error_code := Get_Error_Code(address_date_and_error_code);
  if lv_error_code IS NULL then
    return NULL;
  end if;
  if lv_error_code like 'AD%' then
    -- Get international address error
    return cln$lookup.get_address_status(lv_error_code);
  end if;
  if lv_error_code = '!' then
    return NVL(cln$lookup.get_address_error(lv_error_code), 'Address Override by End User');
  end if;
  if lv_error_code = 'SKIP' then
    return NVL(cln$lookup.get_address_error(lv_error_code), 'Address Skipped by User Exit');
  end if;
  return cln$lookup.get_address_error(lv_error_code);
END Get_Error_Text;
/******************************************************************************************
 *  Function Name   :   Get_Error_Help
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Return the error help text from the code in the SPRADDR_REVIEWED_USER field
 *                      NOTE: The SPRADDR_REVIEWED_USER holds the date and address error
 *                            in the format YYYYMMDD:<error_code>
 *                      NOTE: You can update the help text in the view CLN_Address_Errors_V
 *                            by updating the HELP_TEXT field, or you can override it by
 *                            using the COMMENT_TEXT field
 ******************************************************************************************/
FUNCTION Get_Error_Help (
                     address_date_and_error_code    IN     VARCHAR2 /* SPRADDR_REVIEWED_USER */
                    ) RETURN VARCHAR2
IS
  lv_error_code   VARCHAR2(30);
BEGIN
  lv_error_code := Get_Error_Code(address_date_and_error_code);
  if lv_error_code IS NULL then
    return NULL;
  end if;
  return NVL(cln$lookup.Comment_Text(lv_error_code, 'ADDRERROR')
            ,cln$lookup.Help_Text(lv_error_code, 'ADDRERROR')
            );
END Get_Error_Help;
/******************************************************************************************
 *  Function Name   :   Get_Verified_Date
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Return the Address Verified Date from the SPRADDR_REVIEWED_USER field
 *                      NOTE: The SPRADDR_REVIEWED_USER holds the date and address error
 *                            in the format YYYYMMDD:<error_code>
 ******************************************************************************************/
FUNCTION Get_Verified_Date (
                     address_date_and_error_code    IN     VARCHAR2 /* SPRADDR_REVIEWED_USER */
                    ) RETURN DATE
IS
BEGIN
  return to_date(substr(address_date_and_error_code, 1, instr(address_date_and_error_code, ':')-1)
                ,'YYYYMMDD');
EXCEPTION
  WHEN OTHERS THEN
    RETURN NULL;
END Get_Verified_Date;
/******************************************************************************************
 *  Procedure Name  :   Check_Telephone
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Verify a single telephone number for Banner
 *
 *                      This call wraps the Telephone record implementation and excepts pure
 *                      VARCHAR2 IN/OUT parameters.
 *
 *                      telephone_results - <suggested_area>:<distance>:<error_code>
 *                         - Telephone suggested Area, Distance, and error code, separated by :
 ******************************************************************************************/
PROCEDURE Check_Telephone (
                     phone_area                     IN OUT VARCHAR2 /* SPRTELE_PHONE_AREA */
                    ,phone_number                   IN OUT VARCHAR2 /* SPRTELE_PHONE_NUMBER */
                    ,phone_extension                IN OUT VARCHAR2 /* SPRTELE_PHONE_EXT */
                    ,postal_code                    IN     VARCHAR2 /* SPRADDR_ZIP */
                    ,telephone_results              IN OUT VARCHAR2 /* SPRTELE_DATA_ORIGIN */
                    ,suggested_area                    OUT VARCHAR2 /* Possible new area code */
                    ,distance                          OUT NUMBER   /* Distance from Phone to ZIP Code */
                    ,telephone_error_text           IN OUT VARCHAR2
                    ,fv_plsql_error                 IN OUT VARCHAR2
                    ,fv_telephone_code              IN     VARCHAR2 DEFAULT NULL /* SPRTELE_TELE_CODE */
                    ,fv_object_name                 IN     VARCHAR2 DEFAULT NULL /* Object Name where procedure is called from */
                    )
IS
  -- CLEAN_Address record structure
  l_telephone_rec       CLEAN_Address.Phone_rec;
  lv_area_code          VARCHAR2(10);
BEGIN
  -- Leave if the postal_code is blank - can't do anything
  if postal_code IS NULL
     or phone_number IS NULL then
    return;
  end if;
  --
  -- Backup existing area code
  --
  lv_area_code := phone_area;
  --
  -- 5/16/07 - Check for last 4 of phone number in extension field
  --
  if length(phone_number) = 3
     and length(phone_extension) = 4 then
    phone_number := phone_number||phone_extension;
    phone_extension := NULL;
  end if;
  --
  -- 8/21/09 - Check for phone number split between area code and phone number field
  --
  if length(phone_area) = 3
     and length(phone_number) = 4
     and phone_extension IS NULL then
    phone_number := phone_area||phone_number;
    phone_area := NULL;
    lv_area_code := NULL;
  end if;
  --
  -- Assign the input variables to the telephone record
  --
  l_telephone_rec.Phone_Number := phone_area||phone_number;
  l_telephone_rec.Postal_Code  := postal_code;
  l_telephone_rec.Phone_Extension := phone_extension;
  l_telephone_rec.Phone_Number_Output_Format := '9999999999';
  telephone_error_text         := NULL;
  --
  -- Check the Telephone
  --
  Check_Telephone_Record(
                f_telephone_rec   => l_telephone_rec
               ,fv_phone_area     => phone_area
               ,fv_plsql_error    => fv_plsql_error
               ,fv_telephone_code => fv_telephone_code
               ,fv_object_name    => fv_object_name
               );
  -- Get the suggested area code and distance
  suggested_area := l_telephone_rec.New_Phone_Area_Code;
  distance       := l_telephone_rec.Phone_Distance;
  -- Assign the area code back only if this was an area code SPLIT
  phone_area     := NVL(l_telephone_rec.Phone_Area_Code, phone_area);
  telephone_results    := l_telephone_rec.Country_Name;  /* Use Country_Name as a buffer */
  if l_telephone_rec.Phone_Check_Error_Code IS NOT NULL then
    telephone_error_text := cln$lookup.Get_Phone_Error(l_telephone_rec.Phone_Check_Error_Code);
  end if;
  --
  -- Assign the friendly error message based on results
  --
  if telephone_error_text IS NULL then
    --
    -- Check for Phone area code split
    --
    if phone_area != NVL(lv_area_code, phone_area) then
      telephone_error_text := 'Area Code updated due to area code split:'
               ||chr(10)||'OLD: '||lv_area_code
               ||chr(10)||'NEW: '||phone_area;
    --
    -- Check for a valid distance from the area code to the ZIP Code
    --
    elsif NVL(distance, -1) > NVL(gn_Telephone_Distance_Limit, 0)
          and NVL(distance, -1) != 9999 then
      telephone_error_text := 'WARNING: The Distance from the Area Code to the ZIP Code ('
               ||to_char(distance)
               ||' miles) exceeds the threshhold of '||to_char(gn_Telephone_Distance_Limit)||' miles!';
      -- conditionally show the Suggested Area Code
      if suggested_area IS NOT NULL then
        telephone_error_text := telephone_error_text||chr(10)||'Suggested Area Code: '||suggested_area;
      end if;
    --
    -- Show any "suggested" area codes if the input area code is not valid
    --
    elsif suggested_area IS NOT NULL
          and suggested_area != NVL(phone_area,'x') then
      telephone_error_text := 'WARNING: A corrected Area Code is suggested based on the zip code entered.'
               ||chr(10)||'Suggested Area Code: '||suggested_area;
    end if;
  end if;
END Check_Telephone;
/******************************************************************************************
 *  Procedure Name  :   Check_SPRTELE_Record
 *
 *  Scope           :   PRIVATE
 *
 *  Description     :   Verify a single telephone by passing in the SPRTELE Record type
 ******************************************************************************************/
PROCEDURE Check_SPRTELE_Record (
                     f_SPRTELE_rec                  IN OUT SPRTELE%ROWTYPE
                    ,suggested_area                    OUT VARCHAR2 /* Possible new area code */
                    ,distance                          OUT NUMBER   /* Distance from Phone to ZIP Code */
                    ,telephone_error_text           IN OUT VARCHAR2
                    ,fv_plsql_error                 IN OUT VARCHAR2
                    ,fv_telephone_code              IN     VARCHAR2 DEFAULT NULL /* SPRTELE_TELE_CODE */
                    ,fv_object_name                 IN     VARCHAR2 DEFAULT NULL /* Object Name where procedure is called from */
                    )
IS
  -- Cursor to get the ZIP code from the address
  cursor c_addr IS
    select SPRADDR_ZIP
    from   SPRADDR
    where  SPRADDR_PIDM       = f_SPRTELE_rec.SPRTELE_PIDM
    and    SPRADDR_ATYP_CODE  = f_SPRTELE_rec.SPRTELE_ATYP_CODE
    and    SPRADDR_SEQNO      = f_SPRTELE_rec.SPRTELE_ADDR_SEQNO
    and    NVL(ltrim(upper(SPRADDR_STATUS_IND)),'A') = 'A';
  c_addr_rec c_addr%ROWTYPE;
BEGIN
  --
  -- Get the ZIP from the Address (if available)
  --
  if     f_SPRTELE_rec.SPRTELE_PIDM IS NOT NULL
     and f_SPRTELE_rec.SPRTELE_ATYP_CODE IS NOT NULL
     and f_SPRTELE_rec.SPRTELE_ADDR_SEQNO IS NOT NULL
     then
    open c_addr;
    fetch c_addr
    into c_addr_rec;
    if c_addr%NOTFOUND then
      c_addr_rec.SPRADDR_ZIP := NULL;
    end if;
    close c_addr;
  end if;
  --
  -- Call the CLEAN_Address Banner Verify Telephone procedure with individual parameters
  --
  CLEAN_Address_Banner.Check_Telephone (
           phone_area           => f_SPRTELE_rec.SPRTELE_PHONE_AREA
          ,phone_number         => f_SPRTELE_rec.SPRTELE_PHONE_NUMBER
          ,phone_extension      => f_SPRTELE_rec.SPRTELE_PHONE_EXT
          ,postal_code          => c_addr_rec.SPRADDR_ZIP
          ,telephone_results    => f_SPRTELE_rec.SPRTELE_DATA_ORIGIN
          ,suggested_area       => suggested_area
          ,distance             => distance
          ,telephone_error_text => telephone_error_text
          ,fv_plsql_error       => fv_plsql_error
          ,fv_telephone_code    => fv_telephone_code
          ,fv_object_name       => fv_object_name
          );
END Check_SPRTELE_Record;
/******************************************************************************************
 *  Procedure Name  :   Batch_Check_Telephone
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Batch Verify the telephones in the Banner Telephone table (SPRTELE)
 *                      * fn_max_verify
 *                           - maximum number of telephones to verify
 *                      * fb_update
 *                           - TRUE  = update the record and show results
 *                           - FALSE = don't update, only show results
 *                      * fb_only_unverified
 *                           - TRUE  = Only update unverified records
 *                           - FALSE = Update ALL records
 *                      * fv_telephone_code
 *                           - Banner Telephone Code - only verify this type (SPRTELE_TELE_CODE)
 *                           - NULL = to verify all telephone codes
 *                      * fn_days_back
 *                           - Verify Telephone numbers that were updated/created this many days back
 *                             - Note: You can enter fractional days such as 0.25, 0.5, etc
 *                           - NULL = all telephones
 ******************************************************************************************/
PROCEDURE Batch_Check_Telephone (
                     fn_max_verify                  IN     NUMBER   DEFAULT 1000000
                    ,fb_update                      IN     BOOLEAN  DEFAULT TRUE
                    ,fb_only_unverified             IN     BOOLEAN  DEFAULT TRUE
                    ,fv_telephone_code              IN     VARCHAR2 DEFAULT NULL /* SPRTELE_TELE_CODE */
                    ,fn_days_back                   IN     NUMBER   DEFAULT NULL
                    )
IS
  ln_only_unverified   NUMBER := 0;
  --
  -- Define the cursor to get the addresses from Banner
  --
  cursor c_tele IS
    select t.rowid
          ,t.SPRTELE_PIDM
          ,t.SPRTELE_TELE_CODE
          ,t.SPRTELE_SEQNO
          ,t.SPRTELE_PHONE_AREA   phone_area
          ,t.SPRTELE_PHONE_NUMBER phone_number
          ,t.SPRTELE_PHONE_EXT    phone_ext
          ,a.SPRADDR_ZIP          postal_code
          ,a.SPRADDR_CNTY_CODE    county_code
          ,a.SPRADDR_NATN_CODE    country_code
    from   SPRADDR a
          ,SPRTELE t
    where  t.SPRTELE_PIDM       = a.SPRADDR_PIDM
    and    t.SPRTELE_ATYP_CODE  = a.SPRADDR_ATYP_CODE
    and    t.SPRTELE_ADDR_SEQNO = a.SPRADDR_SEQNO
    and    NVL(ltrim(upper(SPRTELE_STATUS_IND)),'A') = 'A'
    -- Make sure ZIP Code exists for this telephone number
    and    a.SPRADDR_ZIP IS NOT NULL
    and    t.SPRTELE_PHONE_AREA||t.SPRTELE_PHONE_NUMBER IS NOT NULL
    -- Only US / Canada addresses
    --and    NVL(ltrim(upper(a.SPRADDR_NATN_CODE)),'US') in ('US', 'USA', 'U.S.', 'U.S.A.', 'CA', 'CAN','840','157')
    -- Only get addresses not verified if specified
    and    (  ln_only_unverified = 0
           or ltrim(t.SPRTELE_DATA_ORIGIN) IS NULL
           )
    and    (  fv_telephone_code IS NULL
           or t.SPRTELE_TELE_CODE = fv_telephone_code
           )
    and    (  fn_days_back IS NULL
           or t.SPRTELE_ACTIVITY_DATE > sysdate - fn_days_back
           );
  c_tele_rec c_tele%ROWTYPE;
  -- CLEAN_Address record structures
  l_telephone_rec       CLEAN_Address.Phone_rec;
  l_telephone_rec_NULL  CLEAN_Address.Phone_rec;
  -- Internal Statistic Variables
  li_start            NUMBER;
  li_end              NUMBER;
  li_count            NUMBER := 0;
  li_error_count      NUMBER := 0;
  lv_plsql_error      VARCHAR2(2000);
BEGIN
  dbms_output.enable(1000000);
  --
  -- Call the Pre Batch Telephone Check User Exit
  --
  CLEAN_Address_Banner_UE.Pre_Batch_Telephone_Check;
  -- Clear the results table
  Delete_Results;
  -- Set the only unverified flag
  if fb_only_unverified then
    ln_only_unverified := 1;
  end if;
  --
  -- Start the timer
  --
  li_start := dbms_utility.get_time;
  --
  -- Loop through all addresses from the cursor
  --
  OPEN c_tele;
  LOOP
  BEGIN
    FETCH c_tele
    INTO  c_tele_rec;
    EXIT WHEN c_tele%NOTFOUND;
    --
    -- Initialize address record to NULL
    --
    l_telephone_rec := l_telephone_rec_NULL;
    --
    -- 5/16/07 - Check for last 4 of phone number in extension field
    --
    if length(c_tele_rec.phone_number) = 3
       and length(c_tele_rec.phone_ext) = 4 then
      c_tele_rec.phone_number := c_tele_rec.phone_number||c_tele_rec.phone_ext;
      c_tele_rec.phone_ext := NULL;
    end if;
    --
    -- Assign the input variables to the address record
    --
    l_telephone_rec.Phone_Number := c_tele_rec.phone_area||c_tele_rec.phone_number;
    l_telephone_rec.Phone_Extension := c_tele_rec.phone_ext;
    l_telephone_rec.Postal_Code  := c_tele_rec.postal_code;
    l_telephone_rec.Phone_Number_Output_Format := '9999999999';
    --
    -- Verify the Address
    --
    Check_Telephone_Record(
                  f_telephone_rec   => l_telephone_rec
                 ,fv_phone_area     => c_tele_rec.phone_area
                 ,fv_plsql_error    => lv_plsql_error
                 ,fv_telephone_code => c_tele_rec.SPRTELE_TELE_CODE
                 ,fv_object_name    => 'Batch_Check_Telephone'
                 );
    -- Make sure there wasn't an Oracle Pipe error
    if lv_plsql_error IS NOT NULL then
      raise NO_DATA_FOUND;
    end if;
    --
    -- Add the error statistic to the output table
    --
    Add_Result(l_telephone_rec.Phone_Check_Error_Code
              ,cln$lookup.Get_Phone_Error(l_telephone_rec.Phone_Check_Error_Code)
              );
    --
    -- See if we need to update the record
    --
    if fb_update then
      --
      -- Update the Banner Address record
      --
      update SPRTELE set
          SPRTELE_PHONE_AREA   = NVL(l_telephone_rec.Phone_Area_Code, SPRTELE_PHONE_AREA)
         ,SPRTELE_PHONE_NUMBER = c_tele_rec.phone_number
         ,SPRTELE_PHONE_EXT    = c_tele_rec.phone_ext
         ,SPRTELE_DATA_ORIGIN  = l_telephone_rec.Country_Name
      where rowid = c_tele_rec.rowid;
      --
      -- Commit every 50 records
      --
      if mod(li_count+1, 50) = 0 then
        commit;
      end if;
    end if;
    --
    -- Increment the counter
    --
    li_count := li_count + 1;
    --
    -- Leave if we've reached the max counter
    --
    if li_count >= NVL(fn_max_verify, li_count+1) then
      EXIT;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      lv_plsql_error := NVL(lv_plsql_error, SQLERRM);
      --
      -- fv_plsql_error was found - Oracle Pipe error
      --
      -- Show the error message and input/verified address
      dbms_output.put_line(chr(10)||'Unable to Verify Telephone:'
                ||chr(10)||lv_plsql_error);
      dbms_output.put_line('*** Primary Key: '
                         ||to_char(c_tele_rec.sprtele_pidm)
                    ||'-'||c_tele_rec.sprtele_tele_code
                    ||'-'||to_char(c_tele_rec.sprtele_seqno)
                    );
      dbms_output.put_line('*** INPUT Telephone: '
                ||c_tele_rec.phone_area||' '||c_tele_rec.phone_number||' '||c_tele_rec.postal_code
                );
      -- Increment error count
      li_error_count := li_error_count + 1;
      -- Leave if we get more than 50 errors
      if li_error_count > 50 then
        EXIT;
      end if;
  END;
  END LOOP;
  CLOSE c_tele;
  commit;
  --
  -- Get the end time
  --
  li_end := dbms_utility.get_time;
  --
  -- Display the Output
  --
  dbms_output.put_line(chr(10));
  -- Show Update Flag
  if fb_update then
    commit;
    dbms_output.put_line('Record Update : ON');
  else
    dbms_output.put_line('Record Update : OFF');
  end if;
  --
  -- Show Statistics
  --
  dbms_output.put_line('# Records  : '||to_char(li_count));
  dbms_output.put_line('# Errors   : '||to_char(li_error_count));
  dbms_output.put_line('Total Time : '||to_char((li_end - li_start)/100)||' sec');
  if (li_end - li_start) != 0 then
    dbms_output.put_line('# Rec/Hour : '||to_char(round(360000*li_count/(li_end - li_start))));
  end if;
  --
  -- Call the Post Batch Telephone Check User Exit
  --
  CLEAN_Address_Banner_UE.Post_Batch_Telephone_Check;
  --
  -- Show the error summary
  --
  Show_Results;
  -- Delete the results table to clean up
  Delete_Results;
END Batch_Check_Telephone;
/******************************************************************************************
 *  Function Name   :   Get_Telephone_Suggested_Area
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Return the Suggested Area Code from the SPRTELE_DATA_ORIGIN field
 *                      NOTE: The SPRTELE_DATA_ORIGIN holds the telephone results in the format
 *                            <suggested_area>:<distance>:<error_code>
 ******************************************************************************************/
FUNCTION Get_Telephone_Suggested_Area (
                     telephone_results              IN     VARCHAR2 /* SPRTELE_DATA_ORIGIN */
                    ) RETURN VARCHAR2
IS
BEGIN
  return substr(telephone_results, 1, instr(telephone_results, ':')-1);
END Get_Telephone_Suggested_Area;
/******************************************************************************************
 *  Function Name   :   Get_Telephone_Distance
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Return the Distance from the SPRTELE_DATA_ORIGIN field
 *                      - This is the distance from the area code to the Zip code for the
 *                        attached address
 *                      NOTE: The SPRTELE_DATA_ORIGIN holds the telephone results in the format
 *                            <suggested_area>:<distance>:<error_code>
 ******************************************************************************************/
FUNCTION Get_Telephone_Distance (
                     telephone_results              IN     VARCHAR2 /* SPRTELE_DATA_ORIGIN */
                    ) RETURN NUMBER
IS
  ln_distance NUMBER;
BEGIN
  ln_distance := to_number(
              substr(telephone_results
                    ,instr(telephone_results,':')+1
                    ,instr(telephone_results,':',1,2) - instr(telephone_results,':') - 1
                    )
                  );
  return ln_distance;
EXCEPTION
  WHEN OTHERS THEN
    RETURN NULL;
END Get_Telephone_Distance;
/******************************************************************************************
 *  Function Name   :   Get_Telephone_Error_Code
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Return the error code from the SPRTELE_DATA_ORIGIN field
 *                      NOTE: The SPRTELE_DATA_ORIGIN holds the telephone results in the format
 *                            <suggested_area>:<distance>:<error_code>
 ******************************************************************************************/
FUNCTION Get_Telephone_Error_Code (
                     telephone_results              IN     VARCHAR2 /* SPRTELE_DATA_ORIGIN */
                    ) RETURN VARCHAR2
IS
BEGIN
  return substr(telephone_results, instr(telephone_results,':',1,2)+1);
END Get_Telephone_Error_Code;
/******************************************************************************************
 *  Function Name   :   Get_Telephone_Error_Text
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Return the error text from the code in the SPRTELE_DATA_ORIGIN field
 *                      NOTE: The SPRTELE_DATA_ORIGIN holds the telephone results in the format
 *                            <suggested_area>:<distance>:<error_code>
 ******************************************************************************************/
FUNCTION Get_Telephone_Error_Text (
                     telephone_results              IN     VARCHAR2 /* SPRTELE_DATA_ORIGIN */
                    ) RETURN VARCHAR2
IS
  lv_error_code VARCHAR2(10);
BEGIN
  lv_error_code := substr(Get_Telephone_Error_Code(telephone_results),1,10);
  if lv_error_code IS NULL then
    return NULL;
  end if;
  return cln$lookup.Get_Phone_Error(lv_error_code);
END Get_Telephone_Error_Text;
/******************************************************************************************
 *  Procedure Name  :   Sync_Postal_Codes
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Sync the Banner GTVZIPC postal codes table from the CLEAN_Address
 *                      CLN_Postal_Codes table
 *                      - Set fb_sync_county to TRUE to sync the STVCNTY table
 *                      - Set fb_sync_postal_codes to TRUE to sync the GTVZIPC table
 *                      - Set fv_state a specific state to ONLY sync county codes for that state
 *                      - Set fb_set_county_for_postal to FALSE to not populate county on GTVZIPC table
 *                      - Set fb_prefix_state_on_county_name to TRUE to prefix the state code on the
 *                        county name (I.e. FL-Orange, CA-Orange)
 *                      - Set fb_prefix_state_on_county_code to TRUE to prefix the state code on the
 *                        county code (I.e. FL099 instead of 12099), FALSE to use numeric prefix
 *                        NOTE: This only applies if the STVCNTY table has no records (new sync)
 *                      - Set fb_use_unique_postal_codes to only keep one unique postal code without multiple cities - uses preferred city name
 ******************************************************************************************/
PROCEDURE Sync_Postal_Codes (
                     fb_sync_county                 IN     BOOLEAN  DEFAULT TRUE /* STVCNTY */
                    ,fb_sync_postal_codes           IN     BOOLEAN  DEFAULT TRUE /* GTVZIPC */
                    ,fv_state                       IN     VARCHAR2 DEFAULT NULL /* Specific State for STVCNTY */
                    ,fb_set_county_for_postal       IN     BOOLEAN  DEFAULT TRUE /* Populate county on GTVZIPC */
                    ,fb_prefix_state_on_county_name IN     BOOLEAN  DEFAULT FALSE /* Prefixes State code on County Name (i.e. FL-Palm Beach) */
                    ,fb_prefix_state_on_county_code IN     BOOLEAN  DEFAULT TRUE  /* Prefixes State code on County Code (i.e. FL099 instead of 12099 */
                    ,fb_use_unique_postal_codes     IN     BOOLEAN  DEFAULT TRUE  /* Only keep one unique postal code without multiple cities - uses preferred city name */
                    )
IS
  --
  -- Cursor to get predominant County prefix before syncing
  --
  cursor c_county_prefix IS
    select substr(STVCNTY_CODE,1,2) State_Code
          ,length(STVCNTY_CODE) County_Length
          ,count(*) total
    from   STVCNTY cln
    group by substr(STVCNTY_CODE,1,2)
            ,length(STVCNTY_CODE)
    order by County_Length, total DESC;  -- Get most common hits first
  c_county_prefix_rec      c_county_prefix%ROWTYPE;
  ln_alpha_county     BINARY_INTEGER := 0;
  ln_3_digit_county   BINARY_INTEGER := 0;
  ln_prefix_state_on_county_name BINARY_INTEGER := 0;
  lb_sync_county      BOOLEAN := fb_sync_county;
  ln_use_unique_postal_codes BINARY_INTEGER := 0;
  lv_state            VARCHAR2(100);
  lv_postal_state     VARCHAR2(100);
  --
  -- Cursor to get all MISSING County records from CLN_POSTAL_CODES
  --
  cursor c_county IS
    select DISTINCT
           decode(1
                 ,ln_3_digit_county, substr(cln.county_code,3)
                 ,ln_alpha_county,   cln.state||substr(cln.county_code,3)
                 ,cln.county_code
                 )  county_code
          ,decode(ln_prefix_state_on_county_name
                 ,1, decode(state, NULL, NULL, state||'-')||decode(county_code, '00000', 'APO/FPO', county_name)
                 ,decode(county_code, '00000', 'APO/FPO', county_name)
                 ) county_name
    from   cln_postal_codes cln
    where  preferred_flag = 1  /* Only insert/update preferred postal codes, not alternates */
    and    country_code = 'US'
    and    state = NVL(lv_state, state)  /* limit specific states if specified */
    and    county_code IS NOT NULL
    and    (   county_name IS NOT NULL
            or county_code = '00000')  -- APO/FPO Exception
    and    NOT exists (
              select 1
              from   stvcnty
              where  stvcnty_code = decode(1
                                          ,ln_3_digit_county, substr(cln.county_code,3)
                                          ,ln_alpha_county,   cln.state||substr(cln.county_code,3)
                                          ,cln.county_code
                                          )
              );
  c_county_rec      c_county%ROWTYPE;
  --
  -- Cursor to get all MISSING records from CLN_POSTAL_CODES
  --
  cursor c_postal IS
    select DISTINCT
           postal_code
          ,CLEAN_Address.InitCity(
               decode(sign(length(city)-gn_Max_City_Length)
                     ,1, city_abbreviation
                     ,city)
              ,state) city
          ,state
          ,decode(cln.county_code, NULL, NULL
                 ,decode(NVL(lv_postal_state,state), state
                        ,decode(1
                               ,ln_3_digit_county, substr(cln.county_code,3)
                               ,ln_alpha_county,   cln.state||substr(cln.county_code,3)
                               ,cln.county_code
                               )
                        ,NULL)) county_code
    from   cln_postal_codes cln
    /* 4/24/09 - Choose to insert/update preferred postal codes or alternates */
    where  (   ln_use_unique_postal_codes = 0
           or (ln_use_unique_postal_codes = 1
               and preferred_flag = 1
               )
           )
    and    country_code = 'US'
    and    postal_code not like 'C%'  -- Ignore county only records
    and    city IS NOT NULL
    and    NOT exists (
              select 1
              from   gtvzipc zip
              where  zip.gtvzipc_code = cln.postal_code
              /* 4/24/09 - Choose to insert/update preferred postal codes or alternates */
              and    (    ln_use_unique_postal_codes = 1
                      or (ln_use_unique_postal_codes = 0
                          and zip.gtvzipc_city = CLEAN_Address.InitCity(decode(sign(length(city)-gn_Max_City_Length)
                                                                    ,1, city_abbreviation
                                                                    ,city), state)
                          )
                     )
              );
  c_postal_rec      c_postal%ROWTYPE;
  -- get a static sysdate for comparison in the delete routine
  ld_updated_date       DATE := trunc(sysdate, 'MI');
  ln_count              NUMBER := 0;
  ln_inserted_count     NUMBER := 0;
  ln_updated_count      NUMBER := 0;
  ln_deleted_count      NUMBER := 0;
  ln_error_count        NUMBER := 0;
BEGIN
  dbms_output.enable(1000000);
  lv_state        := upper(fv_state);
  lv_postal_state := upper(fv_state);
  -- Set postal_state to '.' to not sync county codes on GTVZIPC
  if NOT fb_set_county_for_postal then
    lv_postal_state := '.';
  end if;
  -- Set ln_prefix_state_on_county_name based on parameter
  if fb_prefix_state_on_county_name then
    ln_prefix_state_on_county_name := 1;
  end if;
  if fb_use_unique_postal_codes then
    ln_use_unique_postal_codes := 1;
  end if;
  --
  -- See if the county code prefix is an alpha state code or numeric state code
  -- - required for both County and Postal Code additions
  --
  open  c_county_prefix;
  fetch c_county_prefix
  into  c_county_prefix_rec;
  if    c_county_prefix%NOTFOUND then
    if fb_prefix_state_on_county_code then
      -- Use Alpha prefix for counties (ie FL123)
      ln_alpha_county := 1;
      dbms_output.put_line('... STVCNTY empty - Using Alpha state code as prefix on county code.');
    else
      -- Use Numeric prefix for counties (ie 12123)
      ln_alpha_county := 0;
      dbms_output.put_line('... STVCNTY empty - Using 5-digit County FIPS code for County table sync.');
    end if;
  else
    -- See if we only have 3 digit county codes
    if NVL(c_county_prefix_rec.County_Length,5) = 3 then
      -- State must be specified if the county length is 3 - don't want to override existing
      if lv_state IS NOT NULL then
        ln_3_digit_county := 1;
        dbms_output.put_line('... Using 3-digit County code for County table sync.');
      else
        -- don't sync counties if the county table has 3 character codes and no state is specified
        lb_sync_county := FALSE;
        dbms_output.put_line('... Skipping County table sync because STATE was NOT specified and 3-Digit county codes are used.');
      end if;
    else
      -- Try to convert state code to a number - if successful, use numeric prefix
      BEGIN
        if to_number(NVL(c_county_prefix_rec.state_code,'0')) IS NOT NULL then
          -- Use Numeric prefix for counties (ie 12123)
          ln_alpha_county := 0;
          dbms_output.put_line('... Using 5-digit County FIPS code for County table sync.');
        end if;
      EXCEPTION
        WHEN OTHERS THEN
          -- Use Alpha prefix for counties (ie FL123)
          ln_alpha_county := 1;
          dbms_output.put_line('... Using Alpha state code as prefix on county code.');
      END;
    end if;
  end if;
  close c_county_prefix;
  if lb_sync_county then
    ---------------------------------------------------------------------------------
    -- Sync STVCNTY County Codes
    ---------------------------------------------------------------------------------
    ln_count           := 0;
    ln_inserted_count  := 0;
    ln_updated_count   := 0;
    ln_deleted_count   := 0;
    --
    -- Add missing county records to the CLN_Postal_Codes table so procedures below work completely
    --
    CLEAN_Address.Add_Missing_US_Counties;
    --
    -- update existing county records - reset the activity date
    --
    if ln_3_digit_county = 1 then
      --
      -- Use 3-digit county code
      --
      -- Update county records using county_code index
      --
      update STVCNTY set
             stvcnty_desc = (
                  select UNIQUE
                         decode(ln_prefix_state_on_county_name
                               ,1, decode(state, NULL, NULL, state||'-')||county_name
                               ,county_name
                               ) county_name
                  from   cln_postal_codes cln
                  where  cln.county_code = CLEAN_Address.Get_County_Numeric_FIPS(lv_state||stvcnty_code)
                  and    cln.county_name IS NOT NULL
                  and    cln.COUNTRY_CODE||'' = 'US'
                  and    cln.preferred_flag = 1
                  and    rownum=1
                  )
            ,stvcnty_activity_date = ld_updated_date
      where  exists (
                  select 1
                  from   cln_postal_codes cln
                  where  cln.county_code = CLEAN_Address.Get_County_Numeric_FIPS(lv_state||stvcnty_code)
                  and    cln.county_name IS NOT NULL
                  and    cln.COUNTRY_CODE||'' = 'US'
                  and    cln.preferred_flag = 1
                  and    rownum=1
                  )
      -- don't update previously updated records for this run
      and    stvcnty_activity_date != ld_updated_date;
      -- update counter
      ln_updated_count := ln_updated_count + SQL%ROWCOUNT;
      ln_count := ln_count + ln_updated_count;
      commit;
    elsif ln_alpha_county = 1 then
      --
      -- Alpha county code
      --
      -- Update county records using county_code index
      --
      update STVCNTY set
             stvcnty_desc = (
                  select UNIQUE
                         decode(ln_prefix_state_on_county_name
                               ,1, decode(state, NULL, NULL, state||'-')||county_name
                               ,county_name
                               ) county_name
                  from   cln_postal_codes cln
                  where  cln.county_code = CLEAN_Address.Get_County_Numeric_FIPS(stvcnty_code)
                  and    cln.county_name IS NOT NULL
                  and    cln.COUNTRY_CODE||'' = 'US'
                  and    cln.preferred_flag = 1
                  and    rownum=1
                  )
            ,stvcnty_activity_date = ld_updated_date
      where  exists (
                  select 1
                  from   cln_postal_codes cln
                  where  cln.county_code = CLEAN_Address.Get_County_Numeric_FIPS(stvcnty_code)
                  and    cln.county_name IS NOT NULL
                  and    cln.COUNTRY_CODE||'' = 'US'
                  and    cln.preferred_flag = 1
                  and    rownum=1
                  )
      -- don't update previously updated records for this run
      and    stvcnty_activity_date != ld_updated_date;
      -- update counter
      ln_updated_count := ln_updated_count + SQL%ROWCOUNT;
      ln_count := ln_count + ln_updated_count;
      commit;
    else
      --
      -- Numeric county code
      --
      update STVCNTY set
             stvcnty_desc = (
                  select UNIQUE
                         decode(ln_prefix_state_on_county_name
                               ,1, decode(state, NULL, NULL, state||'-')||county_name
                               ,county_name
                               ) county_name
                  from   cln_postal_codes cln
                  where  cln.county_code = stvcnty_code
                  and    cln.county_name IS NOT NULL
                  and    cln.COUNTRY_CODE||'' = 'US'
                  and    cln.preferred_flag = 1
                  and    rownum=1
                  )
            ,stvcnty_activity_date = ld_updated_date
      where  exists (
                  select 1
                  from   cln_postal_codes cln
                  where  cln.county_code = stvcnty_code
                  and    cln.county_name IS NOT NULL
                  and    cln.COUNTRY_CODE||'' = 'US'
                  and    cln.preferred_flag = 1
                  and    rownum=1
                  )
      -- don't update previously updated records for this run
      and    stvcnty_activity_date != ld_updated_date;
      -- update counter
      ln_updated_count := ln_updated_count + SQL%ROWCOUNT;
      ln_count := ln_count + ln_updated_count;
      commit;
    end if;
    --
    -- Loop through all CLN_POSTAL_CODES for US and add unique missing counties
    --
    OPEN c_county;
    LOOP
      FETCH c_county
      INTO  c_county_rec;
      EXIT WHEN c_county%NOTFOUND;
      --
      -- Insert new county records
      --
      insert into stvcnty (
             STVCNTY_CODE
            ,STVCNTY_DESC
            ,STVCNTY_ACTIVITY_DATE
            -- NOTE: Uncomment the next line if this field is mandatory in your Banner installation - SICAS MOD
            --SICASMOD--,STVCNTY_YACN_CODE_SAPC  -- 9/14/05 SICAS specific addition - NOT NULL
        ) values (
             c_county_rec.county_code
            ,c_county_rec.county_name
            ,ld_updated_date
            -- NOTE: Uncomment the next line if this field is mandatory in your Banner installation - SICAS MOD
            --SICASMOD--,'.'                     -- 9/14/05 SICAS specific addition - NOT NULL
        );
      ln_inserted_count := ln_inserted_count + 1;
      -- increase counter
      ln_count := ln_count + 1;
      -- Commit every 100 records
      if MOD(ln_count,100) = 0 then
        commit;
      end if;
    END LOOP;
    close c_county;
    commit;
    --
    -- Delete unused county records
    --
    BEGIN
      -- Do the actual delete
      delete stvcnty
      where stvcnty_activity_date != ld_updated_date;
      -- get the delete count
      ln_deleted_count := SQL%ROWCOUNT;
      commit;
    EXCEPTION
      WHEN OTHERS THEN
        dbms_output.put_line('Error deleting older county records - probably assigned to older address records:');
        dbms_output.put_line(substr(SQLERRM,1,250));
    END;
    dbms_output.put_line('STVCNTY Processed  : '||lpad(to_char(ln_count),6));
    dbms_output.put_line('- Updated          : '||lpad(to_char(ln_updated_count),6));
    dbms_output.put_line('- Inserted         : '||lpad(to_char(ln_inserted_count),6));
    dbms_output.put_line('- Deleted          : '||lpad(to_char(ln_deleted_count),6));
  end if;
  if fb_sync_postal_codes then
    ---------------------------------------------------------------------------------
    -- Sync GTVZIPC ZIP Codes
    ---------------------------------------------------------------------------------
    ln_count           := 0;
    ln_inserted_count  := 0;
    ln_updated_count   := 0;
    ln_deleted_count   := 0;
    ln_error_count     := 0;
    --
    -- update existing postal code records - reset the activity date
    --
    update gtvzipc zip set
           (gtvzipc_city
           ,gtvzipc_cnty_code
           ) = (
                select CLEAN_Address.InitCity(
                           decode(sign(length(cln.city)-gn_Max_City_Length)
                                 ,1, cln.city_abbreviation
                                 ,cln.city)
                           ,state) city
                      ,decode(cln.county_code, NULL, NULL
                             ,decode(NVL(lv_postal_state,state), state
                                    ,decode(1
                                           ,ln_3_digit_county, substr(cln.county_code,3)
                                           ,ln_alpha_county,   cln.state||substr(cln.county_code,3)
                                           ,cln.county_code
                                           )
                                    ,NULL)) county_code
                from   cln_postal_codes cln
                where  cln.postal_code = zip.gtvzipc_code
                /* 4/24/09 - Choose to insert/update preferred postal codes or alternates */
                and    (    ln_use_unique_postal_codes = 1
                        or (ln_use_unique_postal_codes = 0
                            and zip.gtvzipc_city = CLEAN_Address.InitCity(decode(sign(length(city)-gn_Max_City_Length)
                                                                      ,1, city_abbreviation
                                                                      ,city), state)
                            )
                       )
                and    cln.COUNTRY_CODE||'' = 'US'
                /* 4/24/09 - Choose to insert/update preferred postal codes or alternates */
                and    (   ln_use_unique_postal_codes = 0
                       or (ln_use_unique_postal_codes = 1
                           and cln.preferred_flag = 1
                           )
                       )
                and    rownum=1
                )
          ,gtvzipc_activity_date = ld_updated_date
    where  exists (
                select 1
                from   cln_postal_codes cln
                where  cln.postal_code = zip.gtvzipc_code
                /* 4/24/09 - Choose to insert/update preferred postal codes or alternates */
                and    (    ln_use_unique_postal_codes = 1
                        or (ln_use_unique_postal_codes = 0
                            and zip.gtvzipc_city = CLEAN_Address.InitCity(decode(sign(length(city)-gn_Max_City_Length)
                                                                      ,1, city_abbreviation
                                                                      ,city), state)
                            )
                       )
                and    cln.COUNTRY_CODE||'' = 'US'
                /* 4/24/09 - Choose to insert/update preferred postal codes or alternates */
                and    (   ln_use_unique_postal_codes = 0
                       or (ln_use_unique_postal_codes = 1
                           and cln.preferred_flag = 1
                           )
                       )
                )
    -- don't update previously updated records for this run
    and    gtvzipc_activity_date != ld_updated_date;
    -- update counter
    ln_updated_count := ln_updated_count + SQL%ROWCOUNT;
    ln_count := ln_count + ln_updated_count;
    commit;
    --
    -- Loop through all CLN_POSTAL_CODES for US and add missing ones
    --
    OPEN c_postal;
    LOOP
      FETCH c_postal
      INTO  c_postal_rec;
      EXIT WHEN c_postal%NOTFOUND;
      --
      -- Insert new postal code records
      --
      BEGIN
        insert into gtvzipc (
               GTVZIPC_CODE
              ,GTVZIPC_CITY
              ,GTVZIPC_STAT_CODE
              ,GTVZIPC_NATN_CODE
              ,GTVZIPC_ACTIVITY_DATE
              ,GTVZIPC_USER_ID
              ,GTVZIPC_CNTY_CODE
          ) values (
               c_postal_rec.postal_code
              ,c_postal_rec.city
              ,c_postal_rec.state
              ,NULL /* NULL for US Zips */
              ,ld_updated_date
              ,USER
              ,c_postal_rec.county_code
          );
        ln_inserted_count := ln_inserted_count + 1;
        -- increase counter
        ln_count := ln_count + 1;
        -- Commit every 100 records
        if MOD(ln_count,100) = 0 then
          commit;
        end if;
      EXCEPTION
        WHEN OTHERS THEN
          ln_error_count := ln_error_count + 1;
          if ln_error_count <= 50 then
            dbms_output.put_line('Error inserting ZIP Code record: ['
                 ||c_postal_rec.postal_code||']['||c_postal_rec.city
                 ||']['||c_postal_rec.state||']['||c_postal_rec.county_code||']');
            dbms_output.put_line(substr(SQLERRM,1,250));
            -- 9/15/05 - State FKey Violation
            -- See if this is a State Foreign Key violation - provide work-a-round
            if instr(SQLERRM, 'STVSTAT') > 0 then
              dbms_output.put_line('******************************************************************************');
              dbms_output.put_line('*** Possible state foreign key violation on table STVSTAT. ***');
              dbms_output.put_line('*** Run the following Insert statement from BANINST1 to add missing states.');
              dbms_output.put_line('insert into STVSTAT (STVSTAT_CODE,STVSTAT_DESC,STVSTAT_ACTIVITY_DATE)');
              dbms_output.put_line('select code,description,sysdate from CLN_STATES_V');
              dbms_output.put_line('where code NOT IN (select stvstat_code from STVSTAT);');
              dbms_output.put_line('*** You may need to do the following grant first from the CLNADDR schema:');
              dbms_output.put_line('clnaddr:SQL> grant select on cln_states_v to baninst1;');
              dbms_output.put_line('******************************************************************************');
              -- Exit after this severe error
              RAISE;
            end if;
          else
            if ln_error_count = 51 then
              dbms_output.put_line('...Errors exceeded 50. Suppressing remaining errors...');
            end if;
          end if;
      END;
    END LOOP;
    close c_postal;
    commit;
    --
    -- Delete unused postal code records
    --
    BEGIN
      -- Do the actual delete
      delete gtvzipc zip
      where gtvzipc_activity_date != ld_updated_date
      and   NVL(gtvzipc_natn_code, 'US') in ('US', 'USA', '137','840','157');
      -- get the delete count
      ln_deleted_count := SQL%ROWCOUNT;
      commit;
    EXCEPTION
      WHEN OTHERS THEN
        dbms_output.put_line('Error deleting older ZIP Code records - probably assigned to older address records:');
        dbms_output.put_line(substr(SQLERRM,1,250));
    END;
    dbms_output.put_line('GTVZIPC Processed  : '||lpad(to_char(ln_count),6));
    dbms_output.put_line('- Updated          : '||lpad(to_char(ln_updated_count),6));
    dbms_output.put_line('- Inserted         : '||lpad(to_char(ln_inserted_count),6));
    dbms_output.put_line('- Deleted          : '||lpad(to_char(ln_deleted_count),6));
    dbms_output.put_line('- Errors           : '||lpad(to_char(ln_error_count),6));
  end if;
EXCEPTION
  WHEN OTHERS THEN
    dbms_output.put_line('GTVZIPC Processed  : '||lpad(to_char(ln_count),6));
    dbms_output.put_line('- Updated          : '||lpad(to_char(ln_updated_count),6));
    dbms_output.put_line('- Inserted         : '||lpad(to_char(ln_inserted_count),6));
    dbms_output.put_line('- Deleted          : '||lpad(to_char(ln_deleted_count),6));
    dbms_output.put_line('- Errors           : '||lpad(to_char(ln_error_count),6));
    RAISE;
END Sync_Postal_Codes;
/******************************************************************************************
 *  Procedure Name  :   Standardize_Name
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Standardize the name components and return warning message if first name appears invalid
 *
 *                      This call wraps the Name record implementation and excepts pure
 *                      VARCHAR2 IN/OUT parameters.
 ******************************************************************************************/
PROCEDURE Standardize_Name (
                     first_name                     IN OUT VARCHAR2 /* SPRIDEN_FIRST_NAME */
                    ,last_name                      IN OUT VARCHAR2 /* SPRIDEN_LAST_NAME */
                    ,middle_name                    IN OUT VARCHAR2 /* SPRIDEN_MI */
                    ,prefix                         IN OUT VARCHAR2 /* SPBPERS_NAME_PREFIX */
                    ,suffix                         IN OUT VARCHAR2 /* SPBPERS_NAME_SUFFIX */
                    ,name_error_code                IN OUT VARCHAR2
                    ,name_error_text                IN OUT VARCHAR2
                    ,fv_plsql_error                 IN OUT VARCHAR2
                    ,fv_name_type_code              IN     VARCHAR2 DEFAULT NULL /* SPRIDEN_NTYP_CODE */
                    ,fv_object_name                 IN     VARCHAR2 DEFAULT NULL /* Object Name where procedure is called from */
                    )
IS
  l_name_rec        CLEAN_Address.NAME_REC;
BEGIN
  --
  -- Assign the input variables to the name record
  --
  l_name_rec.first_name      := first_name;
  l_name_rec.last_name       := last_name;
  l_name_rec.middle_name     := middle_name;
  l_name_rec.name_prefix     := prefix;
  l_name_rec.name_suffix     := suffix;
  name_error_text            := NULL;
  --
  -- Standardize the Name
  --
  Standardize_Name_Record (
           f_name_rec          => l_name_rec
          ,fv_name_error_text  => name_error_text
          ,fv_plsql_error      => fv_plsql_error
          ,fv_name_type_code   => fv_name_type_code
          ,fv_object_name      => fv_object_name
          );
  --
  -- Remap the output name fields
  --
  first_name      := l_name_rec.first_name;
  last_name       := l_name_rec.last_name;
  middle_name     := l_name_rec.middle_name;
  prefix          := l_name_rec.name_prefix;
  suffix          := l_name_rec.name_suffix;
  name_error_code := l_name_rec.name_parse_status_code;
END Standardize_Name;
/******************************************************************************************
 *  Function Name   :   Get_Name_Error_Text
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Return the error text from the name error code
 ******************************************************************************************/
FUNCTION Get_Name_Error_Text (
                     name_error_code                IN     VARCHAR2
                    ) RETURN VARCHAR2
IS
BEGIN
  if name_error_code IS NULL then
    return NULL;
  end if;
  if name_error_code = 'U' then
    return 'Unknown First Name';
  end if;
  return cln$lookup.Get_Name_Status(name_error_code);
END Get_Name_Error_Text;
/******************************************************************************************
 *  Procedure Name  :   Validate_Email
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Validate the Email Address, checking syntax and domain name
 *
 *                      This call wraps the Email record implementation and excepts pure
 *                      VARCHAR2 IN/OUT parameters.
 ******************************************************************************************/
PROCEDURE Validate_Email (
                     email_address                  IN OUT VARCHAR2 /* GOREMAIL_EMAIL_ADDRESS */
                    ,email_error_code               IN OUT VARCHAR2
                    ,email_error_text               IN OUT VARCHAR2
                    ,fv_plsql_error                 IN OUT VARCHAR2
                    ,fv_email_type_code             IN     VARCHAR2 DEFAULT NULL /* GOREMAL_EMAL_CODE */
                    ,fv_object_name                 IN     VARCHAR2 DEFAULT NULL /* Object Name where procedure is called from */
                    )
IS
  l_email_rec       CLEAN_Address.EMAIL_REC;
BEGIN
  --
  -- Assign the input variables to the email record
  --
  l_email_rec.email_address := email_address;
  email_error_text          := NULL;
  --
  -- Validate the Email
  --
  Validate_Email_Record (
           f_email_rec         => l_email_rec
          ,fv_email_error_text => email_error_text
          ,fv_plsql_error      => fv_plsql_error
          ,fv_email_type_code  => fv_email_type_code
          ,fv_object_name      => fv_object_name
          );
  --
  -- Remap the output email fields
  --
  email_address := l_email_rec.email_address;
  email_error_code := l_email_rec.email_status_code;
END Validate_Email;
/******************************************************************************************
 *  Function Name   :   Get_Email_Error_Text
 *
 *  Scope           :   PUBLIC
 *
 *  Description     :   Return the error text from the email error code
 ******************************************************************************************/
FUNCTION Get_Email_Error_Text (
                     email_error_code               IN     VARCHAR2
                    ) RETURN VARCHAR2
IS
BEGIN
  if email_error_code IS NULL then
    return NULL;
  end if;
  return cln$lookup.Get_Email_Status(email_error_code);
END Get_Email_Error_Text;
-----------------------------------------------------------------------------------------
--
-- PUBLIC PROCEDURES - PARAMETER GET / SET
--
-----------------------------------------------------------------------------------------
/******************************************************************************************
 *  Function Name   :   Get_Expand_Street_Suffix
 *
 *  Description     :
 *    Set gb_expand_street_suffix to TRUE to expand the street suffix (Ave -> Avenue) if it will
 *    fit on one line.  If set to FALSE, the USPS CASS standardization rules will be used.
 *    DEFAULT: FALSE
 ******************************************************************************************/
Function Get_Expand_Street_Suffix RETURN BOOLEAN
IS
BEGIN
  return NVL(gb_Expand_Street_Suffix, FALSE);
END Get_Expand_Street_Suffix;
/******************************************************************************************
 *  Procedure Name  :   Set_Expand_Street_Suffix
 ******************************************************************************************/
Procedure Set_Expand_Street_Suffix (
                     fb_value                       IN     BOOLEAN  DEFAULT FALSE
                    )
IS
BEGIN
  gb_Expand_Street_Suffix := NVL(fb_value, FALSE);
END Set_Expand_Street_Suffix;
/******************************************************************************************
 *  Function Name   :   Get_Suite_Apt_Before_Street
 *
 *  Description     :
 *    Set gb_suite_apt_before_street to TRUE to put the suite/apartment line before the address line
 *    in cases where the the suite/apartment does not fit on one line.  If set to FALSE, the suite/apt
 *    will appear on the line after the street address line.
 *    DEFAULT: FALSE
 ******************************************************************************************/
Function Get_Suite_Apt_Before_Street RETURN BOOLEAN
IS
BEGIN
  return NVL(gb_Suite_Apt_Before_Street, FALSE);
END Get_Suite_Apt_Before_Street;
/******************************************************************************************
 *  Procedure Name  :   Set_Suite_Apt_Before_Street
 ******************************************************************************************/
Procedure Set_Suite_Apt_Before_Street (
                     fb_value                       IN     BOOLEAN  DEFAULT FALSE
                    )
IS
BEGIN
  gb_Suite_Apt_Before_Street := NVL(fb_value, FALSE);
END Set_Suite_Apt_Before_Street;
/******************************************************************************************
 *  Function Name   :   Get_Append_Suite_To_Street
 *
 *  Description     :
 *    Set gb_append_suite_to_street to TRUE to always "try" to append the suite/apartment line
 *    on the same line as the the address line - only when it will fit.  If set to FALSE, the
 *    suite/apt will be on a separate line if there a free line available
 *    DEFAULT: TRUE
 ******************************************************************************************/
Function Get_Append_Suite_To_Street RETURN BOOLEAN
IS
BEGIN
  return NVL(gb_Append_Suite_To_Street, TRUE);
END Get_Append_Suite_To_Street;
/******************************************************************************************
 *  Procedure Name  :   Set_Append_Suite_To_Street
 ******************************************************************************************/
Procedure Set_Append_Suite_To_Street (
                     fb_value                       IN     BOOLEAN  DEFAULT TRUE
                    )
IS
BEGIN
  gb_Append_Suite_To_Street := NVL(fb_value, TRUE);
END Set_Append_Suite_To_Street;
/******************************************************************************************
 *  Function Name   :   Get_Use_ZIP_Plus4
 *
 *  Description     :
 *    Set gb_use_ZIP_Plus4 to TRUE to append the ZIP+4 digits after the ZIP Code in the
 *    SPRADDR_ZIP field.  If set to FALSE, only the 5-digit ZIP Code will be used
 *    DEFAULT: TRUE
 ******************************************************************************************/
Function Get_Use_ZIP_Plus4 RETURN BOOLEAN
IS
BEGIN
  return NVL(gb_Use_ZIP_Plus4, TRUE);
END Get_Use_ZIP_Plus4;
/******************************************************************************************
 *  Procedure Name  :   Set_Use_ZIP_Plus4
 ******************************************************************************************/
Procedure Set_Use_ZIP_Plus4 (
                     fb_value                       IN     BOOLEAN  DEFAULT TRUE
                    )
IS
BEGIN
  gb_Use_ZIP_Plus4 := NVL(fb_value, TRUE);
END Set_Use_ZIP_Plus4;
/******************************************************************************************
 *  Function Name   :   Get_Batch_Update_Existing
 *
 *  Description     :
 *    Set gb_batch_update_existing to TRUE to update the existing address record when running
 *    in batch mode.  If set to FALSE, a new record will be created if anything changes in the
 *    address.  This could easily double the size of the SPRADDR table if set FALSE, since most
 *    addresses are not keyed in in USPS standards. Please take this into consideration.
 *    DEFAULT: TRUE
 ******************************************************************************************/
Function Get_Batch_Update_Existing RETURN BOOLEAN
IS
BEGIN
  return NVL(gb_Batch_Update_Existing, TRUE);
END Get_Batch_Update_Existing;
/******************************************************************************************
 *  Procedure Name  :   Set_Batch_Update_Existing
 ******************************************************************************************/
Procedure Set_Batch_Update_Existing (
                     fb_value                       IN     BOOLEAN  DEFAULT TRUE
                    )
IS
BEGIN
  gb_Batch_Update_Existing := NVL(fb_value, TRUE);
END Set_Batch_Update_Existing;
/******************************************************************************************
 *  Function Name   :   Get_Telephone_Distance_Limit
 *
 *  Description     :
 *    Set gn_telephone_distance_limit to the number of miles threshhold to allow before an
 *    alert message is displayed to the user.  This is the distance between the telephone
 *    wire center (area code and prefix = first 6 digits) and the 5-digit ZIP Code
 *    DEFAULT: 100
 ******************************************************************************************/
Function Get_Telephone_Distance_Limit RETURN NUMBER
IS
BEGIN
  return NVL(gn_Telephone_Distance_Limit, 100);
END Get_Telephone_Distance_Limit;
/******************************************************************************************
 *  Procedure Name  :   Set_Telephone_Distance_Limit
 ******************************************************************************************/
Procedure Set_Telephone_Distance_Limit (
                     fn_value                       IN     NUMBER  DEFAULT 100
                    )
IS
BEGIN
  gn_Telephone_Distance_Limit := NVL(fn_value, 100);
END Set_Telephone_Distance_Limit;
/******************************************************************************************
 *  Function Name   :   Get_Error_Suggest_Count
 *
 *  Description     :
 *    Set gn_Error_Suggest_Count to the number of possible matches to return for unverified addresses
 *    DEFAULT: 0
 ******************************************************************************************/
Function Get_Error_Suggest_Count RETURN PLS_INTEGER
IS
BEGIN
  return NVL(gn_Error_Suggest_Count, 0);
END Get_Error_Suggest_Count;
/******************************************************************************************
 *  Procedure Name  :   Set_Error_Suggest_Count
 ******************************************************************************************/
Procedure Set_Error_Suggest_Count (
                     fn_value                       IN     PLS_INTEGER  DEFAULT 0
                    )
IS
BEGIN
  gn_Error_Suggest_Count := NVL(fn_value, 0);
END Set_Error_Suggest_Count;
/******************************************************************************************
 *  Function Name   :   Get_Upper_Case_Results
 *
 *  Description     :
 *    Set gb_Upper_Case_Results to TRUE to always UPPER CASE address fields
 *    DEFAULT: FALSE
 ******************************************************************************************/
Function Get_Upper_Case_Results RETURN BOOLEAN
IS
BEGIN
  return NVL(gb_Upper_Case_Results, FALSE);
END Get_Upper_Case_Results;
/******************************************************************************************
 *  Procedure Name  :   Set_Upper_Case_Results
 ******************************************************************************************/
Procedure Set_Upper_Case_Results (
                     fb_value                       IN     BOOLEAN  DEFAULT FALSE
                    )
IS
BEGIN
  gb_Upper_Case_Results := NVL(fb_value, FALSE);
END Set_Upper_Case_Results;
/******************************************************************************************
 *  Function Name   :   Get_Address_Lines
 *
 *  Description     :
 *    Set gn_Address_Lines to the number of address lines to process
 *    DEFAULT: 4
 ******************************************************************************************/
Function Get_Address_Lines RETURN NUMBER
IS
BEGIN
  return NVL(gn_Address_Lines, 4);
END Get_Address_Lines;
/******************************************************************************************
 *  Procedure Name  :   Set_Address_Lines
 ******************************************************************************************/
Procedure Set_Address_Lines (
                     fn_value                       IN     NUMBER  DEFAULT 4
                    )
IS
BEGIN
  gn_Address_Lines := NVL(fn_value, 4);
END Set_Address_Lines;
/******************************************************************************************
 *  Function Name   :   Get_Max_Address_Length
 *
 *  Description     :
 *    Set gn_Max_Address_Length to limit size of address field
 *    DEFAULT: 30
 ******************************************************************************************/
Function Get_Max_Address_Length RETURN NUMBER
IS
BEGIN
  return NVL(gn_Max_Address_Length, 30);
END Get_Max_Address_Length;
/******************************************************************************************
 *  Procedure Name  :   Set_Max_Address_Length
 ******************************************************************************************/
Procedure Set_Max_Address_Length (
                     fn_value                       IN     NUMBER  DEFAULT 55
                    )
IS
BEGIN
  gn_Max_Address_Length := NVL(fn_value, 30);
END Set_Max_Address_Length;
/******************************************************************************************
 *  Function Name   :   Get_Max_City_Length
 *
 *  Description     :
 *    Set gn_Max_City_Length to limit size of City field
 *    DEFAULT: 20
 ******************************************************************************************/
Function Get_Max_City_Length RETURN NUMBER
IS
BEGIN
  return NVL(gn_Max_City_Length, 20);
END Get_Max_City_Length;
/******************************************************************************************
 *  Procedure Name  :   Set_Max_City_Length
 ******************************************************************************************/
Procedure Set_Max_City_Length (
                     fn_value                       IN     NUMBER  DEFAULT 55
                    )
IS
BEGIN
  gn_Max_City_Length := NVL(fn_value, 20);
END Set_Max_City_Length;
/******************************************************************************************
 *  Function Name   :   Get_Ignore_Error
 ******************************************************************************************/
Function Get_Ignore_Error RETURN BOOLEAN
IS
BEGIN
  return gb_Ignore_Error;
END Get_Ignore_Error;
/******************************************************************************************
 *  Procedure Name  :   Set_Ignore_Error
 *
 *  Description     :
 *    Set gb_Ignore_Error to TRUE to allow Web Self-Service to save data even with an error code
 *    - NOTE: The error code will still be stored on the record but allow the transaction to continue
 ******************************************************************************************/
Procedure Set_Ignore_Error (
                     fb_value                       IN     BOOLEAN
                    )
IS
BEGIN
  gb_Ignore_Error := fb_value;
END Set_Ignore_Error;
/******************************************************************************************
 *  Function Name   :   Get_Object_Name
 ******************************************************************************************/
Function Get_Object_Name RETURN VARCHAR2
IS
BEGIN
  return gv_Object_Name;
END Get_Object_Name;
/******************************************************************************************
 *  Procedure Name  :   Set_Object_Name
 *
 *  Description     :
 *    Set gv_Object_Name to the Object that is currently calling the verification procedure
 ******************************************************************************************/
Procedure Set_Object_Name (
                     fv_value                       IN     VARCHAR2
                    )
IS
BEGIN
  gv_Object_Name := fv_value;
END Set_Object_Name;
/******************************************************************************************
 *  Function Name   :   Get_Local_Country
 ******************************************************************************************/
Function Get_Local_Country RETURN VARCHAR2
IS
BEGIN
  return gv_Local_Country;
END Get_Local_Country;
/******************************************************************************************
 *  Procedure Name  :   Set_Local_Country
 *
 *  Description     :
 *    Set gv_Local_Country to the value specified, which is the Banner specific
 *    Nation Code for the predominant country
 ******************************************************************************************/
Procedure Set_Local_Country (
                     fv_value                       IN     VARCHAR2
                    )
IS
BEGIN
  gv_Local_Country := fv_value;
END Set_Local_Country;
/******************************************************************************************
 *  Function Name   :   Get_Skip_International
 ******************************************************************************************/
Function Get_Skip_International RETURN BOOLEAN
IS
BEGIN
  return gb_Skip_International;
END Get_Skip_International;
/******************************************************************************************
 *  Procedure Name  :   Set_Skip_International
 *
 *  Description     :
 *    Set gb_Skip_International TRUE to not process international address records
 *    This allows more control on the purchased Web Service transactions.
 *    NOTE: User should pass this parameter into the batch procedure rather than setting directly
 ******************************************************************************************/
Procedure Set_Skip_International (
                     fb_value                       IN     BOOLEAN
                    )
IS
BEGIN
  gb_Skip_International := fb_value;
END Set_Skip_International;
/******************************************************************************************
 *  Function Name   :   Get_Current_URL
 *
 *  Description     :
 *    Return the current host, port, and PL/SQL DAD URL prefix - used in AJAX implementation
 ******************************************************************************************/
Function Get_Current_URL RETURN VARCHAR2
IS
  lv_current_url VARCHAR2(200) := NULL;
BEGIN
  if gv_current_url IS NOT NULL then
    return gv_current_url;
  end if;
  if NVL(owa_util.get_cgi_env('SERVER_PORT'),'80') not in ('80','443') then
    if instr(owa_util.get_cgi_env('HTTP_HOST'), owa_util.get_cgi_env('SERVER_PORT')) > 0 then
      lv_current_url := lower(owa_util.get_cgi_env('REQUEST_PROTOCOL'))||'://'
                            ||owa_util.get_cgi_env('HTTP_HOST')
                            ||owa_util.get_cgi_env('SCRIPT_NAME')||'/';
    else
      lv_current_url := lower(owa_util.get_cgi_env('REQUEST_PROTOCOL'))||'://'
                            ||owa_util.get_cgi_env('HTTP_HOST')
                            ||':'||owa_util.get_cgi_env('SERVER_PORT')
                            ||owa_util.get_cgi_env('SCRIPT_NAME')||'/';
    end if;
  else
    lv_current_url := lower(owa_util.get_cgi_env('REQUEST_PROTOCOL'))||'://'
                          ||owa_util.get_cgi_env('HTTP_HOST')
                          ||owa_util.get_cgi_env('SCRIPT_NAME')||'/';
  end if;
  -- Call the user exit procedure to set an override url if needed
  clean_address_banner_ue.Set_Override_URL(lv_current_url);
  return lv_current_url;
EXCEPTION
  WHEN OTHERS THEN
    RETURN NVL(lv_current_url, gv_current_url);
END Get_Current_URL;
/******************************************************************************************
 *  Function Name   :   Set_Current_URL
 *
 *  Description     :
 *    Set the current host, port, and PL/SQL DAD URL prefix - used in AJAX implementation
 *    NOTE: The setting should look something like: 'https://www.myURL.edu:8888/pls/DAD/'
 ******************************************************************************************/
Procedure Set_Current_URL (
                     fv_value                       IN     VARCHAR2
                    )
IS
BEGIN
  gv_current_url := fv_value;
END Set_Current_URL;
END CLEAN_Address_Banner_UM;
/
