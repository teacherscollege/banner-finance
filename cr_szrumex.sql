--------------------------------------------------------
--  File created - Friday-November-30-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table SZRUMEX
--------------------------------------------------------

  CREATE TABLE "BANINST1"."SZRUMEX" 
   (	"SZRUMEX_BUS_NAME" VARCHAR2(200 BYTE), 
	"SZRUMEX_ADDR_LINE1" VARCHAR2(200 BYTE), 
	"SZRUMEX_ADDR_LINE2" VARCHAR2(200 BYTE), 
	"SZRUMEX_ADDR_LINE3" VARCHAR2(200 BYTE), 
	"SZRUMEX_CITY" VARCHAR2(200 BYTE), 
	"SZRUMEX_STATE" VARCHAR2(200 BYTE), 
	"SZRUMEX_ZIP" VARCHAR2(200 BYTE), 
	"SZRUMEX_COUNTRY" VARCHAR2(100 BYTE), 
	"SZRUMEX_VEND_ID" VARCHAR2(12 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "DEVELOPMENT" ;
