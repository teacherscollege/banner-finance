  CREATE TABLE "FIMSMGR"."FZRAPPR" 
   (	"FZRAPPR_PIDM" NUMBER NOT NULL ENABLE, 
	"FZRAPPR_INDEX" VARCHAR2(6 BYTE) NOT NULL ENABLE, 
	"FZRAPPR_COAS_CODE" VARCHAR2(1 BYTE) NOT NULL ENABLE, 
	"FZRAPPR_FUND_CODE" VARCHAR2(6 BYTE), 
	"FZRAPPR_ORGN_CODE" VARCHAR2(6 BYTE), 
	"FZRAPPR_ACCT_CODE" VARCHAR2(6 BYTE), 
	"FZRAPPR_PROG_CODE" VARCHAR2(6 BYTE), 
	"FZRAPPR_SYSTEM" VARCHAR2(10 BYTE) NOT NULL ENABLE, 
	"FZRAPPR_SYS_ROLE" VARCHAR2(10 BYTE) NOT NULL ENABLE, 
	"FZRAPPR_INC_SALARY_FLG" VARCHAR2(1 BYTE), 
	"FZRAPPR_APPROVAL_LIMIT" NUMBER(11,2), 
	"FZRAPPR_SELF_APPR_FLG" VARCHAR2(1 BYTE), 
	"FZRAPPR_SELF_APPR_LIMIT" NUMBER(11,2), 
	"FZRAPPR_EFF_DATE" DATE NOT NULL ENABLE, 
	"FZRAPPR_EXP_DATE" DATE, 
	"FZRAPPR_USER" VARCHAR2(30 BYTE), 
	"FZRAPPR_ACTIVITY_DATE" DATE, 
	"FZRAPPR_SURROGATE_ID" NUMBER(19,0), 
	"FZRAPPR_VERSON" NUMBER(19,0), 
	"FZRAPPR_USER_ID" VARCHAR2(30 CHAR), 
	"FZRAPPR_DATA_ORIGIN" VARCHAR2(30 CHAR), 
	"FZRAPPR_VPDI_CODE" VARCHAR2(6 CHAR), 
	"FZRAPPR_GRANT_CODE" VARCHAR2(6 BYTE), 
	 CONSTRAINT "FK_SYS_ROLE" FOREIGN KEY ("FZRAPPR_SYSTEM", "FZRAPPR_SYS_ROLE")
	  REFERENCES "FIMSMGR"."FZRSROLE" ("FZRSROLE_SYS_CODE", "FZRSROLE_ROLE_CODE") ENABLE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DEVELOPMENT" ;

   COMMENT ON COLUMN "FIMSMGR"."FZRAPPR"."FZRAPPR_PIDM" IS 'Person who is granted the role';
   COMMENT ON COLUMN "FIMSMGR"."FZRAPPR"."FZRAPPR_INDEX" IS 'The index for which the role applies';
   COMMENT ON COLUMN "FIMSMGR"."FZRAPPR"."FZRAPPR_COAS_CODE" IS 'Chart of Accounts to which the index belongs';
   COMMENT ON COLUMN "FIMSMGR"."FZRAPPR"."FZRAPPR_FUND_CODE" IS 'The fund code corresponding to the Index';
   COMMENT ON COLUMN "FIMSMGR"."FZRAPPR"."FZRAPPR_ORGN_CODE" IS 'The orgn code corresponding to the Index';
   COMMENT ON COLUMN "FIMSMGR"."FZRAPPR"."FZRAPPR_ACCT_CODE" IS 'The account code corresponding to the Index';
   COMMENT ON COLUMN "FIMSMGR"."FZRAPPR"."FZRAPPR_PROG_CODE" IS 'The program code corresponding to the Index';
   COMMENT ON COLUMN "FIMSMGR"."FZRAPPR"."FZRAPPR_SYSTEM" IS 'The system/application for which this role is granted';
   COMMENT ON COLUMN "FIMSMGR"."FZRAPPR"."FZRAPPR_SYS_ROLE" IS 'The role being granted to the pidm';
   COMMENT ON COLUMN "FIMSMGR"."FZRAPPR"."FZRAPPR_INC_SALARY_FLG" IS 'A yes/no flag indicating if the individual is granted access to salary and benefit information.';
   COMMENT ON COLUMN "FIMSMGR"."FZRAPPR"."FZRAPPR_APPROVAL_LIMIT" IS 'The maximum dollar amount for which this user can approve for approval type roles';
   COMMENT ON COLUMN "FIMSMGR"."FZRAPPR"."FZRAPPR_SELF_APPR_FLG" IS 'A yes/no flag indicating if the individual can self approve for approval type roles';
   COMMENT ON COLUMN "FIMSMGR"."FZRAPPR"."FZRAPPR_SELF_APPR_LIMIT" IS 'The dollar limit for which a user can self approve.';
   COMMENT ON COLUMN "FIMSMGR"."FZRAPPR"."FZRAPPR_EFF_DATE" IS 'The date on which this record became effective';
   COMMENT ON COLUMN "FIMSMGR"."FZRAPPR"."FZRAPPR_EXP_DATE" IS 'The date on which this record expired. null indicates the current record';
   COMMENT ON COLUMN "FIMSMGR"."FZRAPPR"."FZRAPPR_ACTIVITY_DATE" IS 'The date of the last update to the record';
   COMMENT ON COLUMN "FIMSMGR"."FZRAPPR"."FZRAPPR_USER_ID" IS 'An audit column identifying the last user to update the record';

  CREATE INDEX "FIMSMGR"."FZRAPPR_EFFDT_IDX" ON "FIMSMGR"."FZRAPPR" ("FZRAPPR_EFF_DATE", "FZRAPPR_EXP_DATE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DEVELOPMENT" ;

  CREATE INDEX "FIMSMGR"."FZRAPPR_PIDM_INDEX_IDX" ON "FIMSMGR"."FZRAPPR" ("FZRAPPR_PIDM", "FZRAPPR_INDEX") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DEVELOPMENT" ;

  CREATE INDEX "FIMSMGR"."FZRAPPR_SYSROLE_IDX" ON "FIMSMGR"."FZRAPPR" ("FZRAPPR_SYSTEM", "FZRAPPR_SYS_ROLE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DEVELOPMENT" ;