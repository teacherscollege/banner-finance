/* new view for budget reporting*/
create or replace view tcapp.fin_report_all_roles_vw as
select a.fzrappr_index, a.fzrappr_fund_code, a.fzrappr_orgn_code, a.fzrappr_grant_code,
a.fzrappr_pidm, gobeacc_username as fzrappr_sys_role_username, 
tcapp.tc_argos_util.tc_get_format_name(a.fzrappr_pidm,'LF30') fzrappr_sys_role_name,
a.fzrappr_sys_role, 
vw.fzrappr_resppers_pidm,
vw.FZRAPPR_RESPPERS_USERNAME,
vw.fzrappr_resppers_name, 
vw.fzrappr_budadm_pidm,
vw.fzrappr_budadm_username,
vw.fzrappr_budadm_name
from gobeacc, fzrappr a, tcapp.fin_budget_report_role_vw vw
where gobeacc_pidm = a.fzrappr_pidm
and a.fzrappr_system = 'REPORT'
and a.fzrappr_sys_role = 'RPTVIEWER'
and trunc(sysdate) <= trunc(a.fzrappr_exp_date)
and trunc(sysdate) >= trunc(a.fzrappr_eff_date)
and a.fzrappr_index = vw.fzrappr_index
and a.fzrappr_fund_code = vw.fzrappr_fund_code
and a.fzrappr_orgn_code = vw.fzrappr_orgn_code;

drop view fin_report_all_roles__vw;

select * from fzrappr
where fzrappr_system = 'REPORT'
and fzrappr_index = '541145'
and fzrappr_fund_code = '582522'
and fzrappr_orgn_code = '111927';  

-- 114403  DEINER1

select * from 