
  CREATE OR REPLACE VIEW "TCAPP"."FIN_BUDGET_REPORT_ROLE_VW" ("FZRAPPR_INDEX", "FZRAPPR_FUND_CODE", "FZRAPPR_ORGN_CODE", "FZRAPPR_GRANT_CODE", "FZRAPPR_RESPPERS_PIDM", "FZRAPPR_RESPPERS_USERNAME", "FZRAPPR_RESPPERS_NAME", "FZRAPPR_BUDADM_PIDM", "FZRAPPR_BUDADM_USERNAME", "FZRAPPR_BUDADM_NAME", "FZRAPPR_RPTVIEWER_PIDM", "FZRAPPR_RPTVIEWER_USERNAME", "FZRAPPR_RPTVIEWER_NAME") AS 
  select fzrappr_index,fzrappr_fund_code, fzrappr_orgn_code, fzrappr_grant_code,
resppers_pidm fzrappr_resppers_pidm,
a.gobeacc_username fzrappr_resppers_username,
tcapp.tc_argos_util.tc_get_format_name(resppers_pidm,'LF30') fzrappr_resppers_name,
budadm_pidm fzrappr_budadm_pidm,
b.gobeacc_username fzrappr_budadm_username,
tcapp.tc_argos_util.tc_get_format_name(budadm_pidm,'LF30') fzrappr_budadm_name,
rptviewer_pidm fzrappr_rptviewer_pidm,
c.gobeacc_username fzrappr_rptviewer_username,
tcapp.tc_argos_util.tc_get_format_name(rptviewer_pidm,'LF30') fzrappr_rptviewer_name
from gobeacc a,gobeacc b,gobeacc c,
(select fzrappr_index,fzrappr_fund_code, fzrappr_orgn_code, max(fzrappr_grant_code) fzrappr_grant_code,
max(case when fzrappr_sys_role = 'RESPPERS' then fzrappr_pidm  else null end) resppers_pidm,
max(case when fzrappr_sys_role = 'BUDADM' then fzrappr_pidm else null end )budadm_pidm,
max(case when fzrappr_sys_role = 'RPTVIEWER' and fzrappr_inc_salary_flg = 'Y' then fzrappr_pidm else null end) rptviewer_pidm
from fzrappr
where fzrappr_system = 'REPORT'
--and trunc(fzrappr_exp_date = trunc(to_date('31-DEC-2099','dd-mon-yyyy'))
and trunc(sysdate) <= trunc(fzrappr_exp_date)
and trunc(sysdate) >= trunc(fzrappr_eff_date)
group by fzrappr_index, fzrappr_fund_code,fzrappr_orgn_code) resp_pers_matrix
where a.gobeacc_pidm (+)= resppers_pidm
and b.gobeacc_pidm (+)= budadm_pidm
and c.gobeacc_pidm (+)= rptviewer_pidm;