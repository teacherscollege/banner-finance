create or replace TRIGGER fgbjvcd_adr
AFTER DELETE
ON fgbjvcd
FOR EACH ROW

DECLARE

v_target_acct  VARCHAR2(4) := NULL;
v_push_ind     VARCHAR2(1) := NULL;

v_aidy1  VARCHAR2(2) := NULL;
v_aidy2  VARCHAR2(2) := NULL;
v_aidy_code  VARCHAR2(4) := NULL;

v_propagated VARCHAR2(1) := NULL;

CURSOR c_trnd
IS
SELECT 'x'
FROM   fgbtrnd
WHERE  FGBTRND_RUCL_CODE         = :old.FGBJVCD_RUCL_CODE
AND    FGBTRND_DOC_CODE          = :old.FGBJVCD_DOC_NUM
AND    FGBTRND_DOC_SEQ_CODE      = 20
AND    FGBTRND_SEQ_NUM           = :old.FGBJVCD_SEQ_NUM
AND    FGBTRND_ITEM_NUM          = 0
AND    FGBTRND_SUBMISSION_NUMBER = :old.FGBJVCD_SUBMISSION_NUMBER
AND    FGBTRND_REVERSAL_IND      = 'N'
AND    FGBTRND_COAS_CODE         = :old.FGBJVCD_COAS_CODE
AND    FGBTRND_FSYR_CODE         = :old.FGBJVCD_FSYR_CODE
AND    FGBTRND_ACCI_CODE         = :old.FGBJVCD_ACCI_CODE
AND    FGBTRND_FUND_CODE         = :old.FGBJVCD_FUND_CODE
AND    FGBTRND_ORGN_CODE         = :old.FGBJVCD_ORGN_CODE
AND    FGBTRND_ACCT_CODE         = :old.FGBJVCD_ACCT_CODE
AND    DECODE(FGBTRND_DR_CR_IND, '+', FGBTRND_TRANS_AMT,
                             SUBSTR(FGBTRND_TRANS_AMT, 2)) = :old.FGBJVCD_TRANS_AMT
AND    FGBTRND_DR_CR_IND         = :old.FGBJVCD_DR_CR_IND
AND    FGBTRND_USER_ID           = :old.FGBJVCD_USER_ID
AND    TRUNC(FGBTRND_ACTIVITY_DATE) = TRUNC(:old.FGBJVCD_ACTIVITY_DATE);

r_trnd c_trnd%ROWTYPE;


CURSOR c_detail IS
select tbracct_detail_code
from   tbracct t, rfrbase
where  tbracct_a_acci_code = :old.fgbjvcd_acci_code
and    tbracct_a_acct_code = :old.fgbjvcd_acct_code
and    tbracct_a_acci_code = rfrbase_xref_value
and    tbracct_detail_code = rfrbase_fund_code
and    rfrbase_active_ind = 'Y'
and    tbracct_detc_eff_date =
              (select max(tbracct_detc_eff_date)
              from tbracct
              where tbracct_detail_code = t.tbracct_detail_code);

r_det  c_detail%ROWTYPE;

CURSOR c_tbbdetc(p_det IN  tbbdetc.tbbdetc_detail_code%TYPE)
IS
select tbbdetc_detail_code, tbbdetc_dcat_code
from tbbdetc d
where tbbdetc_detail_code = p_det
and   tbbdetc_dcat_code IN ('FA4', 'STI', 'FA1', 'FA6', 'FA',
                           'FA2', 'EXF',
                            'EXD')
and not exists
   (select 'x'
     from rfrbase
     where rfrbase_fund_code = tbbdetc_detail_code);

r_tbb  c_tbbdetc%ROWTYPE;

BEGIN

IF ( ( :old.FGBJVCD_RUCL_CODE IN ('IB01', 'IB02', 'IB03', 'IB04', 'J020')
  OR :old.FGBJVCD_RUCL_CODE IN ('BD01', 'BD02', 'BD03', 'BD04', 'BD05') )
 AND :old.FGBJVCD_DR_CR_IND IN ('+', '-')
 AND SUBSTR(:old.FGBJVCD_TRANS_DESC, 1, 16) != 'Finaid to Budget')

THEN

v_aidy1 := :old.FGBJVCD_FSYR_CODE - 1;
v_aidy2 := :old.FGBJVCD_FSYR_CODE;
v_aidy_code := v_aidy1||v_aidy2;

-- check to see if posted

OPEN  c_trnd;
FETCH c_trnd INTO r_trnd;
  IF    c_trnd%FOUND THEN
  CLOSE c_trnd;

   OPEN  c_detail;
   FETCH c_detail INTO r_det;
     IF  c_detail%FOUND THEN
         DBMS_OUTPUT.PUT_LINE('Found detail');
      CLOSE c_detail;
      v_push_ind := 'Y';

     IF v_propagated = 'P' THEN
      v_push_ind := 'P';
     END IF;

     INSERT INTO rmxjvcd
     (rmxjvcd_doc_num,
      rmxjvcd_submission_number,
      rmxjvcd_RUCL_CODE,
      rmxjvcd_SEQ_NUM,
      rmxjvcd_ACTIVITY_DATE,
      rmxjvcd_USER_ID,
      rmxjvcd_TRANS_DATE,
      rmxjvcd_TRANS_AMT,
      rmxjvcd_DR_CR_IND,
      rmxjvcd_FSYR_CODE,
      rmxjvcd_ACCI_CODE,
      rmxjvcd_ACCT_CODE,
      rmxjvcd_status_ind,
      rmxjvcd_detail_code,
      rmxjvcd_push_ind,
      rmxjvcd_desc
     )
     VALUES(:old.FGBJVCD_DOC_NUM,       :old.FGBJVCD_SUBMISSION_NUMBER,
            :old.FGBJVCD_RUCL_CODE,     :old.FGBJVCD_SEQ_NUM,
            :old.FGBJVCD_ACTIVITY_DATE, :old.FGBJVCD_USER_ID,
            SYSDATE,                    :old.FGBJVCD_TRANS_AMT,
            :old.FGBJVCD_DR_CR_IND,     :old.FGBJVCD_FSYR_CODE,
            :old.FGBJVCD_ACCI_CODE,     :old.FGBJVCD_ACCT_CODE,
            :old.FGBJVCD_STATUS_IND,
            r_det.tbracct_detail_code,  v_push_ind,
            :old.FGBJVCD_TRANS_DESC);
     ELSE
     CLOSE  c_detail;
     OPEN   c_tbbdetc(r_det.tbracct_detail_code);
     FETCH  c_tbbdetc INTO r_tbb;
       IF   c_tbbdetc%FOUND THEN
      CLOSE c_tbbdetc;
            v_push_ind := 'N';

            INSERT INTO rmxjvcd
            (rmxjvcd_doc_num,
             rmxjvcd_submission_number,
             rmxjvcd_RUCL_CODE,
             rmxjvcd_SEQ_NUM,
             rmxjvcd_ACTIVITY_DATE,
             rmxjvcd_USER_ID,
             rmxjvcd_TRANS_DATE,
             rmxjvcd_TRANS_AMT,
             rmxjvcd_DR_CR_IND,
             rmxjvcd_FSYR_CODE,
             rmxjvcd_ACCI_CODE,
             rmxjvcd_ACCT_CODE,
             rmxjvcd_status_ind,
             rmxjvcd_detail_code,
             rmxjvcd_push_ind,
             rmxjvcd_desc
            )
      VALUES(:old.FGBJVCD_DOC_NUM,       :old.FGBJVCD_SUBMISSION_NUMBER,
             :old.FGBJVCD_RUCL_CODE,     :old.FGBJVCD_SEQ_NUM,
             :old.FGBJVCD_ACTIVITY_DATE, :old.FGBJVCD_USER_ID,
             SYSDATE,                    :old.FGBJVCD_TRANS_AMT,
             :old.FGBJVCD_DR_CR_IND,     :old.FGBJVCD_FSYR_CODE,
             :old.FGBJVCD_ACCI_CODE,     :old.FGBJVCD_ACCT_CODE,
             :old.FGBJVCD_STATUS_IND,
             r_det.tbracct_detail_code,  v_push_ind,
             :old.FGBJVCD_TRANS_DESC);
       ELSE
       CLOSE c_tbbdetc;
       NULL;
     END IF;
 END IF;
  ELSE  -- not posted
  CLOSE c_trnd;
  END IF;
  END IF;    -- bad RUCL CODE
END;
